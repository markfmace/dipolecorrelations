/////////////////////////////////////////////////////////
//                                                     //
// COMPUTES FOUR PARTICLE DIFFERNETIAL SPECTRA         //
// TO ALL ORDERS IN THE MV MODEL FOR FIXED (FINITE NC) //
//          WITH MONTE CARLO INTEGRATION               //
//                                                     //
/////////////////////////////////////////////////////////


// NUMBER OF COLORS //
const static int Nc=3;
// CASIMIR FACTOR //
const static double Cf=(Nc*Nc-1.0)/(2.0*Nc);
// NUMBER OF WILSON LINES
const static int nWilsonLines=8;
// SIZE OF ONE DIMENSION OF THE MATRIX //
const static int MatrixSize=24;

// PROPAGATOR INFRARED MASS //
double m=sqrt(0.2/B);
// DOMAIN SIZE //
double B=3.0;
// g^2 MU //
double gSQRmu=1.0;
// POWER FOR NUMBER OF POINTS //
int nPow=4;

// NUMBER OF v_n TO CALCULATE //
int vnMax=3;

// NUMBER OF PARTIAL SUMS TO CALCULATE FOR SIGN
const static int NumSums=16;

// MOMENTA BOUNDS //
double pMin=0.2;
double dp=0.1;
double pMax=3.0;

// DEFINE MOMENTA COUNTING//
int pMaxIndex=(pMax-pMin)/dp+0.1;

// DEFINE MATRIX ORDERING //
int MatrixIndex(int i, int j){
    // ROW MAJOR ORDERING //
    return i*MatrixSize+j;
}

// DEFINE MATRIX ORDERING //
int MomentaIndex(int pIndex,int n){
    // ROW MAJOR ORDERING //
    return vnMax*pIndex+n;
}

// DEFINE MATRIX ORDERING //
int ResultIndex(int config,int pIndex,int n){
    // ROW MAJOR ORDERING //
    return vnMax*(config*pMaxIndex+pIndex)+n;
}

// DEFINE BESSEL FUNCTION //
double Bessel(int n,double pr){
    
    return gsl_sf_bessel_Jn(n,pr);
}
// RAYLEIGH DISTRIBUTION //
double Rayleigh(double r, double sig)
{
    return r*exp(-r*r/(2*sig*sig))/(sig*sig);
}

#include <iomanip>

// RANDOM NUMBER GENERATOR //
#include "../MISC/GSLRandomNumberGenerator.cpp"
// INTEGRALS BY HAND //
#include "../INTEGRATION/Integration.cpp"
// PROBABILITY DISTRIBUTION FUNCTION //
#include "../PROBABILITY/PDFs.cpp"
// GSL DIAGONALIZATION //
#include "../MATRIXOPERATIONS/Diagonalization.cpp"
// GSL INVERSION //
#include "../MATRIXOPERATIONS/Inversion.cpp"
// OUTPUT HANDLING
#include "../IO/StringManipulation.cpp"
#include "../IO/OutputManagement.cpp"
// TIMING //
#include "../MISC/Timing.cpp"
// INCLUDE COMMANDLINE PROCESSING //
#include "../IO/COMMANDLINE/cfile.c"

// CALCULATE GLUON EXCHANGE MATRIX //
#include "../MATRIXOPERATIONS/FullMatrixEvaluationFourDipoles.cpp"


namespace Simulation {
    
    // MPI RANDOM NUMBER SEED //
    int MY_MPI_RNG_SEED;
    
    //////////////////////////
    //SIMULATION PROCDEDURE //
    //////////////////////////
    
    void Run(int MPI_RNG_SEED){
        
        ///////////
        // SETUP //
        ///////////
        
        //SET SEED //
        MY_MPI_RNG_SEED=MPI_RNG_SEED;
        
        //INITIALIZE RANDOM NUMBER GENERATOR //
        RandomNumberGenerator::Init(MY_MPI_RNG_SEED);
        
        // OUTPUT //
        std::cout << "#BEGINNING FOUR DIPOLE DIFFERENTIAL SPECTRA CALCULATION ID=" << MY_MPI_RNG_SEED << std::endl;
        
        ///////////////////
        // VARIABLES     //
        ///////////////////
        
        // RADIAL VARIABLES //
        double r1x,r1y,r2x,r2y,r3x,r3y,r4x,r4y;
        double r1,r2,r3,r4;
        
        // COM VARIABLES //
        double R1x,R1y,R2x,R2y,R3x,R3y,R4x,R4y;
        
        // ANGULAR VARIABLES //
        double phir1,phir2,phir3,phir4,theta1,theta2,rho,psi;
        int dphiIndexMax=16;
        double dphi=2.0*PI/double(dphiIndexMax);
        
        // GENERAL POSITION VECTORS //
        double **x;
        x = new double*[nWilsonLines]; // FIRST INDEX //
        for(int i=0;i<nWilsonLines;i++) {
            x[i] = new double[2]; // SECOND INDEX //
        }
        
        // SET TO ZERO INITIALLY
        for(int i=0;i<nWilsonLines;i++){
            for(int j=0;j<2;j++){
                x[i][j]=0.0;
            }
        }
        
        // MAX TOTAL COUNT //
        int iMax=std::pow(10,nPow);
        
        // MC INTEGRATED QUANTITIES //
        double Integrand;
        double* Mean = new double[vnMax*(pMaxIndex+1)*(NumSums+1)];
        double* IntegrandSqr = new double[vnMax*(pMaxIndex+1)*(NumSums+1)];
        
        // SET ZERO //
        for(int config=0;config<NumSums;config++){
            for(int pIndex=0;pIndex<=pMaxIndex;pIndex++){
                for(int n=0;n<=vnMax;n+=2){
                    
                    Mean[ResultIndex(config,pIndex,n)]=0.0;
                    IntegrandSqr[ResultIndex(config,pIndex,n)]=0.0;
                    
                }
            }
        }
        
        
        ///////////////////
        // MAKE LOG FILE //
        ///////////////////
        
        std::ofstream LogStream;
        LogStream.open(StringManipulation::StringCast(IO::OutputDirectory,"InfoID",MY_MPI_RNG_SEED,".log").c_str());
        
        LogStream.precision(OUTPUT_PRECISION);
        
        // INFO FILE  //
        LogStream << "#PARAMETER LOG FOR MC FOUR DIPOLE" << std::endl;
        LogStream << "#iMax=" << iMax << std::endl;
        LogStream << "#Nc=" << Nc << std::endl;
        LogStream << "#nWilsonLines=" << nWilsonLines << std::endl;
        LogStream << "#m=" << m << std::endl;
        LogStream << "#B=" << B << std::endl;
        LogStream << "#gSQRmu=" << gSQRmu << std::endl;
        LogStream << "#pMin=" << pMin << std::endl;
        LogStream << "#pMax=" << pMax << std::endl;
        LogStream << "#dp=" << dp << std::endl;
        LogStream << "#pMaxIndex=" << pMaxIndex << std::endl;
        LogStream << "#dphiIndexMax=" << dphiIndexMax << std::endl;
        
        std::cerr << "#m=" << m << " #Nc=" << Nc << " #B=" << B << " #gSQRmu=" << gSQRmu << " #iMax=" << iMax << std::endl;
        
        // CLOSE OUT-STREAM //
        LogStream.close();
        
        // COMMANDLINE OUTPUT //
        std::cout << "#COMPUTING VALUES" << std::endl;
        
        // RESET TIMING //
        Timing::Reset();
        
        std::cout << "#CALCULATING NORMS" << std::endl;
        
        // SET NORMS //
        double* Norm=new double[vnMax*(pMaxIndex+1)*(NumSums+1)];
        
        // SET ZERO //
        for(int sign=0;sign<2;sign++){
            for(int pIndex=0;pIndex<=pMaxIndex;pIndex++){
                for(int n=0;n<=vnMax;n+=2){
                    
                    Norm[ResultIndex(sign,pIndex,n)]=0.0;
                    
                }
                
            }
        }
        
        // IntegralFlag= 0 KERN>0, 1 KERN<0, 2 KERN, 3 abs(KERN), //

        // DO INTEGRATIONS //
        for(int pIndex=0;pIndex<=pMaxIndex;pIndex++){
            
            // BEGIN GSL OPTION //
            /*
            double Normalization,error;
            
            gsl_integration_workspace * w = gsl_integration_workspace_alloc (1000);
            gsl_function F;
            
            // SIGN = +1
            double p=1.0*(pMin+dp*pIndex);
            
            F.params=&p;
            
            F.function = &PDF::P0;
            gsl_integration_qagiu(&F, 0.0, 0.0, 1.e-10, 1000, w, &Normalization, &error);
            Norm[ResultIndex(0,pIndex,0)]=Normalization;
            
            F.function = &PDF::P2;
            gsl_integration_qagiu(&F, 0.0, 0.0, 1.e-10, 1000, w, &Normalization, &error);
            Norm[ResultIndex(0,pIndex,2)]=Normalization;
            
            // SIGN = -1
            p=-1.0*(pMin+dp*pIndex);
            
            F.params=&p;
            
            F.function = &PDF::P0;
            gsl_integration_qagiu (&F, 0.0, 0.0, 1.e-10, 1000, w, &Normalization, &error);
            Norm[ResultIndex(1,pIndex,0)]=-Normalization;
            
            F.function = &PDF::P2;
            gsl_integration_qagiu (&F, 0.0, 0.0, 1.e-10, 1000, w, &Normalization, &error);
            Norm[ResultIndex(1,pIndex,2)]=-Normalization;
            
            gsl_integration_workspace_free (w);
             */
            // END GSL OPTION //
            
            // BEGIN BY HAND OPTION //             // n=0 //
             // SIGN = +1 | J_n>0 //
             Norm[ResultIndex(0,pIndex,0)]=PDF::GetNormalization(2,p,0);
             // SIGN = -1 | J_n<0//
             Norm[ResultIndex(1,pIndex,0)]=PDF::GetNormalization(3,p,0);
             
             // n=2 //
             // SIGN = +1 | J_n>0 //
             Norm[ResultIndex(0,pIndex,2)]=PDF::GetNormalization(2,p,2);
             // SIGN = -1 | J_n<0//
             Norm[ResultIndex(1,pIndex,2)]=PDF::GetNormalization(3,p,2);
            // END BY HAND OPTION //
            
        }
        
        // GET TIMING
        double time=Timing::Get();
        
        std::cerr << "#IN t=" << time << " s NORMS WERE CALCULATED" << std::endl;
        
        // COMMANDLINE OUTPUT //
        std::cout << "#COMPUTING VALUES" << std::endl;
        
        // RESET TIMING //
        Timing::Reset();
        // INDEX FOR BOTH POSITIVE POSITIVE, NEGATIVE NEGATIVE, AND POSITIVE NEGATIVE VALUES
        int i[NumSums];
        
        // DEFINE SIGN VARIABLES //
        int IntegrandSign=0, sign1=0, sign2=0, sign3=0, sign4=0;
        
        // FILE OUTPUT
        std::ofstream OutputStream;
        
        OutputStream.open(StringManipulation::StringCast(IO::OutputDirectory,"IntegratedSpectra_m",m,"B",B,"gsm",gSQRmu,"ID",MY_MPI_RNG_SEED,".txt").c_str());
        
        OutputStream.precision(OUTPUT_PRECISION);
        
        // GET AVERAGE AND VALUES FOR ERROR //
        for(int pIndex=0;pIndex<=pMaxIndex;pIndex++){
            
            // DEFINE MOMENTUM //
            double p=pMin+dp*pIndex;
            
            // OUTPUT MOMENTA //
            OutputStream << p << " ";
            
            // INTEGRATE OVER iMax POINTS //
            // RESET INITIAL COUNT //
            for(int n=0;n<=vnMax;n+=2){
                
                std::cerr << "#pIndex=" << pIndex << " n=" << n << std::endl;
                
                // RESET COUNTS //
                for(int j=0;j<NumSums;j++){
                    i[j]=0;
                    
                }
                
                // TOTAL POINTS COUNTED //
                int iTot;
                
                // COUNT TO TOTAL OF iMax //
                do{
                    
                    // RESET COUNTER
                    iTot=0;
                    
                    // IMPORTANCE SAMPLING REJECTION VARIABLE //
                    double RejectVar;
                    do{
                        r1=RandomNumberGenerator::RaleighRNG(sqrt(2.0*B));
                        
                        RejectVar=RandomNumberGenerator::rng();
                        
                    }while(RejectVar>PDF::PABS(n,r1,p)/Rayleigh(r1,sqrt(2*B)));
                    
                    do{
                        r2=RandomNumberGenerator::RaleighRNG(sqrt(2.0*B));
                        
                        RejectVar=RandomNumberGenerator::rng();
                        
                    }while(RejectVar>PDF::PABS(n,r2,p)/Rayleigh(r2,sqrt(2.0*B)));
                    
                    do{
                        r3=RandomNumberGenerator::RaleighRNG(sqrt(2.0*B));
                        
                        RejectVar=RandomNumberGenerator::rng();
                        
                    }while(RejectVar>PDF::PABS(n,r3,p)/Rayleigh(r3,sqrt(2.0*B)));
                    
                    do{
                        r4=RandomNumberGenerator::RaleighRNG(sqrt(2.0*B));
                        
                        RejectVar=RandomNumberGenerator::rng();
                        
                    }while(RejectVar>PDF::PABS(n,r4,p)/Rayleigh(r4,sqrt(2.0*B)));
                    
                    
                    // SET RADIUS VECTOR //
                    R1x=RandomNumberGenerator::Gauss(sqrt(B/2.0));
                    R1y=RandomNumberGenerator::Gauss(sqrt(B/2.0));
                    R2x=RandomNumberGenerator::Gauss(sqrt(B/2.0));
                    R2y=RandomNumberGenerator::Gauss(sqrt(B/2.0));
                    R3x=RandomNumberGenerator::Gauss(sqrt(B/2.0));
                    R3y=RandomNumberGenerator::Gauss(sqrt(B/2.0));
                    R4x=RandomNumberGenerator::Gauss(sqrt(B/2.0));
                    R4y=RandomNumberGenerator::Gauss(sqrt(B/2.0));
                    
                    // FIRST ANGLE //
                    theta1=2.0*PI*RandomNumberGenerator::rng();
                    theta2=2.0*PI*RandomNumberGenerator::rng();
                    rho=2.0*PI*RandomNumberGenerator::rng();
                    
                    // RESET //
                    Integrand=0;
                    
                    //TRAPEZOID RULE ON PSI INTEGRAL
                    for(int dpsiIndex=0;dpsiIndex<dphiIndexMax;dpsiIndex++){
                        
                        // SECONARDY ANGLE //
                        psi=dpsiIndex*dphi;
                        
                        // DEFINE AZIMUTHAL ANGLES //
                        phir1=rho/2.0+psi/4.0+theta1/4.0;
                        phir2=rho/2.0+psi/4.0-theta1/4.0;
                        phir3=rho/2.0-psi/4.0+theta2/4.0;
                        phir4=rho/2.0-psi/4.0-theta2/4.0;
                        
                        r1x=r1*cos(phir1);
                        r1y=r1*sin(phir1);
                        r2x=r2*cos(phir2);
                        r2y=r2*sin(phir2);
                        r3x=r3*cos(phir3);
                        r3y=r3*sin(phir3);
                        r4x=r4*cos(phir4);
                        r4y=r4*sin(phir4);
                        
                        // SET ACUTAL POINTS FOR DIPOLE CORRELATOR
                        x[0][0]=(R1x+0.5*r1x);  x[0][1]=(R1y+0.5*r1y);
                        x[1][0]=(R1x-0.5*r1x);  x[1][1]=(R1y-0.5*r1y);
                        x[2][0]=(R2x+0.5*r2x);  x[2][1]=(R2y+0.5*r2y);
                        x[3][0]=(R2x-0.5*r2x);  x[3][1]=(R2y-0.5*r2y);
                        x[4][0]=(R3x+0.5*r3x);  x[4][1]=(R3y+0.5*r3y);
                        x[5][0]=(R3x-0.5*r3x);  x[5][1]=(R3y-0.5*r3y);
                        x[6][0]=(R4x+0.5*r4x);  x[6][1]=(R4y+0.5*r4y);
                        x[7][0]=(R4x-0.5*r4x);  x[7][1]=(R4y-0.5*r4y);
                        
                        // SET MATRIX EXPONENTIAL RESULT //
                        Integrand+=MatrixEvaluation(x,gSQRmu)*cos(double(n)*psi)*dphi/(2.0*PI);
                        //Integrand+=cos(double(n)*(phir1+phir2-phir3-phir4))*dphi/(2.0*PI);
                        
                    }
                    
                    // DETERMINE SIGN OF EACH DISTRIBUTION //
                    sign1=int(0.5*(1.0-PDF::P(n,r1,p)/sqrt(PDF::P(n,r1,p)*PDF::P(n,r1,p))));
                    sign2=int(0.5*(1.0-PDF::P(n,r2,p)/sqrt(PDF::P(n,r2,p)*PDF::P(n,r2,p))));
                    sign3=int(0.5*(1.0-PDF::P(n,r3,p)/sqrt(PDF::P(n,r3,p)*PDF::P(n,r3,p))));
                    sign4=int(0.5*(1.0-PDF::P(n,r4,p)/sqrt(PDF::P(n,r4,p)*PDF::P(n,r4,p))));
                    
                    // DETERMINE BIN //
                    IntegrandSign=2.0*(2.0*(2.0*sign1+sign2)+sign3)+sign4;
                    
                    // INCREMENT //
                    i[IntegrandSign]++;
                    
                    // MULTIPLY BY DISTR NORMS //
                    Integrand*=Norm[ResultIndex(sign1,pIndex,n)]*Norm[ResultIndex(sign2,pIndex,n)]*Norm[ResultIndex(sign3,pIndex,n)]*Norm[ResultIndex(sign4,pIndex,n)];
                    
                    // DETERMINE MEAN AND VARIANCE //
                    Mean[ResultIndex(IntegrandSign,pIndex,n)]+=(Integrand-Mean[ResultIndex(IntegrandSign,pIndex,n)])/double(i[IntegrandSign]);
                    IntegrandSqr[ResultIndex(IntegrandSign,pIndex,n)]+=Integrand*Integrand;

                    
                    for(int config=0;config<NumSums;config++){
                        iTot+=i[config];
                    }
                    
                }while(iTot<iMax);
                
                // COMMANDLINE OUTPUT //
                std::cerr << "#i= ";
                
                for(int config=0;config<NumSums;config++){
                    std::cerr << i[config] << " ";
                }
                std::cerr << " PTS COMPUTED " << std::endl;
                
                // UNCERTAINTY OF THE MEAN //
                double varSqrMean[NumSums];
                // POSITIVE POSITIVE VALUES
                for(int config=0;config<NumSums;config++){
                    if(i[0]!=0){
                        varSqrMean[config]=(IntegrandSqr[ResultIndex(config,pIndex,n)]-double(i[config])*(Mean[ResultIndex(config,pIndex,n)]*Mean[ResultIndex(config,pIndex,n)]))/ (double(i[config])*(double(i[config])-1.0));
                    }
                    else{
                        varSqrMean[config]=0.0;
                    }
                    
                }
                
                double TotalMean=0.0;
                // SUM OVER BINS //
                for(int config=0;config<NumSums;config++){
                    if(i[config]>=100){
                        TotalMean+=Mean[ResultIndex(config,pIndex,n)];
                    }
                }
                
                double TotalUncertainty=0.0;
                double TempUncertainty=0.0;
                for(int config=0;config<NumSums;config++){
                    if(i[config]>=100){
                        TempUncertainty+=varSqrMean[config]*varSqrMean[config];
                    }
                }
                TotalUncertainty=std::pow(TempUncertainty,0.25);
                // OUTPUT DATA AND UNCERTAINTY //
                OutputStream << TotalMean << " " << TotalUncertainty << " ";
                
            }
            // NEW LINE //
            OutputStream << std::endl;
            
        }
        
        // CLOSE OUT-STREAM //
        OutputStream.close();
        
        // COMMANDLINE OUTPUT //
        std::cout << "#FINISHED COMPUTING VALUES" << std::endl;
        
        // CLEAN-UP //
        delete[] Mean;
        delete[] IntegrandSqr;
        
        delete[] Norm;
        
        for(int j=0;j<nWilsonLines;j++){
            delete[] x[j];
        }
        // COMMANDLINE OUTPUT //
        std::cout << "#DONE SAVING VALUES ID=" << MY_MPI_RNG_SEED << std::endl;
        
        
    }
}
