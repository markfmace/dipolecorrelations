/////////////////////////////////////////////////////////
//                                                     //
// COMPUTES ONE DIPOLE AVERAGE FOR A GIVEN POSITION    //
// TO ALL ORDERS IN THE MV MODEL FOR FIXED (FINITE NC) //
//          WITH MONTE CARLO INTEGRATION               //
//                                                     //
/////////////////////////////////////////////////////////


// NUMBER OF COLORS //
const static int Nc=3;
// CASIMIR FACTOR //
const static double Cf=(Nc*Nc-1.0)/(2.0*Nc);
// NUMBER OF WILSON LINES
const static int nWilsonLines=2;
// SIZE OF ONE DIMENSION OF THE MATRIX //
const static int MatrixSize=1;

// PROPAGATOR INFRARED MASS //
double m=0.1;
// DOMAIN SIZE //
double B=3.0;
// g^2 MU //
double gSQRmu=1.0;
// POWER FOR NUMBER OF POINTS //
int nPow=5;

// NUMBER OF v_n TO CALCULATE //
int vnMax=1;

// MOMENTA BOUNDS //
double pMin=0.2;
double dp=0.2;
double pMax=3.0;

// DEFINE MOMENTA COUNTING//
int pMaxIndex=(pMax-pMin)/dp+0.1;

// DEFINE MATRIX ORDERING //
int MatrixIndex(int i, int j){
    // ROW MAJOR ORDERING //
    return i*MatrixSize+j;
}

// DEFINE MATRIX ORDERING //
int MomentaIndex(int pIndex,int n){
    // ROW MAJOR ORDERING //
    return vnMax*pIndex+n;
}

// DEFINE MATRIX ORDERING //
int ResultIndex(int pow,int pIndex,int n){
    // ROW MAJOR ORDERING //
    return vnMax*(pow*pMaxIndex+pIndex)+n;
}

// DEFINE BESSEL FUNCTION //
double Bessel(int n,double pr){
    
    return gsl_sf_bessel_Jn(n,pr);
}

#include <iomanip>


// RANDOM NUMBER GENERATOR //
#include "../MISC/GSLRandomNumberGenerator.cpp"
// GSL DIAGONALIZATION //
#include "../MATRIXOPERATIONS/Diagonalization.cpp"
// GSL INVERSION //
#include "../MATRIXOPERATIONS/Inversion.cpp"
// OUTPUT HANDLING
#include "../IO/StringManipulation.cpp"
#include "../IO/OutputManagement.cpp"
// TIMING //
#include "../MISC/Timing.cpp"
// INCLUDE COMMANDLINE PROCESSING //
#include "../IO/COMMANDLINE/cfile.c"

double Lfunction(double ux,double uy,double vx,double vy){
    
    if(ux==vx && uy==vy){
        return 0.0;
    }
    else{
        return EuclideanNorm(ux,uy,vx,vy)*gsl_sf_bessel_K1(m*EuclideanNorm(ux,uy,vx,vy))/(4.0*m*PI) - 1.0/(4.0*std::pow(m,2)*PI);
    }
    
}

// SINGLE PARTICLE KERNEL //
double SingleParticleKernel(double x,double y,double gSQRmu){
    return exp(Cf*gSQRmu*gSQRmu*Lfunction(x/2.0,0.0,-x/2.0,0.0));
}
// RAYLEIGH DISTRIBUTION //
double Rayleigh(double r, double sig)
{
    return r*exp(-r*r/(2*sig*sig))/(sig*sig);
}
#include "../INTEGRATION/Integration.cpp"

#include "../PROBABILITY/PDFs.cpp"


namespace Simulation {
    
    // MPI RANDOM NUMBER SEED //
    int MY_MPI_RNG_SEED;
    
    //////////////////////////
    //SIMULATION PROCDEDURE //
    //////////////////////////
    
    void Run(int MPI_RNG_SEED){
        
        ///////////
        // SETUP //
        ///////////
        
        //SET SEED //
        MY_MPI_RNG_SEED=MPI_RNG_SEED;
        
        //INITIALIZE RANDOM NUMBER GENERATOR //
        RandomNumberGenerator::Init(MY_MPI_RNG_SEED);
        
        // OUTPUT //
        std::cout << "#BEGINNING ONE DIPOLE CALCULATION ID=" << MY_MPI_RNG_SEED << std::endl;
        
        ///////////////////
        // VARIABLES     //
        ///////////////////
        
        // RADIAL VARIABLES //
        double r;
        
        // MOMENTUM VARIABLES //
        double p;
        
        // iMax
        int iMax=std::pow(10,nPow);
        
        
        // MC INTEGRATED QUANTITIES //
        double Integrand;
        double* Mean = new double[vnMax*(pMaxIndex+1)*2];
        double* IntegrandSqr = new double[vnMax*(pMaxIndex+1)*2];
        
        // SET ZERO //
        for(int pow=0;pow<2;pow++){
            for(int pIndex=0;pIndex<pMaxIndex;pIndex++){
                for(int n=0;n<vnMax;n+=2){
                    
                    Mean[ResultIndex(pow,pIndex,n)]=0.0;
                    IntegrandSqr[ResultIndex(pow,pIndex,n)]=0.0;
                    
                }
            }
        }
        
        // RESULT OF MATRIX EXPONENTITION AND EXPONENTIAL SUPRESSION FACTOR //
        double MatResult;
        
        ///////////////////
        // MAKE LOG FILE //
        ///////////////////
        
        std::ofstream LogStream;
        LogStream.open(StringManipulation::StringCast(IO::OutputDirectory,"InfoID",MY_MPI_RNG_SEED,".log").c_str());
        
        LogStream.precision(OUTPUT_PRECISION);
        
        // INFO FILE  //
        LogStream << "#PARAMETER LOG FOR MC SINGLE DIPOLE DIFFERENTIAL SPECTRA" << std::endl;
        LogStream << "#iMax=" << iMax << std::endl;
        LogStream << "#Nc=" << Nc << std::endl;
        LogStream << "#nWilsonLines=" << nWilsonLines << std::endl;
        LogStream << "#m=" << m << std::endl;
        LogStream << "#B=" << B << std::endl;
        LogStream << "#gSQRmu=" << gSQRmu << std::endl;
        LogStream << "#pMin=" << pMin << std::endl;
        LogStream << "#pMax=" << pMax << std::endl;
        LogStream << "#dp=" << dp << std::endl;
        LogStream << "#pMaxIndex=" << pMaxIndex << std::endl;
        
        std::cerr << "#m=" << m << " #Nc=" << Nc << " #B=" << B << " #gSQRmu=" << gSQRmu << " #iMax=" << iMax << std::endl;
        
        // CLOSE OUT-STREAM //
        LogStream.close();
        
        // COMMANDLINE OUTPUT //
        std::cout << "#COMPUTING VALUES" << std::endl;
        
        // RESET TIMING //
        Timing::Reset();
        
        std::cout << "#CALCULATING NORMS" << std::endl;
        
        // SET NORMS //
        double* Norm=new double[vnMax*(pMaxIndex+1)*2];
        double* NormTot=new double[vnMax*(pMaxIndex+1)];
        
        // SET ZERO //
        for(int pow=0;pow<2;pow++){
            for(int pIndex=0;pIndex<pMaxIndex;pIndex++){
                for(int n=0;n<vnMax;n+=2){
                    
                    Norm[ResultIndex(pow,pIndex,n)]=0.0;
                    NormTot[MomentaIndex(pIndex,n)]=0.0;
                    
                    
                }
                
            }
        }
        // DO INTEGRATIONS //
        for(int pIndex=0;pIndex<pMaxIndex;pIndex++){
            
            double Normalization,error;
            
            gsl_integration_workspace * w = gsl_integration_workspace_alloc (1000);
            gsl_function F;
            
            // SIGN = +1
            p=1.0*(pMin+dp*pIndex);
            
            F.params=&p;
            
            F.function = &PDF::P0;
            gsl_integration_qagiu (&F, 0.0, 0.0, 1.e-10, 1000, w, &Normalization, &error);
            Norm[ResultIndex(0,pIndex,0)]=Normalization;
            
            // SIGN = -1
            p=-1.0*(pMin+dp*pIndex);
            
            F.params=&p;
            
            F.function = &PDF::P0;
            gsl_integration_qagiu (&F, 0.0, 0.0, 1.e-10, 1000, w, &Normalization, &error);
            Norm[ResultIndex(1,pIndex,0)]=Normalization;
            
            
            // TOTAL NORM //
            for(int n=0;n<vnMax;n+=2){
                NormTot[MomentaIndex(pIndex,n)]=Norm[ResultIndex(0,pIndex,n)]+Norm[ResultIndex(1,pIndex,n)];
            }
            
            gsl_integration_workspace_free (w);
            
        }
        
        // GET TIMING
        double time=Timing::Get();
        
        std::cerr << "#IN t=" << time << " s NORMS WERE CALCULAED" << std::endl;
        
        // COMMANDLINE OUTPUT //
        std::cout << "#COMPUTING VALUES" << std::endl;
        
        // RESET TIMING //
        Timing::Reset();
        // INDEX FOR BOTH POSITIVE AND NEGATIVE VALUES
        int i[2];
        
        for(int pow=0;pow<2;pow++){
            // DEFINE SIGN
            int Sign=std::pow(-1,pow);
            
            // INTEGRATE OVER iMax POINTS //
            for(i[pow]=1;i[pow]<=iMax;i[pow]++){
                
                // GET AVERAGE AND VALUES FOR ERROR //
                for(int pIndex=0;pIndex<pMaxIndex;pIndex++){
                    
                    // DEFINE MOMENTUM //
                    p=Sign*(pMin+dp*pIndex);
                    
                    //std::cerr << "#SIGN=" << Sign << " #i=" << i << " #p=" << p << std::endl;
                    
                    
                    for(int n=0;n<vnMax;n+=2){
                        
                        
                        if(Norm[ResultIndex(pow,pIndex,n)]<std::pow(10.0,-6)){
                            //std::cerr << "#TOO SMALL NORM " << std::endl;
                            break;
                        }
                        
                        double RejectVar;
                        
                        do{
                            r=4.0*B*RandomNumberGenerator::rng();

                            
                            RejectVar=RandomNumberGenerator::rng();
                            
                        }while(RejectVar>PDF::P0(r,&p)/Norm[ResultIndex(pow,pIndex,n)]);
                        
                        
                        // SET MATRIX EXPONENTIAL RESULT //
                        MatResult=SingleParticleKernel(r,0.0,gSQRmu);

                        
                        Integrand=MatResult*Norm[ResultIndex(pow,pIndex,n)];

                        Mean[ResultIndex(pow,pIndex,n)]+=(Integrand-Mean[ResultIndex(pow,pIndex,n)])/double(i[pow]);
                        IntegrandSqr[ResultIndex(pow,pIndex,n)]+=Integrand*Integrand;
                    }
                    
                }
                
            }
        }
        
        // GET TIMING
        time=Timing::Get();
        // OUTPUT TIMING
        std::cerr << "#IN t=" << time << " s iMax=" << iMax << " PTS WERE COMPUTED " << std::endl;
        
        // COMMANDLINE OUTPUT //
        std::cout << "#FINISHED COMPUTING VALUES" << std::endl;
        
        // COMMANDLINE OUTPUT //
        std::cout << "#SAVING VALUES" << std::endl;
        // FILE OUTPUT
        std::ofstream OutputStream;
        
        OutputStream.open(StringManipulation::StringCast(IO::OutputDirectory,"IntegratedSpectra_m",m,"B",B,"ID",MY_MPI_RNG_SEED,".txt").c_str());
        
        OutputStream.precision(OUTPUT_PRECISION);
        
        OutputStream << "#t=" << time << " s iMax=" << iMax << " PTS COMPUTED " << std::endl;
        
        // OUTPUT
        for(int pIndex=0;pIndex<pMaxIndex;pIndex++){
            
            // DEFINE MOMENTUM //
            p=pMin+dp*pIndex;
            
            // OUPUT MOMENTA TO FILE //
            OutputStream << p << " ";
            
            for(int n=0;n<vnMax;n+=2){
                
                // UNCERTAINTY OF THE MEAN //
                // POSITIVE VALUES
                double varSqrMeanP=(IntegrandSqr[ResultIndex(0,pIndex,n)]-double(iMax)*(Mean[ResultIndex(0,pIndex,n)]*Mean[ResultIndex(0,pIndex,n)]))/ (double(iMax)*(double(iMax)-1.0));
                // NEGATIVE VALUES
                double varSqrMeanM=(IntegrandSqr[ResultIndex(1,pIndex,n)]-double(iMax)*(Mean[ResultIndex(1,pIndex,n)]*Mean[ResultIndex(1,pIndex,n)]))/ (double(iMax)*(double(iMax)-1.0));
                /*
                if(abs(varSqrMeanP) < std::pow(10,-16)){
                    varSqrMeanP=0.0;
                }
                if(abs(varSqrMeanM) < std::pow(10,-16)){
                    varSqrMeanM=0.0;
                }
                */
                
                // OUTPUT INTEGRATED DATA AND UNCERTAINTY //
                //std::cerr << p << " " << Mean[ResultIndex(0,pIndex,n)] << " " << Mean[ResultIndex(1,pIndex,n)] << " " << varSqrMeanP << " " << varSqrMeanM << std::endl;

                
                OutputStream << Mean[ResultIndex(0,pIndex,n)]-Mean[ResultIndex(1,pIndex,n)] << " " << sqrt(abs(varSqrMeanP)+abs(varSqrMeanM)) << " ";
                
                
            }
            // NEW LINE //
            OutputStream << std::endl;
        }
        
        // CLOSE OUT-STREAM //
        OutputStream.close();
        
        // CLEAN-UP //
        delete[] Mean;
        delete[] IntegrandSqr;
        
        delete[] Norm;
        delete[] NormTot;
        
        // COMMANDLINE OUTPUT //
        std::cout << "#DONE SAVING VALUES ID=" << MY_MPI_RNG_SEED << std::endl;
        
        
    }
}
