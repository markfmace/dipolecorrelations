namespace Timing{
    
    double c0;
    double TotalTime;
    
    void Reset(){
        
        c0=clock();
        
        TotalTime=0.0;
        
    }
    
    double Get(){
        
        return (clock()-c0)/CLOCKS_PER_SEC;
        
    }
    
    
    void UnPauseClock(){
        
        c0=clock();
        
    }
    
    void PauseClock(){
        
        TotalTime+=Get();
        
        std::cerr <<"# TOTAL RUN TIME AT PAUSE: " <<TotalTime << std::endl;

        
    }



    
}
