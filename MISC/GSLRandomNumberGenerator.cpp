#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>

#define D_SQRT2    double(1.414213562373095048801688724209698078569671875376948073176679738)   // sqrt(2)


namespace RandomNumberGenerator{
    
    //GSL RANDOM NUMBER GENERATORS AND SEED
    const gsl_rng_type *Type;
    gsl_rng *Generator;
    long int MySEED;
    
    //INITIALIZATION OF RANDOM NUMBER GENERATOR
    void Init(long int SEED){
        
        gsl_rng_env_setup();
        
        Type=gsl_rng_mt19937;
        Generator=gsl_rng_alloc(Type);
        
        MySEED=SEED;
        
        gsl_rng_set(Generator,SEED);
        
    }
    
    //UNIFORM DISTRIBUTED RANDOM NUMBER
    double rng(){
        return gsl_rng_uniform(Generator);
    }
    
    //UNIFORM DISTRIBUTED RANDOM NUMBER
    double Uniform(double Min, double Max){
        return Max*gsl_rng_uniform(Generator)+Min;
    }
    
    //GAUSSIAN DISTRIBUTED RANDOM NUMBER
    double Gauss(){
        return gsl_ran_gaussian(Generator,1.0);
    }
    
    //GAUSSIAN DISTRIBUTED RANDOM NUMBER
    double Gauss(double Amplitude){
        return gsl_ran_gaussian(Generator,Amplitude);
    }
    
    //GAUSSIAN DISTRIBUTED RANDOM NUMBER
    double RaleighRNG(double Amplitude){
        return gsl_ran_rayleigh(Generator,Amplitude);
    }
    
    //GAUSSIAN DISTRIBUTED RANDOM NUMBER
    std::complex<double> ComplexGauss(){
        
        double Re=gsl_ran_gaussian(Generator,1.0); double Im=gsl_ran_gaussian(Generator,1.0);
        
        return std::complex<double>(Re,Im)/D_SQRT2;
    }

    
    
}
