/**************************************************************
 !*       Purpose: This program computes Struve function       *
 !*                H0(x) using subroutine STVH0                *
 !*       Input :  x   --- Argument of H0(x) ( x ò 0 )         *
 !*       Output:  SH0 --- H0(x)                               *
 !*       Example:                                             *
 !*                   x          H0(x)                         *
 !*                ----------------------                      *
 !*                  0.0       .00000000                       *
 !*                  5.0      -.18521682                       *
 !*                 10.0       .11874368                       *
 !*                 15.0       .24772383                       *
 !*                 20.0       .09439370                       *
 !*                 25.0      -.10182519                       *
 !* ---------------------------------------------------------- *
 !* REFERENCE: "Fortran Routines for Computation of Special    *
 !*             Functions, jin.ece.uiuc.edu/routines/routines  *
 !*             .html".                                        *
 !*                                                            *
 !*                          C++ Release By J-P Moreau, Paris. *
 !*                                  (www.jpmoreau.fr)         *
 !*************************************************************/
double StruveH0(double X) {
    /*      =============================================
     !       Purpose: Compute Struve function H0(x)
     !       Input :  x   --- Argument of H0(x) ( x ò 0 )
     !       Output:  SH0 --- H0(x)
     !       ============================================= */
    double A0=0.0,BY0=0.0,P0=0.0,Q0=0.0,R=0.0,S=0.0,T=0.0,T2=0.0,TA0=0.0;
    int K=0,KM=0;
    
    S=1.0;
    R=1.0;
    if (X <= 20.0) {
        A0=2.0*X/PI;
        for (K=1; K<61; K++) {
            R=-R*X/(2.0*K+1.0)*X/(2.0*K+1.0);
            S=S+R;
            if (fabs(R) < fabs(S)*1.0e-12) goto e15;
        }
    e15:       return A0*S;
    }
    else {
        KM=int(0.5*(X+1.0));
        if (X >= 50.0) KM=25;
        for (K=1; K<=KM; K++) {
            R=-R*pow((2.0*K-1.0)/X,2);
            S=S+R;
            if (fabs(R) < fabs(S)*1.0e-12) goto e25;
        }
    e25:       T=4.0/X;
        T2=T*T;
        P0=((((-.37043e-5*T2+.173565e-4)*T2-.487613e-4)*T2+.17343e-3)*T2-0.1753062e-2)*T2+.3989422793;
        Q0=T*(((((.32312e-5*T2-0.142078e-4)*T2+0.342468e-4)*T2-0.869791e-4)*T2+0.4564324e-3)*T2-0.0124669441);
        TA0=X-0.25*PI;
        BY0=2.0/sqrt(X)*(P0*sin(TA0)+Q0*cos(TA0));
        return 2.0/(PI*X)*S+BY0;
    }
}
/**************************************************************
 !*       Purpose: This program computes Struve function       *
 !*                H1(x) using subroutine STVH1                *
 !*       Input :  x   --- Argument of H1(x) ( x ò 0 )         *
 !*       Output:  SH1 --- H1(x)                               *
 !*       Example:                                             *
 !*                   x          H1(x)                         *
 !*                -----------------------                     *
 !*                  0.0       .00000000                       *
 !*                  5.0       .80781195                       *
 !*                 10.0       .89183249                       *
 !*                 15.0       .66048730                       *
 !*                 20.0       .47268818                       *
 !*                 25.0       .53880362                       *
 !* ---------------------------------------------------------- *
 !* REFERENCE: "Fortran Routines for Computation of Special    *
 !*             Functions, jin.ece.uiuc.edu/routines/routines  *
 !*             .html".                                        *
 !*                                                            *
 !*                          C++ Release By J-P Moreau, Paris. *
 !*                                  (www.jpmoreau.fr)         *
 !*************************************************************/

double StruveH1(double X) {
    /*      =============================================
     !       Purpose: Compute Struve function H1(x)
     !       Input :  x   --- Argument of H1(x) ( x ò 0 )
     !       Output:  SH1 --- H1(x)
     !       ============================================= */
    double A0=0.0,BY1=0.0,P1=0.0,Q1=0.0,R=0.0,S=0.0,T=0.0,T2=0.0,TA1=0.0;
    int K=0,KM=0;
    
    R=1.0;
    if (X <= 20.0) {
        S=0.0;
        A0=-2.0/PI;
        for (K=1; K<=60; K++) {
            R=-R*X*X/(4.0*K*K-1.0);
            S=S+R;
            if (fabs(R) < fabs(S)*1.0e-12) goto e15;
        }
    e15:       return A0*S;
    }
    else {
        S=1.0;
        KM=int(0.5*X);
        if (X > 50.0)  KM=25;
        for (K=1; K<=KM; K++) {
            R=-R*(4.0*K*K-1.0)/(X*X);
            S=S+R;
            if (fabs(R) < fabs(S)*1.0e-12) goto e25;
        }
    e25:       T=4.0/X;
        T2=T*T;
        P1=((((0.42414e-5*T2-0.20092e-4)*T2+0.580759e-4)*T2-0.223203e-3)*T2+0.29218256e-2)*T2+0.3989422819;
        Q1=T*(((((-0.36594e-5*T2+0.1622e-4)*T2-0.398708e-4)*T2+0.1064741e-3)*T2-0.63904e-3)*T2+0.0374008364);
        TA1=X-0.75*PI;
        BY1=2.0/sqrt(X)*(P1*sin(TA1)+Q1*cos(TA1));
        return 2.0/PI*(1.0+S/(X*X))+BY1;
    }
}

double GAMMA(double X) {
    
    double GA=0.0,GR=0.0,R=0.0,Z=0.0;
    double G[27];
    int K=0,M=0,M1=0;
    
    if (X==int(X))
        if (X>0.0) {
            GA=1.0;
            M1=(int) (X-1);
            for (K=2; K<=M1; K++)  GA=GA*K;
        }
        else
            GA=1e+100;
        else {
            if (fabs(X)>1.0) {
                Z=fabs(X);
                M=int(Z);
                R=1.0;
                for (K=1; K<=M; K++)  R=R*(Z-K);
                Z=Z-M;
            }
            else
                Z=X;
            
            G[1]=1.0; G[2]=0.5772156649015329;
            G[3]=-0.6558780715202538; G[4]=-0.420026350340952e-1;
            G[5]=0.1665386113822915; G[6]=-0.421977345555443e-1;
            G[7]=-0.96219715278770e-2; G[8]=0.72189432466630e-2;
            G[9]=-0.11651675918591e-2; G[10]=-0.2152416741149e-3;
            G[11]=0.1280502823882e-3; G[12]=-0.201348547807e-4;
            G[13]=-0.12504934821e-5; G[14]=0.11330272320e-5;
            G[15]=-0.2056338417e-6; G[16]=0.61160950e-8;
            G[17]=0.50020075e-8; G[18]=-0.11812746e-8;
            G[19]=0.1043427e-9; G[20]=0.77823e-11;
            G[21]=-0.36968e-11; G[22]=0.51e-12;
            G[23]=-0.206e-13; G[24]=-0.54e-14;
            G[25]=0.14e-14; G[26]=0.1e-15;
            
            GR=G[26];
            for (K=25; K>0; K--) GR=GR*Z+G[K];
            GA=1.0/(GR*Z);
            if (fabs(X)>1.0) {
                GA=GA*R;
                if (X < 0.0)  GA=-PI/(X*GA*sin(PI*X));
            }
        }
    return GA;
}

double StruveH(double V, double X) {
    
    double GA=0.0,GB=0.0,PU0=0.0,PU1=0.0,QU0=0.0,QU1=0.0,R1=0.0,R2=0.0,S=0.0,S0=0.0,SA=0.0,T0=0.0,T1=0.0,U=0.0,U0=0.0,V0=0.0,VA=0.0,VB=0.0,VT=0;
    double BF=0.0,BF0=0.0,BF1=0.0,BY0=0.0,BY1=0.0,BYV=0.0,SR=0;
    int K=0,L=0,N=0;
    
    if (X == 0.0) {
        if (V > -1.0 || int(V)-V == 0.5){
            return 0.0;
        }
        else if (V < -1.0){
            return std::pow(-1, int(0.5-V)-1)*1.0e+300;
        }
        else if (V == -1.0){
            return 2.0/PI;
        }
    }
    if (X <= 20.0) {
        V0=V+1.5;
        GA = GAMMA(V0);
        S=2.0/(sqrt(PI)*GA);
        R1=1.0;
        for (K=1; K<101; K++) {
            VA=K+1.5;
            GA = GAMMA(VA);
            VB=V+K+1.5;
            GB = GAMMA(VB);
            R1=-R1*pow(0.5*X,2);
            R2=R1/(GA*GB);
            S=S+R2;
            if (fabs(R2) < fabs(S)*1.0e-12) goto e15;
        }
    e15:       return pow(0.5*X,V+1.0)*S;
    }
    else {
        SA=pow(0.5*X, V-1.0)/PI;
        V0=V+0.5;
        GA = GAMMA(V0);
        S=sqrt(PI)/GA;
        R1=1.0;
        for (K=1; K<13; K++) {
            VA=K+0.5;
            GA = GAMMA(VA);
            VB=-K+V+0.5;
            GB = GAMMA(VB);
            R1=R1/pow(0.5*X,2);
            S=S+R1*GA/GB;
        }
        S0=SA*S;
        U=fabs(V);
        N=int(U);
        U0=U-N;
        for (L=0; L<2; L++) {
            VT=4.0*pow(U0+L,2);
            R1=1.0;
            PU1=1.0;
            for (K=1; K<13; K++) {
                R1=-0.0078125*R1*(VT-pow(4.0*K-3.0,2))*(VT-pow(4.0*K-1.0,2))/((2.0*K-1.0)*K*X*X);
                PU1=PU1+R1;
            }
            QU1=1.0;
            R2=1.0;
            for (K=1; K<13; K++) {
                R2=-0.0078125*R2*(VT-pow(4.0*K-1.0,2))*(VT-pow(4.0*K+1.0,2))/((2.0*K+1.0)*K*X*X);
                QU1=QU1+R2;
            }
            QU1=0.125*(VT-1.0)/X*QU1;
            if (L == 0) {
                PU0=PU1;
                QU0=QU1;
            }
        }
        T0=X-(0.5*U0+0.25)*PI;
        T1=X-(0.5*U0+0.75)*PI;
        SR=sqrt(2.0/(PI*X));
        BY0=SR*(PU0*sin(T0)+QU0*cos(T0));
        BY1=SR*(PU1*sin(T1)+QU1*cos(T1));
        BF0=BY0;
        BF1=BY1;
        for (K=2; K<=N; K++) {
            BF=2.0*(K-1.0+U0)/X*BF1-BF0;
            BF0=BF1;
            BF1=BF;
        }
        if (N == 0) BYV=BY0;
        if (N == 1) BYV=BY1;
        if (N > 1)  BYV=BF;
        return BYV+S0;
    }
}

