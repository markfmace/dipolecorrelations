/////////////////////////////////////////////////////////
//                                                     //
// COMPUTES FOUR PARTICLE INTEGRATED SPECTRA           //
// TO ALL ORDERS IN THE MV MODEL FOR FIXED (FINITE NC) //
//          WITH MONTE CARLO INTEGRATION               //
//                                                     //
/////////////////////////////////////////////////////////


// NUMBER OF COLORS //
int Nc=3;
// CASIMIR FACTOR //
double Cf=(Nc*Nc-1.0)/(2.0*Nc);
// NUMBER OF WILSON LINES
const static int nWilsonLines=8;
// SIZE OF ONE DIMENSION OF THE MATRIX //
const static int MatrixSize=24;

// DOMAIN SIZE //
double B=4.0; /// DONT CHANGE ///

// LAMBDA QCD//
double LambdaQCD=0.241; // REFERS TO PHENO VALUE OF \Lambda=0.241 GeV

// Qs^2 //
double QsSQR=1.0; 

// POWER FOR NUMBER OF POINTS //
int nPow=4;

// NUMBER OF v_n TO CALCULATE //
int vnMaxPlusOne=3;

// NUMBER OF PARTIAL SUMS TO CALCULATE FOR SIGN
const static int NumSums=16;

// MOMENTA BOUNDS //
// THIS IS ptMax
double ptMin=0.2;
double dpt=0.2;
double ptMax=2.0;

// DEFINE MOMENTA COUNTING//
int ptIndexMax=(ptMax-ptMin)/dpt+0.1;

// DEFINE MATRIX ORDERING //
int MatrixIndex(int i, int j){
    // ROW MAJOR ORDERING //
    return i*MatrixSize+j;
}

// DEFINE MATRIX ORDERING //
int ResultIndex(int config,int pIndex,int n){
    // ROW MAJOR ORDERING //
    return vnMaxPlusOne*(config*(ptIndexMax+1)+pIndex)+n;
}

// DEFINE BESSEL FUNCTION //
double Bessel(int n,double pr){
    
    return gsl_sf_bessel_Jn(n,pr);
}
// RAYLEIGH DISTRIBUTION //
double Rayleigh(double r, double sig)
{
    return r*exp(-r*r/(2*sig*sig))/(sig*sig);
}

#include <iomanip>

// RANDOM NUMBER GENERATOR //
#include "../MISC/GSLRandomNumberGenerator.cpp"
// INTEGRALS BY HAND //
#include "../INTEGRATION/Integration.cpp"
// PROBABILITY DISTRIBUTION FUNCTION //
#include "../PROBABILITY/PDFs.cpp"
// GSL DIAGONALIZATION //
//#include "../MATRIXOPERATIONS/Diagonalization.cpp"
//#include "../MATRIXOPERATIONS/LapackDiagonalization.cpp"

// GSL INVERSION //
//#include "../MATRIXOPERATIONS/Inversion.cpp"
// OUTPUT HANDLING
#include "../IO/StringManipulation.cpp"
#include "../IO/OutputManagement.cpp"
// TIMING //
#include "../MISC/Timing.cpp"
// INCLUDE COMMANDLINE PROCESSING //
#include "../IO/COMMANDLINE/cfile.c"

// CALCULATE GLUON EXCHANGE MATRIX //
#include "../MATRIXOPERATIONS/FullMatrixEvaluationFourDipoles.cpp"
//#include "../MATRIXOPERATIONS/nGluonMatrixEvaluationFourDipoles.cpp"



namespace Simulation {
    
    // MPI RANDOM NUMBER SEED //
    int MY_MPI_RNG_SEED;
    
    //////////////////////////
    //SIMULATION PROCDEDURE //
    //////////////////////////
    
    void Run(int MPI_RNG_SEED){
        
        ///////////
        // SETUP //
        ///////////
        
        //SET SEED //
        MY_MPI_RNG_SEED=MPI_RNG_SEED;
        
        //INITIALIZE RANDOM NUMBER GENERATOR //
        RandomNumberGenerator::Init(MY_MPI_RNG_SEED);
        
        // OUTPUT //
        std::cout << "#BEGINNING FOUR DIPOLE D24 SPECTRA CALCULATION ID=" << MY_MPI_RNG_SEED << std::endl;
        
        ////////
        // Qs //
        ////////
        // SET g4muSQR based on QsSQR
        double Q02=QsSQR/std::log(1.0/(LambdaQCD*std::sqrt(2.0/QsSQR))+std::exp(1.0));
        
        double g4muSQR=4.0*PI*QsSQR/Cf;
        
        std::cerr << "#QsSQR=" << QsSQR << " #Q02=" << Q02 << " #g4muSQR=" << g4muSQR << std::endl;
        
        ///////////////////
        // VARIABLES     //
        ///////////////////
        
        // RADIAL VARIABLES //
        double r1,r2,r3,r4;
        double r1x,r1y,r2x,r2y,r3x,r3y,r4x,r4y;
        
        // COM VARIABLES //
        double R1x,R1y,R2x,R2y,R3x,R3y,R4x,R4y;
        
        // ANGULAR VARIABLES //
        double phir1,phir2,phir3,phir4,phi,psi12,psi34,psi;
        int dpsiIndexMax=32;
        double dpsi=2.0*PI/double(dpsiIndexMax);
        
        // GENERAL POSITION VECTORS //
        double **x;
        x = new double*[nWilsonLines]; // FIRST INDEX //
        for(int i=0;i<nWilsonLines;i++) {
            x[i] = new double[2]; // SECOND INDEX //
        }
        
        // SET TO ZERO INITIALLY
        for(int i=0;i<nWilsonLines;i++){
            for(int j=0;j<2;j++){
                x[i][j]=0.0;
            }
        }
        
        // MAX TOTAL COUNT //
        int iMax=std::pow(10,nPow);
        
        // MC INTEGRATED QUANTITIES //
        double Integrand;
        double* Mean=new double[NumSums];
        double* IntegrandSqr=new double[NumSums];

        ///////////////////
        // MAKE LOG FILE //
        ///////////////////
        
        std::ofstream LogStream;
        LogStream.open(StringManipulation::StringCast(IO::OutputDirectory,"InfoID",MY_MPI_RNG_SEED,".log").c_str());
        
        LogStream.precision(OUTPUT_PRECISION);
        
        // INFO FILE  //
        LogStream << "#PARAMETER LOG FOR MC FOUR DIPOLE D24 SPECTRA" << std::endl;
        LogStream << "#iMax=" << iMax << std::endl;
        LogStream << "#Nc=" << Nc << std::endl;
        LogStream << "#nWilsonLines=" << nWilsonLines << std::endl;
        LogStream << "#LambdaQCD=" << LambdaQCD << std::endl;
        LogStream << "#B=" << B << std::endl;
        LogStream << "#g4muSQR=" << g4muSQR << std::endl;
        LogStream << "#ptMin=" << ptMin << std::endl;
        LogStream << "#ptMax=" << ptMax << std::endl;
        LogStream << "#dpt=" << dpt << std::endl;
        LogStream << "#ptIndexMax=" << ptIndexMax << std::endl;
        LogStream << "#dpsiIndexMax=" << dpsiIndexMax << std::endl;
        
        std::cerr << "#LambdaQCD=" << LambdaQCD << " #Nc=" << Nc << " #B=" << B << " #g4muSQR=" << g4muSQR << " #iMax=" << iMax << std::endl;
        
        // CLOSE OUT-STREAM //
        LogStream.close();
        
        // COMMANDLINE OUTPUT //
        std::cout << "#COMPUTING VALUES" << std::endl;
        
        // RESET TIMING //
        Timing::Reset();
        
        std::cout << "#CALCULATING NORMS" << std::endl;
        
        // SET NORMS //
        double* NormDif=new double[vnMaxPlusOne*(ptIndexMax+1)*NumSums];
        
        // SET ZERO //
        for(int sign=0;sign<NumSums;sign++){
            for(int ptIndex=0;ptIndex<=ptIndexMax;ptIndex++){
                for(int n=0;n<vnMaxPlusOne;n+=2){

                    NormDif[ResultIndex(sign,ptIndex,n)]=0.0;
                    
                }
                
            }
        }
        
        // DO INTEGRALS FOR DIFFERENTIAL PART //
        for(int ptIndex=0;ptIndex<=ptIndexMax;ptIndex++){
            
            // BEGIN BY HAND OPTION //
            double pt=ptMin+dpt*ptIndex;
            
            // n=0 //
            // SIGN = +1 | J_n>0 //
            NormDif[ResultIndex(0,ptIndex,0)]=PDF::GetNormalization(0,pt,0,B);
            
            // SIGN = -1 | J_n<0//
            NormDif[ResultIndex(1,ptIndex,0)]=PDF::GetNormalization(1,pt,0,B);
            
            // n=2 //
            // SIGN = +1 | J_n>0 //
            NormDif[ResultIndex(0,ptIndex,2)]=PDF::GetNormalization(0,pt,2,B);
            
            // SIGN = -1 | J_n<0//
            NormDif[ResultIndex(1,ptIndex,2)]=PDF::GetNormalization(1,pt,2,B);
            // END BY HAND OPTION //
            
            std::cout << "#FINISHED CALCULATING DIFFERENTIAL NORMS pT=" << pt << std::endl;

            
        }
        
        // GET TIMING
        double time=Timing::Get();
        
        std::cerr << "#IN t=" << time << " s NORMS WERE CALCULATED" << std::endl;
        
        // COMMANDLINE OUTPUT //
        std::cout << "#COMPUTING VALUES" << std::endl;
        
        // RESET TIMING //
        Timing::Reset();
        // INDEX FOR BOTH POSITIVE POSITIVE, NEGATIVE NEGATIVE, AND POSITIVE NEGATIVE VALUES
        int i[NumSums];
        
        // DEFINE SIGN VARIABLES //
        int IntegrandSign=0, sign1=0, sign2=0, sign3=0, sign4=0;
        
        // FILE OUTPUT
        std::ofstream OutputStream;
        
        OutputStream.open(StringManipulation::StringCast(IO::OutputDirectory,"IntegratedSpectra_QsSQR",QsSQR,"ID",MY_MPI_RNG_SEED,".txt").c_str());
        
        OutputStream.precision(OUTPUT_PRECISION);
        
        // GET AVERAGE AND VALUES FOR ERROR //
        for(int ptIndex=0;ptIndex<=ptIndexMax;ptIndex++){
            
            // DEFINE MOMENTUM //
            double pt=ptMin+dpt*ptIndex;
            
            // OUTPUT MOMENTA //
            OutputStream << pt << " ";
            
            // INTEGRATE OVER iMax POINTS //
            // RESET INITIAL COUNT //
            for(int n=0;n<vnMaxPlusOne;n+=2){
                
                std::cerr << "#ptIndex=" << ptIndex << " n=" << n << std::endl;
                
                // RESET COUNTS //
                for(int config=0;config<NumSums;config++){
                    i[config]=0;
                    Mean[config]=0.0;
                    IntegrandSqr[config]=0.0;
                }
                // TOTAL POINTS COUNTED //
                int iTot;
                // COUNT TO TOTAL OF iMax //
                do{
                    // RESET COUNTER
                    iTot=0;
                    
                    // DIFFERNETIAL VARIABLE //
                    r1=PDF::SamplePDistrubtion(pt,n,B);
                    r2=PDF::SamplePDistrubtion(pt,n,B);
                    r3=PDF::SamplePDistrubtion(pt,n,B);
                    r4=PDF::SamplePDistrubtion(pt,n,B);


                    // SET RADIUS VECTOR //
                    R1x= RandomNumberGenerator::Gauss(sqrt(B/2.0));
                    R1y= RandomNumberGenerator::Gauss(sqrt(B/2.0));
                    R2x= RandomNumberGenerator::Gauss(sqrt(B/2.0));
                    R2y= RandomNumberGenerator::Gauss(sqrt(B/2.0));
                    R3x= RandomNumberGenerator::Gauss(sqrt(B/2.0));
                    R3y= RandomNumberGenerator::Gauss(sqrt(B/2.0));
                    R4x= RandomNumberGenerator::Gauss(sqrt(B/2.0));
                    R4y= RandomNumberGenerator::Gauss(sqrt(B/2.0));
                    
                    // FIRST ANGLE //
                    psi12=2.0*PI*RandomNumberGenerator::rng();
                    psi34=2.0*PI*RandomNumberGenerator::rng();
                    phi=2.0*PI*RandomNumberGenerator::rng();

                    // RESET //
                    Integrand = 0;
                    
                    //TRAPEZOID RULE ON PSI INTEGRAL
                    for(int dpsiIndex=0;dpsiIndex<=dpsiIndexMax;dpsiIndex++){
                        
                        // SECONARDY ANGLE //
                        psi=dpsiIndex*dpsi;
                        
                        // DEFINE AZIMUTHAL ANGLES //
                        phir1=0.25*(2.0*phi+psi+2.0*psi12);
                        phir2=0.25*(2.0*phi+psi-2.0*psi12);
                        phir3=0.25*(2.0*phi-psi+2.0*psi34);
                        phir4=0.25*(2.0*phi-psi-2.0*psi34);
                        
                        r1x=r1*cos(phir1);
                        r1y=r1*sin(phir1);
                        r2x=r2*cos(phir2);
                        r2y=r2*sin(phir2);
                        r3x=r3*cos(phir3);
                        r3y=r3*sin(phir3);
                        r4x=r4*cos(phir4);
                        r4y=r4*sin(phir4);
                        
                        // SET ACUTAL POINTS FOR DIPOLE CORRELATOR
                        x[0][0]=(R1x+0.5*r1x);  x[0][1]=(R1y+0.5*r1y);
                        x[1][0]=(R1x-0.5*r1x);  x[1][1]=(R1y-0.5*r1y);
                        x[2][0]=(R2x+0.5*r2x);  x[2][1]=(R2y+0.5*r2y);
                        x[3][0]=(R2x-0.5*r2x);  x[3][1]=(R2y-0.5*r2y);
                        x[4][0]=(R3x+0.5*r3x);  x[4][1]=(R3y+0.5*r3y);
                        x[5][0]=(R3x-0.5*r3x);  x[5][1]=(R3y-0.5*r3y);
                        x[6][0]=(R4x+0.5*r4x);  x[6][1]=(R4y+0.5*r4y);
                        x[7][0]=(R4x-0.5*r4x);  x[7][1]=(R4y-0.5*r4y);
                        
                        double SimpsonCoeff=(dpsiIndex==0 || dpsiIndex==dpsiIndexMax ? 1.0 : 2.0+2.0*(dpsiIndex%2))*dpsi/(6.0*PI);
                        
                        // SET MATRIX EXPONENTIAL RESULT //
                        //Integrand+=MatrixEvaluation(x,g4muSQR)*cos(double(n)*psi)*dpsi/(2.0*PI);
                        Integrand+=SimpsonCoeff*MatrixEvaluation(x,g4muSQR,LambdaQCD)*cos(double(n)*psi)*dpsi;

                    }

                    // DETERMINE SIGN OF EACH DISTRIBUTION //
                    // DIFFERENTIAL VARIABLES //
                    sign1=int(0.5*(1.0-PDF::P(n,r1,pt,B)/sqrt(PDF::P(n,r1,pt,B)*PDF::P(n,r1,pt,B))));
                    sign2=int(0.5*(1.0-PDF::P(n,r2,pt,B)/sqrt(PDF::P(n,r2,pt,B)*PDF::P(n,r2,pt,B))));
                    sign3=int(0.5*(1.0-PDF::P(n,r3,pt,B)/sqrt(PDF::P(n,r3,pt,B)*PDF::P(n,r3,pt,B))));
                    sign4=int(0.5*(1.0-PDF::P(n,r4,pt,B)/sqrt(PDF::P(n,r4,pt,B)*PDF::P(n,r4,pt,B))));
                    
                    // DETERMINE BIN //
                    IntegrandSign=2.0*(2.0*(2.0*sign1+sign2)+sign3)+sign4;
                    
                    // INCREMENT COUNTER FOR GIVEN BIN //
                    i[IntegrandSign]++;
                    // MULTIPLY BY DISTR NORMS //
                    Integrand*=NormDif[ResultIndex(sign1,ptIndex,n)]*NormDif[ResultIndex(sign2,ptIndex,n)]*NormDif[ResultIndex(sign3,ptIndex,n)]*NormDif[ResultIndex(sign4,ptIndex,n)];
                    // DETERMINE MEAN AND VARIANCE //
                    Mean[IntegrandSign]+=(Integrand-Mean[IntegrandSign])/double(i[IntegrandSign]);
                    IntegrandSqr[IntegrandSign]+=Integrand*Integrand;
                    
                    for(int j=0;j<NumSums;j++){
                        iTot+=i[j];
                    }
                    
                    //std::cerr << iTot << std::endl;
                    
                }while(iTot<iMax);
                

                // COMMANDLINE OUTPUT //
                std::cerr << "#i= ";
                
                for(int j=0;j<NumSums;j++){
                    std::cerr << i[j] << " ";
                }
                std::cerr << " PTS COMPUTED " << std::endl;
                
                // UNCERTAINTY OF THE MEAN //
                double varSqrMean[NumSums];
                // POSITIVE POSITIVE VALUES
                for(int config=0;config<NumSums;config++){
                    if(i[0]!=0){
                        varSqrMean[config]=(IntegrandSqr[config]-double(i[config])*(Mean[config]*Mean[config]))/ (double(i[config])*(double(i[config])-1.0));
                    }
                    else{
                        varSqrMean[config]=0.0;
                    }
                    
                }
                
                double TotalMean=0.0;
                // SUM OVER BINS //
                for(int config=0;config<NumSums;config++){
                    if(i[config]>=100){
                        TotalMean+=Mean[config];
                    }
                }
                double TotalUncertainty=0.0;
                double TempUncertainty=0.0;
                for(int config=0;config<NumSums;config++){
                    if(i[config]>=100){
                        TempUncertainty+=sqrt(varSqrMean[config]*varSqrMean[config]);
                    }
                }
                TotalUncertainty=sqrt(TempUncertainty);
                // OUTPUT DATA AND UNCERTAINTY //
                OutputStream << TotalMean << " " << TotalUncertainty << " ";
                
            }
            // NEW LINE //
            OutputStream << std::endl;
            
        }
        
        // CLOSE OUT-STREAM //
        OutputStream.close();
        
        // COMMANDLINE OUTPUT //
        std::cout << "#FINISHED COMPUTING VALUES" << std::endl;
        
        // CLEAN-UP //
        delete[] Mean;
        delete[] IntegrandSqr;
        
        delete[] NormDif;
        
        for(int j=0;j<nWilsonLines;j++){
            delete[] x[j];
        }
        // COMMANDLINE OUTPUT //
        std::cout << "#DONE SAVING VALUES ID=" << MY_MPI_RNG_SEED << std::endl;
        
        
    }
}
