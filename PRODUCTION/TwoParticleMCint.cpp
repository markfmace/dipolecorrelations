/////////////////////////////////////////////////////////
//                                                     //
// COMPUTES TWO PARTICLE INTEGRATED SPECTRA            //
// TO ALL ORDERS IN THE MV MODEL FOR FIXED (FINITE NC) //
//          WITH MONTE CARLO INTEGRATION               //
//                                                     //
/////////////////////////////////////////////////////////


// NUMBER OF COLORS //
int Nc=3;
// CASIMIR FACTOR //
double Cf=(Nc*Nc-1.0)/(2.0*Nc);
// NUMBER OF WILSON LINES
const static int nWilsonLines=4;
// SIZE OF ONE DIMENSION OF THE MATRIX //
const static int MatrixSize=2;

// DOMAIN SIZE //
double B=4.0; /// DONT CHANGE ///

// LAMBDA QCD //
double LambdaQCD=0.241; // REFERS TO PHENO VALUE OF \Lambda=0.241 GeV

// Qs^2 //
double QsSQR=1.0;

// POWER FOR NUMBER OF POINTS //
double nPow=5;

// NUMBER OF v_n TO CALCULATE //
int vnMaxPlusOne=6; // DO NOT CHANGE

// HARMONIC //
int nGlobal=2; // CHANGE AT COMMANDLINE

// NUMBER OF PARTIAL SUMS TO CALCULATE FOR SIGN
const static int NumSums=4;

// MOMENTA BOUNDS //
// THIS IS ptMax
double pMin=2.0;
double dp=1.0;
double pAbsMax=2.0;

// DEFINE MOMENTA COUNTING//
int pMaxIndexMax=(pAbsMax-pMin)/dp+0.1;

// DEFINE MATRIX ORDERING //
int MatrixIndex(int i, int j){
    // ROW MAJOR ORDERING //
    return i*MatrixSize+j;
}


// DEFINE MATRIX ORDERING //
int ResultIndex(int config,int pIndex,int n){
    // ROW MAJOR ORDERING //
    return vnMaxPlusOne*(config*(pMaxIndexMax+1)+pIndex)+n;
}

// DEFINE BESSEL FUNCTION //
double Bessel(int n,double pr){
    
    return gsl_sf_bessel_Jn(n,pr);
}
// RAYLEIGH DISTRIBUTION //
double Rayleigh(double r, double sig)
{
    return r*exp(-r*r/(2*sig*sig))/(sig*sig);
}

#include <iomanip>

// RANDOM NUMBER GENERATOR //
#include "../MISC/GSLRandomNumberGenerator.cpp"

// INTEGRALS BY HAND //
#include "../INTEGRATION/Integration.cpp"
// PROBABILITY DISTRIBUTION FUNCTION //
#include "../PROBABILITY/PDFs.cpp"
// GSL DIAGONALIZATION //
//#include "../MATRIXOPERATIONS/Diagonalization.cpp"
// GSL INVERSION //
//#include "../MATRIXOPERATIONS/Inversion.cpp"
// OUTPUT HANDLING
#include "../IO/StringManipulation.cpp"
#include "../IO/OutputManagement.cpp"
// TIMING //
#include "../MISC/Timing.cpp"
// INCLUDE COMMANDLINE PROCESSING //
#include "../IO/COMMANDLINE/cfile.c"

// CALCULATE GLUON EXCHANGE MATRIX //
#include "../MATRIXOPERATIONS/FullMatrixEvaluation.cpp"
//#include "../MATRIXOPERATIONS/FullMatrixEvaluation_SEPARATED.cpp"

//#include "../MATRIXOPERATIONS/TwoDipoleQED.cpp"

/*
 #if Nc==1
 #include "../MATRIXOPERATIONS/TwoDipoleQED.cpp"
 #else
 #include "../MATRIXOPERATIONS/FullMatrixEvaluation.cpp"
 #endif
 */


namespace Simulation {
    
    // MPI RANDOM NUMBER SEED //
    int MY_MPI_RNG_SEED;
    
    //////////////////////////
    //SIMULATION PROCDEDURE //
    //////////////////////////
    
    void Run(int MPI_RNG_SEED){
        
        ///////////
        // SETUP //
        ///////////
        
        //SET SEED //
        MY_MPI_RNG_SEED=MPI_RNG_SEED;
        
        //INITIALIZE RANDOM NUMBER GENERATOR //
        RandomNumberGenerator::Init(MY_MPI_RNG_SEED);
        
        // OUTPUT //
        std::cout << "#BEGINNING TWO DIPOLE INTEGRATED SPECTRA CALCULATION ID=" << MY_MPI_RNG_SEED << std::endl;
        
        ////////
        // Qs //
        ////////
        // SET g4muSQR based on QsSQR
        double Q02=QsSQR/std::log(1.0/(LambdaQCD*std::sqrt(2.0/QsSQR))+std::exp(1.0));
        
        double g4muSQR=4.0*PI*Q02/Cf;
        //double g4muSQR=QsSQR;//4.0*PI*Q02;
        
        std::cerr << "#QsSQR=" << QsSQR << " #Q02=" << Q02 << " #g4muSQR=" << g4muSQR << std::endl;
        
        ///////////////////
        // VARIABLES     //
        ///////////////////
        
        // RADIAL VARIABLES //
        double r1,r2;
        double r1x,r1y,r2x,r2y;
        
        // COM VARIABLES //
        double R1x,R1y,R2x,R2y;
        
        // ANGULAR VARIABLES //
        double phir1,phir2,theta,psi;
        int dpsiIndexMax=32;
        double dphi=2.0*PI/double(dpsiIndexMax);
        
        // DIPOLE VARIABLES //
        double x1,y1,x2,y2,x3,y3,x4,y4;
        
        // MAX TOTAL COUNT //
        int iMax=std::pow(10,nPow);
        
        ///////////////////
        // MAKE LOG FILE //
        ///////////////////
        
        std::ofstream LogStream;
        LogStream.open(StringManipulation::StringCast(IO::OutputDirectory,"InfoID",MY_MPI_RNG_SEED,".log").c_str());
        
        LogStream.precision(OUTPUT_PRECISION);
        
        // INFO FILE  //
        LogStream << "#PARAMETER LOG FOR MC TWO DIPOLE INTEGRATED SPECTRA" << std::endl;
        LogStream << "#iMax=" << iMax << std::endl;
        LogStream << "#Nc=" << Nc << std::endl;
        LogStream << "#nWilsonLines=" << nWilsonLines << std::endl;
        LogStream << "#LambdaQCD=" << LambdaQCD << std::endl;
        LogStream << "#B=" << B << std::endl;
        LogStream << "#QsSQR=" << QsSQR << std::endl;
        LogStream << "#g4muSQR=" << g4muSQR << std::endl;
        LogStream << "#pMin=" << pMin << std::endl;
        LogStream << "#pAbsMax=" << pAbsMax << std::endl;
        LogStream << "#dp=" << dp << std::endl;
        LogStream << "#pMaxIndexMax=" << pMaxIndexMax << std::endl;
        LogStream << "#dpsiIndexMax=" << dpsiIndexMax << std::endl;
        
        std::cerr << "#LambdaQCD=" << LambdaQCD << " #Nc=" << Nc << " #Cf=" << Cf << " #B=" << B << " #g4muSQR=" << g4muSQR << " #nGlobal=" << nGlobal << " #iMax=" << iMax << std::endl;
        
        // CLOSE OUT-STREAM //
        LogStream.close();
        
        // COMMANDLINE OUTPUT //
        std::cout << "#COMPUTING VALUES" << std::endl;
        
        // RESET TIMING //
        Timing::Reset();
        
        std::cout << "#CALCULATING NORMS" << std::endl;
        
        // MC INTEGRATED QUANTITIES //
        double Integrand;
        double* Mean = new double[NumSums];
        double* IntegrandSqr = new double[NumSums];
        
        // SET ZERO //
        for(int config=0;config<NumSums;config++){
            
            Mean[config]=0.0;
            IntegrandSqr[config]=0.0;
            
        }
        
        // SET NORMS //
        double* Norm=new double[(vnMaxPlusOne)*(pMaxIndexMax+1)*(NumSums+1)];
        
        // SET ZERO //
        for(int sign=0;sign<2;sign++){
            for(int pIndex=0;pIndex<=pMaxIndexMax;pIndex++){
                for(int n=0;n<vnMaxPlusOne;n++){
                    
                    Norm[ResultIndex(sign,pIndex,n)]=0.0;
                    
                }
                
            }
        }
        // IntegralFlag= 0 KERN>0, 1 KERN<0, 2 KERN, 3 abs(KERN), //
        
        // DO INTEGRATIONS //
        for(int pMaxIndex=0;pMaxIndex<=pMaxIndexMax;pMaxIndex++){
            for(int n=0;n<vnMaxPlusOne;n++){
                if(n==nGlobal){
                    for(int sign=0;sign<2;sign++){
                        
                        // BEGIN BY HAND OPTION //
                        double pMax=pMin+dp*pMaxIndex;
                        
                        // SIGN = +1 | J_n>0 --SIGN = -1 | J_n<0//
                        Norm[ResultIndex(sign,pMaxIndex,n)]=PDF::GetNormalizationIntegrated(sign,pMax,n,B);
                        
                    }
                }
            }
        }
        
        // GET TIMING
        double time=Timing::Get();
        
        std::cerr << "#IN t=" << time << " s NORMS WERE CALCULATED" << std::endl;
        
        // COMMANDLINE OUTPUT //
        std::cout << "#COMPUTING VALUES" << std::endl;
        
        // RESET TIMING //
        Timing::Reset();
        
        // INDEX FOR BOTH POSITIVE POSITIVE, NEGATIVE NEGATIVE, AND POSITIVE NEGATIVE VALUES
        int i[NumSums];
        
        // DEFINE SIGN VARIABLES //
        int IntegrandSign=0, sign1=0, sign2=0;
        
        // FILE OUTPUT
        std::ofstream OutputStream;
        
        OutputStream.open(StringManipulation::StringCast(IO::OutputDirectory,"IntegratedSpectra_n",nGlobal,"QsSQR",QsSQR,"ID",MY_MPI_RNG_SEED,".txt").c_str());
        
        OutputStream.precision(OUTPUT_PRECISION);
        
        // GET AVERAGE AND VALUES FOR ERROR //
        for(int pMaxIndex=0;pMaxIndex<=pMaxIndexMax;pMaxIndex++){
            
            // DEFINE MOMENTUM //
            double pMax=pMin+dp*pMaxIndex;
            
            // OUTPUT MOMENTA //
            OutputStream << pMax << " " << QsSQR << " ";
            
            // INTEGRATE OVER iMax POINTS //
            // RESET INITIAL COUNT //
            for(int n=0;n<vnMaxPlusOne;n++){
                if(n==nGlobal){
                    // pt-0 c0-1 dc0-2 c2-3 dc2-4 c3-5 dc3-6 c4-7 dc4-8 c5-9 dc5-10 c6-11 dc6-12
                    std::cerr << "#pMaxIndex=" << pMaxIndex << " n=" << n << std::endl;
                    
                    // RESET COUNTS //
                    for(int config=0;config<NumSums;config++){
                        i[config]=0;
                        Mean[config]=0.0;
                        IntegrandSqr[config]=0.0;
                    }
                    // TOTAL POINTS COUNTED //
                    int iTot;
                    // COUNT TO TOTAL OF iMax //
                    do{
                        // RESET COUNTER
                        iTot=0;
                        //double limit=B/2.0;
                        //do{
                            r1=PDF::SamplePDistrubtionInt(pMax,n,B);
                            r2=PDF::SamplePDistrubtionInt(pMax,n,B);
                            
                            // SET RADIUS VECTOR //
                            R1x=RandomNumberGenerator::Gauss(sqrt(B/2.0));
                            R1y=RandomNumberGenerator::Gauss(sqrt(B/2.0));
                            R2x=RandomNumberGenerator::Gauss(sqrt(B/2.0));
                            R2y=RandomNumberGenerator::Gauss(sqrt(B/2.0));
                            
                        //}while(sqrt(std::pow((R1x-0.5*r1)-(R2x-0.5*r2),2)+std::pow((R1y+0.5*r1)-(R2y+0.5*r2),2))>limit || sqrt(std::pow((R1x-0.5*r1)-(R2x-0.5*r2),2)+std::pow((R1y-0.5*r1)-(R2y-0.5*r2),2))>limit);
                        
                        
                        // FIRST ANGLE //
                        theta=2.0*PI*RandomNumberGenerator::rng();
                        
                        // RESET //
                        Integrand = 0;
                        
                        
                        //TRAPEZOID RULE ON PSI INTEGRAL
                        for(int dpsiIndex=0;dpsiIndex<=dpsiIndexMax;dpsiIndex++){
                            
                            // SECONARDY ANGLE //
                            psi=dpsiIndex*dphi;
                            
                            // DEFINE AZIMUTHAL ANGLES //
                            phir1=theta+psi/2.0;
                            phir2=theta-psi/2.0;
                            
                            r1x=r1*cos(phir1);
                            r1y=r1*sin(phir1);
                            r2x=r2*cos(phir2);
                            r2y=r2*sin(phir2);
                            
                            // SET ACUTAL POINTS FOR DIPOLE CORRELATOR
                            x1=(R1x+0.5*r1x);
                            y1=(R1y+0.5*r1y);
                            x2=(R1x-0.5*r1x);
                            y2=(R1y-0.5*r1y);
                            x3=(R2x+0.5*r2x);
                            y3=(R2y+0.5*r2y);
                            x4=(R2x-0.5*r2x);
                            y4=(R2y-0.5*r2y);

                            double SimpsonCoeff=(dpsiIndex==0 || dpsiIndex==dpsiIndexMax ? 1.0 : 2.0+2.0*(dpsiIndex%2))*dphi/(6.0*PI);

                            // SET MATRIX EXPONENTIAL RESULT //
                            Integrand+=SimpsonCoeff*MatrixEvaluation(x1,y1,x2,y2,x3,y3,x4,y4,g4muSQR,LambdaQCD)*cos(double(n)*psi);

                            
                        }
                        
                        // DETERMINE SIGN OF EACH DISTRIBUTION //
                        sign1=int(0.5*(1.0-PDF::PInt(n,r1,pMax,B)/sqrt(PDF::PInt(n,r1,pMax,B)*PDF::PInt(n,r1,pMax,B))));
                        sign2=int(0.5*(1.0-PDF::PInt(n,r2,pMax,B)/sqrt(PDF::PInt(n,r2,pMax,B)*PDF::PInt(n,r2,pMax,B))));
                        // DETERMINE BIN //
                        IntegrandSign=2*sign1+sign2;
                        
                        // INCREMENT COUNTER FOR GIVEN BIN //
                        i[IntegrandSign]++;
                        // MULTIPLY BY DISTR NORMS //
                        Integrand*=Norm[ResultIndex(sign1,pMaxIndex,n)]*Norm[ResultIndex(sign2,pMaxIndex,n)];
                        // DETERMINE MEAN AND VARIANCE //
                        Mean[IntegrandSign]+=(Integrand-Mean[IntegrandSign])/double(i[IntegrandSign]);
                        IntegrandSqr[IntegrandSign]+=Integrand*Integrand;
                        
                        for(int j=0;j<NumSums;j++){
                            iTot+=i[j];
                        }
                        
                    }while(iTot<iMax);
                    
                    
                    // COMMANDLINE OUTPUT //
                    std::cerr << "#i= ";
                    
                    for(int j=0;j<NumSums;j++){
                        std::cerr << i[j] << " ";
                    }
                    std::cerr << " PTS COMPUTED " << std::endl;
                    
                    // UNCERTAINTY OF THE MEAN //
                    double varSqrMean[NumSums];
                    // POSITIVE POSITIVE VALUES
                    for(int config=0;config<NumSums;config++){
                        if(i[0]!=0){
                            varSqrMean[config]=(IntegrandSqr[config]-double(i[config])*(Mean[config]*Mean[config]))/ (double(i[config])*(double(i[config])-1.0));
                        }
                        else{
                            varSqrMean[config]=0.0;
                        }
                        
                    }
                    
                    double TotalMean=0.0;
                    // SUM OVER BINS //
                    for(int config=0;config<NumSums;config++){
                        if(i[config]>=100){
                            TotalMean+=Mean[config];
                        }
                    }
                    double TotalUncertainty=0.0;
                    double TempUncertainty=0.0;
                    for(int config=0;config<NumSums;config++){
                        if(i[config]>=100){
                            TempUncertainty+=sqrt(varSqrMean[config]*varSqrMean[config]);
                        }
                    }
                    TotalUncertainty=sqrt(TempUncertainty);
                    // OUTPUT DATA AND UNCERTAINTY //
                    OutputStream << TotalMean << " " << TotalUncertainty << " ";
                    
                }//n!=1
            }// n-loop
            // NEW LINE //
            OutputStream << std::endl;
            
        }

        // CLOSE OUT-STREAM //
        OutputStream.close();
        
        // COMMANDLINE OUTPUT //
        std::cout << "#FINISHED COMPUTING VALUES" << std::endl;
        
        // CLEAN-UP //
        delete[] Mean;
        delete[] IntegrandSqr;
        
        delete[] Norm;
        
        // COMMANDLINE OUTPUT //
        std::cout << "#DONE SAVING VALUES ID=" << MY_MPI_RNG_SEED << std::endl;
        
        
    }
}
