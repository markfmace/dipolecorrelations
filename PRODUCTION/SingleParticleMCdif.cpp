/////////////////////////////////////////////////////////
//                                                     //
// COMPUTES ONE PARTICLE SINGLE DIFFERNTIAL SPECTRA    //
// TO ALL ORDERS IN THE MV MODEL FOR FIXED (FINITE NC) //
//          WITH MONTE CARLO INTEGRATION               //
//                                                     //
/////////////////////////////////////////////////////////


// NUMBER OF COLORS //
int Nc=3;
// CASIMIR FACTOR //
double Cf=(Nc*Nc-1.0)/(2.0*Nc);
// NUMBER OF WILSON LINES
const static int nWilsonLines=2;
// SIZE OF ONE DIMENSION OF THE MATRIX //
const static int MatrixSize=1;

// DOMAIN SIZE //
double B=4.0; /// DONT CHANGE ///

// LAMBDA QCD //
double LambdaQCD=0.241; // REFERS TO PHENO VALUE OF \Lambda=0.241 GeV

// Qs^2 //
double QsSQR=1.0;

// POWER FOR NUMBER OF POINTS //
int nPow=5;

// NUMBER OF v_n TO CALCULATE //
int vnMaxPlusOne=1;

// NUMBER OF PARTIAL SUMS TO CALCULATE FOR SIGN
const static int NumSums=2;

// MOMENTA BOUNDS //
double ptMin=0.1;
double dpt=0.1;
double ptMax=5.0;

// DEFINE MOMENTA COUNTING//
int ptIndexMax=(ptMax-ptMin)/dpt+0.1;

// DEFINE MATRIX ORDERING //
int MatrixIndex(int i, int j){
    // ROW MAJOR ORDERING //
    return i*MatrixSize+j;
}

// DEFINE MATRIX ORDERING //
int ResultIndex(int config,int pIndex,int n){
    // ROW MAJOR ORDERING //
    return vnMaxPlusOne*(config+pIndex)+n;
}

// DEFINE BESSEL FUNCTION //
double Bessel(int n,double pr){
    
    return gsl_sf_bessel_Jn(n,pr);
}
// RAYLEIGH DISTRIBUTION //
double Rayleigh(double r, double sig)
{
    return r*exp(-r*r/(2*sig*sig))/(sig*sig);
}

#include <iomanip>

// RANDOM NUMBER GENERATOR //
#include "../MISC/GSLRandomNumberGenerator.cpp"

// INTEGRALS BY HAND //
#include "../INTEGRATION/Integration.cpp"
// PROBABILITY DISTRIBUTION FUNCTION //
#include "../PROBABILITY/PDFs.cpp"
// GSL DIAGONALIZATION //
//#include "../MATRIXOPERATIONS/Diagonalization.cpp"
// GSL INVERSION //
//#include "../MATRIXOPERATIONS/Inversion.cpp"
// OUTPUT HANDLING
#include "../IO/StringManipulation.cpp"
#include "../IO/OutputManagement.cpp"
// TIMING //
#include "../MISC/Timing.cpp"
// INCLUDE COMMANDLINE PROCESSING //
#include "../IO/COMMANDLINE/cfile.c"

// CALCULATE GLUON EXCHANGE MATRIX //
//#include "../MATRIXOPERATIONS/FullMatrixEvaluation.cpp"
//#include "../MATRIXOPERATIONS/nGluonMatrixEvaluation.cpp"
//#include "../MATRIXOPERATIONS/TwoDipoleQED.cpp"

// DIS AAMQS FORM
//L(r) = (g^4μ^2*r^2)/4/Log[1/(r*Lqcd) + Exp[1]]
//Λqcd ≡ (m/2)exp(γE−1/2)
double ec=1;
double Lfunction2(double ux,double uy,double vx,double vy,double Lambda){
    
    if(ux==vx && uy==vy){
        return 0.0;
    }
    else{
        return -std::pow(EuclideanNorm(ux,uy,vx,vy),2)/(16.0*PI)*std::log(1.0/(EuclideanNorm(ux,uy,vx,vy)*Lambda)+std::exp(1.0)*ec);
    }
    
}
// SINGLE PARTICLE KERNEL //
double SingleParticleKernel(double x,double y,double g4muSQR,double Lambda){
    return exp(Cf*g4muSQR*Lfunction2(x/2.0,0.0,-x/2.0,0.0,Lambda));
}


namespace Simulation {
    
    // MPI RANDOM NUMBER SEED //
    int MY_MPI_RNG_SEED;
    
    //////////////////////////
    //SIMULATION PROCDEDURE //
    //////////////////////////
    
    void Run(int MPI_RNG_SEED){
        
        ///////////
        // SETUP //
        ///////////
        
        //SET SEED //
        MY_MPI_RNG_SEED=MPI_RNG_SEED;
        
        //INITIALIZE RANDOM NUMBER GENERATOR //
        RandomNumberGenerator::Init(MY_MPI_RNG_SEED);
        
        // OUTPUT //
        std::cout << "#BEGINNING TWO DIPOLE D22 SPECTRA CALCULATION ID=" << MY_MPI_RNG_SEED << std::endl;
        
        
        ////////
        // Qs //
        ////////
        // SET g4muSQR based on QsSQR
        double Q02=QsSQR/std::log(1.0/(LambdaQCD*std::sqrt(2.0/QsSQR))+std::exp(1.0));
        
        double g4muSQR=4.0*PI*Q02/Cf;
        
        std::cerr << "#QsSQR=" << QsSQR << " #Q02=" << Q02 << " #g4muSQR=" << g4muSQR << std::endl;
        
        ///////////////////
        // VARIABLES     //
        ///////////////////
        
        // RADIAL VARIABLES //
        double r1;

        // MAX TOTAL COUNT //
        int iMax=std::pow(10,nPow);
        
        // MC INTEGRATED QUANTITIES //
        double Integrand;
        double* Mean=new double[NumSums];
        double* IntegrandSqr=new double[NumSums];
        
        ///////////////////
        // MAKE LOG FILE //
        ///////////////////
        
        std::ofstream LogStream;
        LogStream.open(StringManipulation::StringCast(IO::OutputDirectory,"InfoID",MY_MPI_RNG_SEED,".log").c_str());
        
        LogStream.precision(OUTPUT_PRECISION);
        
        // INFO FILE  //
        LogStream << "#PARAMETER LOG FOR MC ONE DIPOLE DIFF SPECTRA" << std::endl;
        LogStream << "#iMax=" << iMax << std::endl;
        LogStream << "#Nc=" << Nc << std::endl;
        LogStream << "#nWilsonLines=" << nWilsonLines << std::endl;
        LogStream << "#LambdaQCD=" << LambdaQCD << std::endl;
        LogStream << "#B=" << B << std::endl;
        LogStream << "#QsSQR=" << QsSQR << std::endl;
        LogStream << "#g4muSQR=" << g4muSQR << std::endl;
        LogStream << "#ptMin=" << ptMin << std::endl;
        LogStream << "#ptMax=" << ptMax << std::endl;
        LogStream << "#dpt=" << dpt << std::endl;
        LogStream << "#ptIndexMax=" << ptIndexMax << std::endl;
        
        std::cerr << "#LambdaQCD=" << LambdaQCD << " #Nc=" << Nc << " #Cf=" << Cf << " #B=" << B << " #g4muSQR=" << g4muSQR << " #iMax=" << iMax << std::endl;
        
        // CLOSE OUT-STREAM //
        LogStream.close();
        
        // COMMANDLINE OUTPUT //
        std::cout << "#COMPUTING VALUES" << std::endl;
        
        // RESET TIMING //
        Timing::Reset();
        
        // SET NORMS //
        double* NormDif=new double[vnMaxPlusOne*(ptIndexMax+1)*NumSums];
        
        // SET ZERO //
        for(int sign=0;sign<NumSums;sign++){
            for(int n=0;n<vnMaxPlusOne;n++){
                for(int ptIndex=0;ptIndex<=ptIndexMax;ptIndex++){
                    
                    NormDif[ResultIndex(sign,ptIndex,n)]=0.0;
                    
                }
            }
        }
        
        // IntegralFlag= 0 KERN>0, 1 KERN<0, 2 KERN, 3 abs(KERN), //
        std::cout << "#CALCULATING DIFFERENTIAL NORMS" << std::endl;
        
        // DO INTEGRALS FOR DIFFERENTIAL PART //
        for(int ptIndex=0;ptIndex<=ptIndexMax;ptIndex++){
            
            // BEGIN BY HAND OPTION //
            double pt=ptMin+dpt*ptIndex;
            
            // n=0 //
            // SIGN = +1 | J_n>0 //
            NormDif[ResultIndex(0,ptIndex,0)]=PDF::GetNormalization(0,pt,0,B);
            
            // SIGN = -1 | J_n<0//
            NormDif[ResultIndex(1,ptIndex,0)]=PDF::GetNormalization(1,pt,0,B);

            // END BY HAND OPTION //
            
        }
        
        std::cout << "#CALCULATING INTEGRATED NORMS" << std::endl;
        
        // GET TIMING
        double time=Timing::Get();
        
        std::cerr << "#IN t=" << time << " s NORMS WERE CALCULATED" << std::endl;
        
        // COMMANDLINE OUTPUT //
        std::cout << "#COMPUTING VALUES" << std::endl;
        
        // RESET TIMING //
        Timing::Reset();
        // INDEX FOR BOTH POSITIVE POSITIVE, NEGATIVE NEGATIVE, AND POSITIVE NEGATIVE VALUES
        int i[NumSums];
        
        // DEFINE SIGN VARIABLES //
        int IntegrandSign=0, sign1=0;
        
        // FILE OUTPUT
        std::ofstream OutputStream;
        
        OutputStream.open(StringManipulation::StringCast(IO::OutputDirectory,"IntegratedSpectra_QsSQR",QsSQR,"ID",MY_MPI_RNG_SEED,".txt").c_str());
        
        OutputStream.precision(OUTPUT_PRECISION);
        
        // GET AVERAGE AND VALUES FOR ERROR //
        for(int ptIndex=0;ptIndex<=ptIndexMax;ptIndex++){
            
            // DEFINE MOMENTUM //
            double pt=ptMin+dpt*ptIndex;
            
            // OUTPUT MOMENTA //
            OutputStream << pt << " ";
            
            // INTEGRATE OVER iMax POINTS //
            // RESET INITIAL COUNT //
            for(int n=0;n<vnMaxPlusOne;n+=2){
                
                std::cerr << "#ptIndex=" << ptIndex << " n=" << n << std::endl;
                
                // RESET COUNTS //
                for(int config=0;config<NumSums;config++){
                    i[config]=0;
                    Mean[config]=0.0;
                    IntegrandSqr[config]=0.0;
                }
                // TOTAL POINTS COUNTED //
                int iTot;
                // COUNT TO TOTAL OF iMax //
                do{
                    
                    // DIFFERNETIAL VARIABLE
                    r1=PDF::SamplePDistrubtion(pt,n,B);


                    // SET MATRIX EXPONENTIAL RESULT //
                    Integrand=SingleParticleKernel(r1,0.0,g4muSQR,LambdaQCD);
                        
                    
                    // DETERMINE SIGN OF EACH DISTRIBUTION //
                    // +1 FOR NEGATIVE, 0 FOR POSTITIVE //
                    // DIFFERENTIAL VARIABLE //
                    sign1=int(0.5*(1.0-PDF::P(n,r1,pt,B)/fabs(PDF::P(n,r1,pt,B))));
                    // DETERMINE BIN //
                    IntegrandSign=sign1;
                    
                    // INCREMENT COUNTER FOR GIVEN BIN //
                    i[IntegrandSign]++;
                    // MULTIPLY BY DISTR NORMS //
                    Integrand*=NormDif[ResultIndex(sign1,ptIndex,n)];
                    // DETERMINE MEAN AND VARIANCE //
                    Mean[IntegrandSign]+=(Integrand-Mean[IntegrandSign])/double(i[IntegrandSign]);
                    IntegrandSqr[IntegrandSign]+=Integrand*Integrand;
                    
                    // RESET COUNTER
                    iTot=0;
                    // TOTAL COUNT //
                    for(int j=0;j<NumSums;j++){
                        iTot+=i[j];
                    }
                    
                }while(iTot<iMax);
                
                
                // COMMANDLINE OUTPUT //
                std::cerr << "#i= ";
                
                for(int j=0;j<NumSums;j++){
                    std::cerr << i[j] << " ";
                }
                std::cerr << " PTS COMPUTED " << std::endl;
                
                // UNCERTAINTY OF THE MEAN //
                double varSqrMean[NumSums];
                // POSITIVE POSITIVE VALUES
                for(int config=0;config<NumSums;config++){
                    if(i[0]!=0){
                        varSqrMean[config]=(IntegrandSqr[config]-double(i[config])*(Mean[config]*Mean[config]))/ (double(i[config])*(double(i[config])-1.0));
                    }
                    else{
                        varSqrMean[config]=0.0;
                    }
                    
                }
                
                double TotalMean=0.0;
                // SUM OVER BINS //
                for(int config=0;config<NumSums;config++){
                    if(i[config]>=100){
                        TotalMean+=Mean[config];
                    }
                }
                double TotalUncertainty=0.0;
                double TempUncertainty=0.0;
                for(int config=0;config<NumSums;config++){
                    if(i[config]>=100){
                        TempUncertainty+=varSqrMean[config]*varSqrMean[config];
                    }
                }
                TotalUncertainty=std::pow(TempUncertainty,0.25);
                // OUTPUT DATA AND UNCERTAINTY //
                OutputStream << TotalMean << " " << TotalUncertainty << " ";
                
            }
            // NEW LINE //
            OutputStream << std::endl;
            
        }
        
        // CLOSE OUT-STREAM //
        OutputStream.close();
        
        // COMMANDLINE OUTPUT //
        std::cout << "#FINISHED COMPUTING VALUES" << std::endl;
        
        // CLEAN-UP //
        delete[] Mean;
        delete[] IntegrandSqr;
        
        delete[] NormDif;
        
        
        // COMMANDLINE OUTPUT //
        std::cout << "#DONE SAVING VALUES ID=" << MY_MPI_RNG_SEED << std::endl;
        
        
    }
}
