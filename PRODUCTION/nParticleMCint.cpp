/////////////////////////////////////////////////////////
//                                                     //
// COMPUTES FOUR PARTICLE INTEGRATED SPECTRA           //
// TO ALL ORDERS IN THE MV MODEL FOR FIXED (FINITE NC) //
//          WITH MONTE CARLO INTEGRATION               //
//                                                     //
/////////////////////////////////////////////////////////


// NUMBER OF COLORS //
int Nc=1;

// NUMBER OF WILSON LINES
const static int nDipoles=8;

// DOMAIN SIZE //
double B=4.0; /// DONT CHANGE ///

// g4muSQR
double g4muSQR=16.0;

// LAMBDA QCD //
double LambdaQCD=0.241; // REFERS TO PHENO VALUE OF \Lambda=0.241 GeV

// Qs^2 //
double QsSQR=1.0;

// POWER FOR NUMBER OF POINTS //
double nPow=4;

// HARMONIC //
int n=2;

// NUMBER OF v_n TO CALCULATE //
int vnMaxPlusOne=3; // DO NOT CHANGE

// NUMBER OF PARTIAL SUMS TO CALCULATE FOR SIGN
const static int NumSums=std::pow(2,6);

// DEFINE MOMENTA COUNTING//
int pMax=2.0; // GeV

// DEFINE BESSEL FUNCTION //
double Bessel(int n,double pr){
    
    return gsl_sf_bessel_Jn(n,pr);
}
// RAYLEIGH DISTRIBUTION //
double Rayleigh(double r, double sig)
{
    return r*exp(-r*r/(2*sig*sig))/(sig*sig);
}

#include <iomanip>

// RANDOM NUMBER GENERATOR //
#include "../MISC/GSLRandomNumberGenerator.cpp"
// INTEGRALS BY HAND //
#include "../INTEGRATION/Integration.cpp"
// PROBABILITY DISTRIBUTION FUNCTION //
#include "../PROBABILITY/PDFs.cpp"
// GSL DIAGONALIZATION //
//#include "../MATRIXOPERATIONS/Diagonalization.cpp"
//#include "../MATRIXOPERATIONS/LapackDiagonalization.cpp"

// GSL INVERSION //
//#include "../MATRIXOPERATIONS/Inversion.cpp"
// OUTPUT HANDLING
#include "../IO/StringManipulation.cpp"
#include "../IO/OutputManagement.cpp"
// TIMING //
#include "../MISC/Timing.cpp"
// INCLUDE COMMANDLINE PROCESSING //
#include "../IO/COMMANDLINE/cfile.c"

// CALCULATE GLUON EXCHANGE MATRIX //
//#include "../MATRIXOPERATIONS/FullMatrixEvaluationFourDipoles.cpp"
#include "../MATRIXOPERATIONS/nDipoleQED.cpp"
/*
 #if Nc==1
 #include "../MATRIXOPERATIONS/FourDipoleQED.cpp"
 #else
 #include "../MATRIXOPERATIONS/FullMatrixEvaluationFourDipoles.cpp"
 #endif
 */

namespace Simulation {
    
    // MPI RANDOM NUMBER SEED //
    int MY_MPI_RNG_SEED;
    
    //////////////////////////
    //SIMULATION PROCDEDURE //
    //////////////////////////
    
    void Run(int MPI_RNG_SEED){
        
        ///////////
        // SETUP //
        ///////////
        
        //SET SEED //
        MY_MPI_RNG_SEED=MPI_RNG_SEED;
        
        //INITIALIZE RANDOM NUMBER GENERATOR //
        RandomNumberGenerator::Init(MY_MPI_RNG_SEED);
        
        // OUTPUT //
        std::cout << "#BEGINNING FOUR DIPOLE INTEGRATED SPECTRA CALCULATION ID=" << MY_MPI_RNG_SEED << std::endl;
        
        std::cerr << "#g4muSQR=" << g4muSQR << std::endl;
        
        ///////////////////
        // VARIABLES     //
        ///////////////////
        
    
        // GENERAL POSITION VECTORS //
        double **x;
        x = new double*[2*nDipoles]; // FIRST INDEX //
        for(int i=0;i<2*nDipoles;i++) {
            x[i] = new double[2]; // SECOND INDEX //
        }
        
        // SET TO ZERO INITIALLY
        for(int i=0;i<2*nDipoles;i++){
            for(int j=0;j<2;j++){
                x[i][j]=0.0;
            }
        }
        
        // MAX TOTAL COUNT //
        int iMax=std::pow(10,nPow);

        ///////////////////
        // MAKE LOG FILE //
        ///////////////////
        
        std::ofstream LogStream;
        LogStream.open(StringManipulation::StringCast(IO::OutputDirectory,"InfoID",MY_MPI_RNG_SEED,".log").c_str());
        
        LogStream.precision(OUTPUT_PRECISION);
        
        // INFO FILE  //
        LogStream << "#PARAMETER LOG FOR MC FOUR DIPOLE INTEGRATED SPECTRA" << std::endl;
        LogStream << "#iMax=" << iMax << std::endl;
        LogStream << "#Nc=" << Nc << std::endl;
        LogStream << "#nDipoles=" << nDipoles << std::endl;
        LogStream << "#LambdaQCD=" << LambdaQCD << std::endl;
        LogStream << "#B=" << B << std::endl;
        LogStream << "#g4muSQR=" << g4muSQR << std::endl;
        LogStream << "#pMax=" << pMax << std::endl;
        
        std::cerr << "#LambdaQCD=" << LambdaQCD << " #Nc=" << Nc << " #g4muSQR=" << g4muSQR << " #n=" << n << " #iMax=" << iMax << std::endl;
        
        // CLOSE OUT-STREAM //
        LogStream.close();
        
        // COMMANDLINE OUTPUT //
        std::cout << "#COMPUTING VALUES" << std::endl;
        
        // RESET TIMING //
        Timing::Reset();
        
        std::cout << "#CALCULATING NORMS" << std::endl;
        
        // SET NORMS //
        double* r=new double[nDipoles];
        double* Rx=new double[nDipoles];
        double* Ry=new double[nDipoles];
        double* phi=new double[nDipoles];
        double* sign=new double[nDipoles];

        
        for(int j=0;j<nDipoles;j++){
            r[j]=0.0;
            Rx[j]=0.0;
            Ry[j]=0.0;
            phi[j]=0.0;
            sign[j]=0;
        }
        
        double* Norm=new double[2];
        
        // SET ZERO //
        for(int sign=0;sign<2;sign++){
            Norm[sign]=0.0;
            
        }
        
        // IntegralFlag= 0 KERN>0, 1 KERN<0, 2 KERN, 3 abs(KERN), //
        // DO INTEGRATIONS //
        for(int sign=0;sign<2;sign++){
            
            // SIGN = +1 | J_n>0 --SIGN = -1 | J_n<0//
            Norm[sign]=PDF::GetNormalizationIntegrated(sign,pMax,n,B);
            
        }
        
        
        // GET TIMING
        double time=Timing::Get();
        
        std::cerr << "#IN t=" << time << " s NORMS WERE CALCULATED" << std::endl;
        
        // COMMANDLINE OUTPUT //
        std::cout << "#COMPUTING VALUES" << std::endl;
        
        // RESET TIMING //
        Timing::Reset();
        
        // FILE OUTPUT
        std::ofstream OutputStream;
        
        OutputStream.open(StringManipulation::StringCast(IO::OutputDirectory,"IntegratedSpectra_n",n,"g4muSQR",g4muSQR,"pMax",pMax,"ID",MY_MPI_RNG_SEED,".txt").c_str());
        
        OutputStream.precision(OUTPUT_PRECISION);
        
        double Mean=0.0; double IntegrandSqr=0.0;
        
        // OUTPUT MOMENTA //
        OutputStream << pMax << " " << g4muSQR << " ";
        
        // INTEGRATE OVER iMax POINTS //
        
        std::cerr << "#pMax=" << pMax << " n=" << n << std::endl;
        
        // TOTAL POINTS COUNTED //
        int iTot=0;
        // COUNT TO TOTAL OF iMax //
        do{
            iTot++;

            
            for(int j=0;j<nDipoles;j++){
                r[j]=PDF::SamplePDistrubtionInt(pMax,n,B);
                Rx[j]=RandomNumberGenerator::Gauss(sqrt(B/2.0));
                Ry[j]=RandomNumberGenerator::Gauss(sqrt(B/2.0));
                phi[j]=2.0*PI*RandomNumberGenerator::rng();
                
            }
            
            // SET ACUTAL POINTS FOR DIPOLE CORRELATOR
            for(int j=0;j<nDipoles;j++){
                x[2*j][0]=(Rx[j]+0.5*r[j]*cos(phi[j]));
                x[2*j][1]=(Ry[j]+0.5*r[j]*cos(phi[j]));
                
                x[2*j+1][0]=(Rx[j]-0.5*r[j]*cos(phi[j]));
                x[2*j+1][1]=(Ry[j]-0.5*r[j]*cos(phi[j]));
                
            }
            
            double psi = 0.0;
            for(int j=0;j<nDipoles/2;j++){
                psi+=phi[j]-phi[nDipoles-1-j];
            }
            
            // SET MATRIX EXPONENTIAL RESULT //
            double Integrand=MatrixEvaluation(x,g4muSQR,LambdaQCD,nDipoles)*cos(double(n)*psi);
            
            // DETERMINE SIGN OF EACH DISTRIBUTION //
            for(int j=0;j<nDipoles;j++){
                sign[j]=int(0.5*(1.0-PDF::PInt(n,r[j],pMax,B)/sqrt(PDF::PInt(n,r[j],pMax,B)*PDF::PInt(n,r[j],pMax,B))));
            }
            
            // MULTIPLY BY DISTR NORMS //
            for(int j=0;j<nDipoles;j++){
                
                int INDEX=sign[j];
                
                Integrand*=Norm[INDEX];
                
            }
            
            // DETERMINE MEAN AND VARIANCE //
            Mean+=(Integrand-Mean)/double(iTot);
            IntegrandSqr+=Integrand*Integrand;
            
            
        }while(iTot<iMax);
        
        
        // UNCERTAINTY OF THE MEAN //
        double varSqrMean=(IntegrandSqr-double(iTot)*(Mean*Mean))/ (double(iTot)*(double(iTot)-1.0));

        double TotalUncertainty=std::pow(varSqrMean,0.5);
        // OUTPUT DATA AND UNCERTAINTY //
        OutputStream << Mean << " " << TotalUncertainty << " ";
        
        // CLOSE OUT-STREAM //
        OutputStream.close();
        
        // COMMANDLINE OUTPUT //
        std::cout << "#FINISHED COMPUTING VALUES" << std::endl;
        
        // CLEAN-UP //
        delete[] r;
        delete[] Rx;
        delete[] Ry;
        delete[] phi;
        delete[] sign;

        
        delete[] Norm;
        
        for(int j=0;j<2*nDipoles;j++){
            delete[] x[j];
        }
        // COMMANDLINE OUTPUT //
        std::cout << "#DONE SAVING VALUES ID=" << MY_MPI_RNG_SEED << std::endl;
        
        
    }
}
