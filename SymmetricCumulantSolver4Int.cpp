// INCLUDE MPI SAMPLING //
#include "MPI/Basic.cpp"

#include "Definitions.cpp"

#include "SYMMETRIC_CUMULANTS/FourParticleMCint.cpp"


int main(int argc,char **argv){
    
    ///////////////////////////////
    //INITIALIZE MPI ENVIRONMENT //
    ///////////////////////////////
    
    MPI_Init(&argc, &argv);
    MPI_Barrier(MPI_COMM_WORLD);
    
    //SET MPI ID's AND NUMBER OF NODES
    MPI_Comm_rank(MPI_COMM_WORLD,&MPIBasic::ID);
    MPI_Comm_size(MPI_COMM_WORLD,&MPIBasic::NumberOfNodes);
    
    //////////////////////////////////
    //PROCESS COMMANDLINE ARGUMENTS //
    //////////////////////////////////
    
    Konfig arguments(argc,argv);
    
    ///////////////////////////////
    // GET SIMULATION PARAMETERS //
    ///////////////////////////////
    
    int NumberOfConfigurations=1;
    
    arguments.Getval("nconfs",NumberOfConfigurations);
    
    arguments.Getval("m",m);
    arguments.Getval("B",B);
    arguments.Getval("gsm",g4muSQR);
    arguments.Getval("nPow",nPow);

    
    std::cerr << "#m=" << m << " B=" << B << " g4muSQR=" << g4muSQR << " nPow=" << nPow << std::endl;

    //////////////////////////
    // SET OUTPUT DIRECTORY //
    //////////////////////////
    
    char OutDir[256]="MC_2";
    
    arguments.Getval("o",OutDir);
    
    IO::SetOutputDirectory(OutDir);
    
    ///////////////////////////////
    // GET SIMULATION PARAMETERS //
    ///////////////////////////////
    
    // EXPORT OMP NUMBER OF THREADS //
    std::cout << "#NUMBER OF THREADS " << omp_get_max_threads() << std::endl;
    
    // SAMPLE DIFFERENT CONFIGURATIONS //
    for(int n=0;n<NumberOfConfigurations;n++){
        
        //SET GLOBAL RANDOM NUMBER SEED//
        int GLOBAL_RNG_SEED;
        
        if(MPIBasic::ID==0){
            
            GLOBAL_RNG_SEED=time(0);
            
            arguments.Getval("SEED",GLOBAL_RNG_SEED);
        }
        
        // BROADCAST GLOBAL RANDOM SEED //
        MPI_Bcast(&GLOBAL_RNG_SEED, 1, MPI_INT,0,MPI_COMM_WORLD);
        
        // PERFORM CLASSICAL STATISTICAL SIMULATION //
        Simulation::Run(GLOBAL_RNG_SEED+MPIBasic::ID);
        
        // COMMADNLINE NOTIFICATION //
        std::cerr << "#COMPLETED " << GLOBAL_RNG_SEED+MPIBasic::ID << std::endl;
        
    }
    
    //SYNCHRONIZE ALL MPI NODES
    MPI_Barrier(MPI_COMM_WORLD);
    
    //FINALIZE MPI
    MPI_Finalize();
    
    //EXIT
    exit(0);
    
    
}
