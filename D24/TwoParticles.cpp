/////////////////////////////////////////////////////////
//                                                     //
// COMPUTES TWO PARTICLE SINGLE DIFFERNTIAL SPECTRA    //
// TO ALL ORDERS IN THE MV MODEL FOR FIXED (FINITE NC) //
//          WITH MONTE CARLO INTEGRATION               //
//                                                     //
/////////////////////////////////////////////////////////


// NUMBER OF COLORS //
const static int Nc=3;
// CASIMIR FACTOR //
const static double Cf=(Nc*Nc-1.0)/(2.0*Nc);
// NUMBER OF WILSON LINES
const static int nWilsonLines=4;
// SIZE OF ONE DIMENSION OF THE MATRIX //
const static int MatrixSize=2;

// DOMAIN SIZE //
double B=1.0; /// DONT CHANGE ///
double Bb=1.0*B;
double Bk=1.0*B;

// PROPAGATOR INFRARED MASS IN TERMS OF B//
double m=0.482; // REFERS TO PHENO VALUE OF \Lambda=0.241 GeV
// g^2 MU //
double g4muSQR=16.0; // ACTUALLY g4muSQR*B -- g4\[Mu]2[Q_] := 4*Pi/((3^2 - 1)/(2*3))*Q^2*4;
// POWER FOR NUMBER OF POINTS //
int nPow=5;

// NUMBER OF v_n TO CALCULATE //
int vnMaxPlusOne=3;

// NUMBER OF PARTIAL SUMS TO CALCULATE FOR SIGN
const static int NumSums=4;

// MOMENTA BOUNDS //
// THIS IS ptMax*sqrt(B)
double ptMin=0.2;
double dpt=0.4;
double ptMax=3.0;

// INTEGRATED MOMENTA BOUND //
// THIS IS ptInt*sqrt(B)
double ptInt=4.0;
int ptIntIndex=0;

// DEFINE MOMENTA COUNTING//
int ptIndexMax=(ptMax-ptMin)/dpt+0.1;

// DEFINE MATRIX ORDERING //
int MatrixIndex(int i, int j){
    // ROW MAJOR ORDERING //
    return i*MatrixSize+j;
}

// DEFINE MATRIX ORDERING //
int ResultIndex(int config,int pIndex,int n){
    // ROW MAJOR ORDERING //
    return vnMaxPlusOne*(config*(ptIndexMax+1)+pIndex)+n;
}

// DEFINE BESSEL FUNCTION //
double Bessel(int n,double pr){
    
    return gsl_sf_bessel_Jn(n,pr);
}
// RAYLEIGH DISTRIBUTION //
double Rayleigh(double r, double sig)
{
    return r*exp(-r*r/(2*sig*sig))/(sig*sig);
}

#include <iomanip>

// RANDOM NUMBER GENERATOR //
#include "../MISC/GSLRandomNumberGenerator.cpp"

// INTEGRALS BY HAND //
#include "../INTEGRATION/Integration.cpp"
// PROBABILITY DISTRIBUTION FUNCTION //
#include "../PROBABILITY/PDFs.cpp"
// GSL DIAGONALIZATION //
//#include "../MATRIXOPERATIONS/Diagonalization.cpp"
// GSL INVERSION //
//#include "../MATRIXOPERATIONS/Inversion.cpp"
// OUTPUT HANDLING
#include "../IO/StringManipulation.cpp"
#include "../IO/OutputManagement.cpp"
// TIMING //
#include "../MISC/Timing.cpp"
// INCLUDE COMMANDLINE PROCESSING //
#include "../IO/COMMANDLINE/cfile.c"

// CALCULATE GLUON EXCHANGE MATRIX //
#include "../MATRIXOPERATIONS/FullMatrixEvaluation.cpp"
//#include "../MATRIXOPERATIONS/nGluonMatrixEvaluation.cpp"



namespace Simulation {
    
    // MPI RANDOM NUMBER SEED //
    int MY_MPI_RNG_SEED;
    
    //////////////////////////
    //SIMULATION PROCDEDURE //
    //////////////////////////
    
    void Run(int MPI_RNG_SEED){
        
        ///////////
        // SETUP //
        ///////////
        
        //SET SEED //
        MY_MPI_RNG_SEED=MPI_RNG_SEED;
        
        //INITIALIZE RANDOM NUMBER GENERATOR //
        RandomNumberGenerator::Init(MY_MPI_RNG_SEED);
        
        // OUTPUT //
        std::cout << "#BEGINNING TWO DIPOLE D22 SPECTRA CALCULATION ID=" << MY_MPI_RNG_SEED << std::endl;
        
        ///////////////////
        // VARIABLES     //
        ///////////////////
        
        // RADIAL VARIABLES //
        double r1,r2;
        double r1x,r1y,r2x,r2y;
        
        // COM VARIABLES //
        double R1x,R1y,R2x,R2y;
        
        // ANGULAR VARIABLES //
        double phir1,phir2,theta,psi;
        int dphiIndexMax=32;
        double dphi=2.0*PI/double(dphiIndexMax);
        
        // DIPOLE VARIABLES //
        double x1,y1,x2,y2,x3,y3,x4,y4;
        
        // MAX TOTAL COUNT //
        int iMax=std::pow(10,nPow);
        
        // MC INTEGRATED QUANTITIES //
        double Integrand;
        double* Mean=new double[NumSums];
        double* IntegrandSqr=new double[NumSums];
        
        ///////////////////
        // MAKE LOG FILE //
        ///////////////////
        
        std::ofstream LogStream;
        LogStream.open(StringManipulation::StringCast(IO::OutputDirectory,"InfoID",MY_MPI_RNG_SEED,".log").c_str());
        
        LogStream.precision(OUTPUT_PRECISION);
        
        // INFO FILE  //
        LogStream << "#PARAMETER LOG FOR MC TWO DIPOLE D22 SPECTRA" << std::endl;
        LogStream << "#iMax=" << iMax << std::endl;
        LogStream << "#Nc=" << Nc << std::endl;
        LogStream << "#nWilsonLines=" << nWilsonLines << std::endl;
        LogStream << "#m=" << m << std::endl;
        LogStream << "#B=" << B << std::endl;
        LogStream << "#g4muSQR=" << g4muSQR << std::endl;
        LogStream << "#ptMin=" << ptMin << std::endl;
        LogStream << "#ptMax=" << ptMax << std::endl;
        LogStream << "#ptInt=" << ptInt << std::endl;
        LogStream << "#dpt=" << dpt << std::endl;
        LogStream << "#ptIndexMax=" << ptIndexMax << std::endl;
        LogStream << "#dphiIndexMax=" << dphiIndexMax << std::endl;
        
        std::cerr << "#m=" << m << " #Nc=" << Nc << " #B=" << B << " #g4muSQR=" << g4muSQR << " #ptInt " << ptInt << " #iMax=" << iMax << std::endl;
        
        // CLOSE OUT-STREAM //
        LogStream.close();
        
        // COMMANDLINE OUTPUT //
        std::cout << "#COMPUTING VALUES" << std::endl;
        
        // RESET TIMING //
        Timing::Reset();
        
        // SET NORMS //
        double* NormDif=new double[vnMaxPlusOne*(ptIndexMax+1)*NumSums];
        double* NormInt=new double[vnMaxPlusOne*(ptIndexMax+1)*NumSums];
        
        // SET ZERO //
        for(int sign=0;sign<NumSums;sign++){
            for(int n=0;n<vnMaxPlusOne;n++){
                for(int ptIndex=0;ptIndex<=ptIndexMax;ptIndex++){
                    
                    NormDif[ResultIndex(sign,ptIndex,n)]=0.0;
                    NormInt[ResultIndex(sign,ptIndex,n)]=0.0;

                }
            }
        }
        
        // IntegralFlag= 0 KERN>0, 1 KERN<0, 2 KERN, 3 abs(KERN), //
        std::cout << "#CALCULATING DIFFERENTIAL NORMS" << std::endl;

        // DO INTEGRALS FOR DIFFERENTIAL PART //
        for(int ptIndex=0;ptIndex<=ptIndexMax;ptIndex++){
            
            // BEGIN BY HAND OPTION //
            double pt=ptMin+dpt*ptIndex;
            
            // n=0 //
            // SIGN = +1 | J_n>0 //
            NormDif[ResultIndex(0,ptIndex,0)]=PDF::GetNormalization(0,pt,0,Bk);
            
            // SIGN = -1 | J_n<0//
            NormDif[ResultIndex(1,ptIndex,0)]=PDF::GetNormalization(1,pt,0,Bk);
            
            // n=2 //
            // SIGN = +1 | J_n>0 //
            NormDif[ResultIndex(0,ptIndex,2)]=PDF::GetNormalization(0,pt,2,Bk);
            
            // SIGN = -1 | J_n<0//
            NormDif[ResultIndex(1,ptIndex,2)]=PDF::GetNormalization(1,pt,2,Bk);
            // END BY HAND OPTION //
            
        }
        
        std::cout << "#CALCULATING INTEGRATED NORMS" << std::endl;
        // DO INTEGRALS FOR INTEGRATED PART //
        
        // n=0 //
        // SIGN = +1 | J_n>0 //
        NormInt[ResultIndex(0,ptIntIndex,0)]=PDF::GetNormalizationIntegrated(0,ptInt,0,Bk);
        // SIGN = -1 | J_n<0//
        NormInt[ResultIndex(1,ptIntIndex,0)]=PDF::GetNormalizationIntegrated(1,ptInt,0,Bk);
        
        // n=2 //
        // SIGN = +1 | J_n>0 //
        NormInt[ResultIndex(0,ptIntIndex,2)]=PDF::GetNormalizationIntegrated(0,ptInt,2,Bk);
        // SIGN = -1 | J_n<0//
        NormInt[ResultIndex(1,ptIntIndex,2)]=PDF::GetNormalizationIntegrated(1,ptInt,2,Bk);
        
        
        // GET TIMING
        double time=Timing::Get();
        
        std::cerr << "#IN t=" << time << " s NORMS WERE CALCULATED" << std::endl;
        
        // COMMANDLINE OUTPUT //
        std::cout << "#COMPUTING VALUES" << std::endl;
        
        // RESET TIMING //
        Timing::Reset();
        // INDEX FOR BOTH POSITIVE POSITIVE, NEGATIVE NEGATIVE, AND POSITIVE NEGATIVE VALUES
        int i[NumSums];
        
        // DEFINE SIGN VARIABLES //
        int IntegrandSign=0, sign1=0, sign2=0;
        
        // FILE OUTPUT
        std::ofstream OutputStream;
        
        OutputStream.open(StringManipulation::StringCast(IO::OutputDirectory,"IntegratedSpectra_Bb",Bb,"Bk",Bk,"g4muSQR",g4muSQR,"ID",MY_MPI_RNG_SEED,".txt").c_str());
        
        OutputStream.precision(OUTPUT_PRECISION);
        
        // GET AVERAGE AND VALUES FOR ERROR //
        for(int ptIndex=0;ptIndex<=ptIndexMax;ptIndex++){
            
            // DEFINE MOMENTUM //
            double pt=ptMin+dpt*ptIndex;
            
            // OUTPUT MOMENTA //
            OutputStream << pt << " ";
            
            // INTEGRATE OVER iMax POINTS //
            // RESET INITIAL COUNT //
            for(int n=0;n<vnMaxPlusOne;n+=2){
                
                std::cerr << "#ptIndex=" << ptIndex << " n=" << n << std::endl;
                
                // RESET COUNTS //
                for(int config=0;config<NumSums;config++){
                    i[config]=0;
                    Mean[config]=0.0;
                    IntegrandSqr[config]=0.0;
                }
                // TOTAL POINTS COUNTED //
                int iTot;
                // COUNT TO TOTAL OF iMax //
                do{

                    // IMPORTANCE SAMPLING REJECTION VARIABLE //
                    double RejectVar;
                    // INTEGRATED VARIABLE
                    do{
                        r1=RandomNumberGenerator::RaleighRNG(sqrt(2*Bk));
                        
                        RejectVar=RandomNumberGenerator::rng();
                        
                    }while(RejectVar>PDF::PABS(n,r1,pt,Bk)/Rayleigh(r1,sqrt(2*Bk)));                    // INTEGRATED VARIABLE //
                    r2=PDF::SamplePDistrubtionInt(ptInt,n,Bk);
                    
                    // SET RADIUS VECTOR //
                    R1x=RandomNumberGenerator::Gauss(sqrt(Bb/2.0));
                    R1y=RandomNumberGenerator::Gauss(sqrt(Bb/2.0));
                    R2x=RandomNumberGenerator::Gauss(sqrt(Bb/2.0));
                    R2y=RandomNumberGenerator::Gauss(sqrt(Bb/2.0));
                    
                    // FIRST ANGLE //
                    theta=2.0*PI*RandomNumberGenerator::rng();
                    
                    // RESET //
                    Integrand=0.0;
                    
                    //TRAPEZOID RULE ON PSI INTEGRAL
                    for(int dphiIndex=0;dphiIndex<=dphiIndexMax;dphiIndex++){
                        
                        // SECONARDY ANGLE //
                        psi=dphiIndex*dphi;
                        
                        // DEFINE AZIMUTHAL ANGLES //
                        phir1=theta+psi/2.0;
                        phir2=theta-psi/2.0;
                        
                        r1x=r1*cos(phir1);
                        r1y=r1*sin(phir1);
                        r2x=r2*cos(phir2);
                        r2y=r2*sin(phir2);
                        
                        // SET ACUTAL POINTS FOR DIPOLE CORRELATOR
                        x1=(R1x+0.5*r1x);
                        y1=(R1y+0.5*r1y);
                        x2=(R1x-0.5*r1x);
                        y2=(R1y-0.5*r1y);
                        x3=(R2x+0.5*r2x);
                        y3=(R2y+0.5*r2y);
                        x4=(R2x-0.5*r2x);
                        y4=(R2y-0.5*r2y);
                        
                        double SimpsonCoeff=(dphiIndex==0 || dphiIndex==dphiIndexMax ? 1.0 : 2.0+2.0*(dphiIndex%2))*dphi/(6.0*PI);
                        
                        // SET MATRIX EXPONENTIAL RESULT //
                        Integrand+=SimpsonCoeff*MatrixEvaluation(x1,y1,x2,y2,x3,y3,x4,y4,g4muSQR,m)*cos(double(n)*psi);
                        
                    }
                    
                    // DETERMINE SIGN OF EACH DISTRIBUTION //
                    // +1 FOR NEGATIVE, 0 FOR POSTITIVE //
                    // DIFFERENTIAL VARIABLE //
                    sign1=int(0.5*(1.0-PDF::P(n,r1,pt,Bk)/sqrt(PDF::P(n,r1,pt,Bk)*PDF::P(n,r1,pt,Bk))));
                    // INTEGRATED VARIABLE //
                    sign2=int(0.5*(1.0-PDF::PInt(n,r2,ptInt,Bk)/sqrt(PDF::PInt(n,r2,ptInt,Bk)*PDF::PInt(n,r2,ptInt,Bk))));
                    // DETERMINE BIN //
                    IntegrandSign=2*sign1+sign2;
                    
                    // INCREMENT COUNTER FOR GIVEN BIN //
                    i[IntegrandSign]++;
                    // MULTIPLY BY DISTR NORMS //
                    Integrand*=NormDif[ResultIndex(sign1,ptIndex,n)]*NormInt[ResultIndex(sign2,ptIntIndex,n)];
                    // DETERMINE MEAN AND VARIANCE //
                    Mean[IntegrandSign]+=(Integrand-Mean[IntegrandSign])/double(i[IntegrandSign]);
                    IntegrandSqr[IntegrandSign]+=Integrand*Integrand;
                    
                    // RESET COUNTER
                    iTot=0;
                    // TOTAL COUNT //
                    for(int j=0;j<NumSums;j++){
                        iTot+=i[j];
                    }
                    
                }while(iTot<iMax);
                
                
                // COMMANDLINE OUTPUT //
                std::cerr << "#i= ";
                
                for(int j=0;j<NumSums;j++){
                    std::cerr << i[j] << " ";
                }
                std::cerr << " PTS COMPUTED " << std::endl;
                
                // UNCERTAINTY OF THE MEAN //
                double varSqrMean[NumSums];
                // POSITIVE POSITIVE VALUES
                for(int config=0;config<NumSums;config++){
                    if(i[0]!=0){
                        varSqrMean[config]=(IntegrandSqr[config]-double(i[config])*(Mean[config]*Mean[config]))/ (double(i[config])*(double(i[config])-1.0));
                    }
                    else{
                        varSqrMean[config]=0.0;
                    }
                    
                }
                
                double TotalMean=0.0;
                // SUM OVER BINS //
                for(int config=0;config<NumSums;config++){
                    if(i[config]>=100){
                        TotalMean+=Mean[config];
                    }
                }
                double TotalUncertainty=0.0;
                double TempUncertainty=0.0;
                for(int config=0;config<NumSums;config++){
                    if(i[config]>=100){
                        TempUncertainty+=varSqrMean[config]*varSqrMean[config];
                    }
                }
                TotalUncertainty=std::pow(TempUncertainty,0.25);
                // OUTPUT DATA AND UNCERTAINTY //
                OutputStream << TotalMean << " " << TotalUncertainty << " ";
                
            }
            // NEW LINE //
            OutputStream << std::endl;
            
        }
        
        // CLOSE OUT-STREAM //
        OutputStream.close();
        
        // COMMANDLINE OUTPUT //
        std::cout << "#FINISHED COMPUTING VALUES" << std::endl;
        
        // CLEAN-UP //
        delete[] Mean;
        delete[] IntegrandSqr;
        
        delete[] NormDif;
        delete[] NormInt;
        
        
        // COMMANDLINE OUTPUT //
        std::cout << "#DONE SAVING VALUES ID=" << MY_MPI_RNG_SEED << std::endl;
        
        
    }
}
