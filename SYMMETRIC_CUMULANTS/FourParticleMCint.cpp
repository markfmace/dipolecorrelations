/////////////////////////////////////////////////////////
//                                                     //
// COMPUTES FOUR PARTICLE INTEGRATED SPECTRA           //
// TO ALL ORDERS IN THE MV MODEL FOR FIXED (FINITE NC) //
//          WITH MONTE CARLO INTEGRATION               //
//                                                     //
/////////////////////////////////////////////////////////


// NUMBER OF COLORS //
const static int Nc=3;
// CASIMIR FACTOR //
const static double Cf=(Nc*Nc-1.0)/(2.0*Nc);
// NUMBER OF WILSON LINES
const static int nWilsonLines=8;
// SIZE OF ONE DIMENSION OF THE MATRIX //
const static int MatrixSize=24;

// DOMAIN SIZE //
double B=1.0; /// DONT CHANGE ///
double Bb=1.0*B; // B FOR b or R
double Bk=1.0*B; // B for k or r
// PROPAGATOR INFRARED MASS IN TERMS OF B//
//double m=sqrt(0.2/B);
// g^2 MU //
//double g4muSQR=16.0; // ACUTALLY g4muSQR*B

// PROPAGATOR INFRARED MASS IN TERMS OF B//
double m=0.482; // REFERS TO PHENO VALUE OF \Lambda=0.241 GeV
// g^2 MU //
double g4muSQR=16.0; // ACUTALLY g4muSQR*B -- g4\[Mu]2[Q_] := 4*Pi/((3^2 - 1)/(2*3))*Q^2*4;
// POWER FOR NUMBER OF POINTS //
int nPow=4;

// NUMBER OF v_n TO CALCULATE //
int vnMaxPlusOne=6;

// NUMBER OF PARTIAL SUMS TO CALCULATE FOR SIGN
const static int NumSums=16;

// MOMENTA BOUNDS //
// THIS IS ptMax*sqrt(B)
double pMin=2.0;
double dp=1.0;
double pAbsMax=5.0;

// DEFINE MOMENTA COUNTING//
int pMaxIndexMax=(pAbsMax-pMin)/dp+0.1;

// DEFINE MATRIX ORDERING //
int MatrixIndex(int i, int j){
    // ROW MAJOR ORDERING //
    return i*MatrixSize+j;
}

// DEFINE MATRIX ORDERING //
int ResultIndex(int config,int pIndex,int n){
    // ROW MAJOR ORDERING //
    return vnMaxPlusOne*(config*(pMaxIndexMax+1)+pIndex)+n;
}

// DEFINE BESSEL FUNCTION //
double Bessel(int n,double pr){
    
    return gsl_sf_bessel_Jn(n,pr);
}
// RAYLEIGH DISTRIBUTION //
double Rayleigh(double r, double sig)
{
    return r*exp(-r*r/(2*sig*sig))/(sig*sig);
}

#include <iomanip>

// RANDOM NUMBER GENERATOR //
#include "../MISC/GSLRandomNumberGenerator.cpp"
// INTEGRALS BY HAND //
#include "../INTEGRATION/Integration.cpp"
// PROBABILITY DISTRIBUTION FUNCTION //
#include "../PROBABILITY/PDFs.cpp"
// GSL DIAGONALIZATION //
//#include "../MATRIXOPERATIONS/Diagonalization.cpp"
//#include "../MATRIXOPERATIONS/LapackDiagonalization.cpp"

// GSL INVERSION //
//#include "../MATRIXOPERATIONS/Inversion.cpp"
// OUTPUT HANDLING
#include "../IO/StringManipulation.cpp"
#include "../IO/OutputManagement.cpp"
// TIMING //
#include "../MISC/Timing.cpp"
// INCLUDE COMMANDLINE PROCESSING //
#include "../IO/COMMANDLINE/cfile.c"

// CALCULATE GLUON EXCHANGE MATRIX //
#include "../MATRIXOPERATIONS/FullMatrixEvaluationFourDipoles.cpp"
//#include "../MATRIXOPERATIONS/FullMatrixEvaluation.cpp"

//#include "../MATRIXOPERATIONS/nGluonMatrixEvaluationFourDipoles.cpp"



namespace Simulation {
    
    // MPI RANDOM NUMBER SEED //
    int MY_MPI_RNG_SEED;
    
    //////////////////////////
    //SIMULATION PROCDEDURE //
    //////////////////////////
    
    void Run(int MPI_RNG_SEED){
        
        ///////////
        // SETUP //
        ///////////
        
        //SET SEED //
        MY_MPI_RNG_SEED=MPI_RNG_SEED;
        
        //INITIALIZE RANDOM NUMBER GENERATOR //
        RandomNumberGenerator::Init(MY_MPI_RNG_SEED);
        
        // OUTPUT //
        std::cout << "#BEGINNING FOUR DIPOLE INTEGRATED SPECTRA CALCULATION ID=" << MY_MPI_RNG_SEED << std::endl;
        
        ///////////////////
        // VARIABLES     //
        ///////////////////
        
        // RADIAL VARIABLES //
        double r1,r2,r3,r4;
        double r1x,r1y,r2x,r2y,r3x,r3y,r4x,r4y;
        
        // COM VARIABLES //
        double R1x,R1y,R2x,R2y,R3x,R3y,R4x,R4y;
        
        // ANGULAR VARIABLES //
        double phir1,phir2,phir3,phir4,phi13,phi24,psi13,psi24;
        int psiIndexMax=64;
        double dpsi=2.0*PI/double(psiIndexMax);
        
        // GENERAL POSITION VECTORS //
        double **x;
        x = new double*[nWilsonLines]; // FIRST INDEX //
        for(int i=0;i<nWilsonLines;i++) {
            x[i] = new double[2]; // SECOND INDEX //
        }
        
        // SET TO ZERO INITIALLY
        for(int i=0;i<nWilsonLines;i++){
            for(int j=0;j<2;j++){
                x[i][j]=0.0;
            }
        }
        
        // MAX TOTAL COUNT //
        int iMax=std::pow(10,nPow);
        
        // MC INTEGRATED QUANTITIES //
        double Integrand;
        double* Mean = new double[NumSums];
        double* IntegrandSqr = new double[NumSums];
        
        
        ///////////////////
        // MAKE LOG FILE //
        ///////////////////
        
        std::ofstream LogStream;
        LogStream.open(StringManipulation::StringCast(IO::OutputDirectory,"InfoID",MY_MPI_RNG_SEED,".log").c_str());
        
        LogStream.precision(OUTPUT_PRECISION);
        
        // INFO FILE  //
        LogStream << "#PARAMETER LOG FOR MC FOUR DIPOLE INTEGRATED SPECTRA" << std::endl;
        LogStream << "#iMax=" << iMax << std::endl;
        LogStream << "#Nc=" << Nc << std::endl;
        LogStream << "#nWilsonLines=" << nWilsonLines << std::endl;
        LogStream << "#vnMaxPlusOne=" << vnMaxPlusOne << std::endl;
        LogStream << "#m=" << m << std::endl;
        LogStream << "#B=" << B << std::endl;
        LogStream << "#Bk=" << Bk << std::endl;
        LogStream << "#Bb=" << Bb << std::endl;
        LogStream << "#g4muSQR=" << g4muSQR << std::endl;
        LogStream << "#pMin=" << pMin << std::endl;
        LogStream << "#pAbsMax=" << pAbsMax << std::endl;
        LogStream << "#dp=" << dp << std::endl;
        LogStream << "#pMaxIndexMax=" << pMaxIndexMax << std::endl;
        LogStream << "#psiIndexMax=" << psiIndexMax << std::endl;
        
        std::cerr << "#m=" << m << " #Nc=" << Nc << " #B=" << B << " #Bb=" << Bb << " #Bk=" << Bk << " #g4muSQR=" << g4muSQR << " #iMax=" << iMax << std::endl;
        
        // CLOSE OUT-STREAM //
        LogStream.close();
        
        // COMMANDLINE OUTPUT //
        std::cout << "#COMPUTING VALUES" << std::endl;
        
        // RESET TIMING //
        Timing::Reset();
        
        std::cout << "#CALCULATING NORMS" << std::endl;
        
        // SET NORMS //
        double* Norm=new double[vnMaxPlusOne*(pMaxIndexMax+1)*(NumSums+1)];
        
        // SET ZERO //
        for(int sign=0;sign<2;sign++){
            for(int pIndex=0;pIndex<=pMaxIndexMax;pIndex++){
                for(int n=0;n<vnMaxPlusOne;n++){
                    
                    Norm[ResultIndex(sign,pIndex,n)]=0.0;
                    
                }
                
            }
        }
        // IntegralFlag= 0 KERN>0, 1 KERN<0, 2 KERN, 3 abs(KERN), //
        
        // DO INTEGRATIONS //
        for(int pMaxIndex=0;pMaxIndex<=pMaxIndexMax;pMaxIndex++){
            for(int n=0;n<vnMaxPlusOne;n++){
                //if(n!=1){
                if(n==5 || n==3 || n==2){
                    for(int sign=0;sign<2;sign++){
                        
                        // BEGIN BY HAND OPTION //
                        double pMax=pMin+dp*pMaxIndex;
                        
                        // SIGN = +1 | J_n>0 --SIGN = -1 | J_n<0//
                        Norm[ResultIndex(sign,pMaxIndex,n)]=PDF::GetNormalizationIntegrated(sign,pMax,n,Bk);
                        
                    }
                }
            }
        }
        
        
        // GET TIMING
        double time=Timing::Get();
        
        std::cerr << "#IN t=" << time << " s NORMS WERE CALCULATED" << std::endl;
        
        // COMMANDLINE OUTPUT //
        std::cout << "#COMPUTING VALUES" << std::endl;
        
        // RESET TIMING //
        Timing::Reset();
        // INDEX FOR BOTH POSITIVE POSITIVE, NEGATIVE NEGATIVE, AND POSITIVE NEGATIVE VALUES
        int i[NumSums];
        
        // DEFINE SIGN VARIABLES //
        int IntegrandSign=0, sign1=0, sign2=0, sign3=0, sign4=0;
        
        // FILE OUTPUT
        std::ofstream OutputStream;
        
        OutputStream.open(StringManipulation::StringCast(IO::OutputDirectory,"IntegratedSpectra_Bb",Bb,"Bk",Bk,"g4muSQR",g4muSQR,"ID",MY_MPI_RNG_SEED,".txt").c_str());
        
        OutputStream.precision(OUTPUT_PRECISION);
        
        // GET AVERAGE AND VALUES FOR ERROR //
        for(int pMaxIndex=0;pMaxIndex<=pMaxIndexMax;pMaxIndex++){
            
            // DEFINE MOMENTUM //
            double pMax=pMin+dp*pMaxIndex;
            
            // OUTPUT MOMENTA //
            OutputStream << pMax << " ";
            
            // INTEGRATE OVER iMax POINTS //
            for(int n2=0;n2<(vnMaxPlusOne-2);n2++){
                for(int n1=0;n1<vnMaxPlusOne;n1++){
                    // pt-0 c00-1 dc00-2 c22-3 dc22-4 c32-5 dc32-6 c42-7 dc42-8 c52-9 dc52-10 c43-11 dc43-12 c53-13 dc53-14
                    // (0,0) (2,2) (3,2) (4,2) (5,2) (4,3) (5,3)
                    //if((n1!=1 && n2!=1) && ((n1==0 && n2==0) || (n1==2 && n2==2) || (n1!=0 && n2!=0 && n1>n2))){
                    if((n1==5 && n2==2) || (n1==5 && n2==3)){

                        std::cerr << "#pMaxIndex=" << pMaxIndex << " n1=" << n1 << " n2=" << n2 << std::endl;
                        
                        // RESET COUNTS AND AVERAGES //
                        for(int config=0;config<NumSums;config++){
                            i[config]=0;
                            Mean[config]=0.0;
                            IntegrandSqr[config]=0.0;
                        }
                        // TOTAL POINTS COUNTED //
                        int iTot;
                        // COUNT TO TOTAL OF iMax //
                        do{
                            // RESET COUNTER
                            iTot=0;
                            
                            r1=PDF::SamplePDistrubtionInt(pMax,n1,Bk);
                            r2=PDF::SamplePDistrubtionInt(pMax,n2,Bk);
                            r3=PDF::SamplePDistrubtionInt(pMax,n1,Bk);
                            r4=PDF::SamplePDistrubtionInt(pMax,n2,Bk);
                            
                            // SET RADIUS VECTOR //
                            R1x= RandomNumberGenerator::Gauss(sqrt(Bb/2.0));
                            R1y= RandomNumberGenerator::Gauss(sqrt(Bb/2.0));
                            R2x= RandomNumberGenerator::Gauss(sqrt(Bb/2.0));
                            R2y= RandomNumberGenerator::Gauss(sqrt(Bb/2.0));
                            R3x= RandomNumberGenerator::Gauss(sqrt(Bb/2.0));
                            R3y= RandomNumberGenerator::Gauss(sqrt(Bb/2.0));
                            R4x= RandomNumberGenerator::Gauss(sqrt(Bb/2.0));
                            R4y= RandomNumberGenerator::Gauss(sqrt(Bb/2.0));
                            
                            // FIRST ANGLE //
                            phi13=2.0*PI*RandomNumberGenerator::rng();
                            phi24=2.0*PI*RandomNumberGenerator::rng();
                            
                            //std::cerr << r1 << " " << r2 << " " << r3 << " " << r4 << " " << R1x << " " << R1y << " " << R2x << " " << R2y << " " << R3x << " " << R3y << " " << R4x << " " << R4y << " " << phi13 << " " << phi24 << std::endl;
                            
//                            r1=0.687042;
//                            r2=0.811465;
//                            r3=2.11596;
//                            r4=1.32287;
//                            R1x=1.08834;
//                            R1y=0.175291;
//                            R2x=0.27248;
//                            R2y=1.87417;
//                            R3x=0.649161;
//                            R3y=-0.676164;
//                            R4x=-0.137144;
//                            R4y=0.946113;
//                            phi13=6.20663;
//                            phi24=4.34239;
                            
                            //Integrand=0.00459615
                            // INTEGRAND=0.0045961
                            // INTEGRAND=0.00459632
                            
                            // RESET //
                            Integrand = 0;
                            
                            //TRAPEZOID RULE ON PSI INTEGRAL
                            for(int psi24Index=0;psi24Index<=psiIndexMax;psi24Index++){
                                for(int psi13Index=0;psi13Index<=psiIndexMax;psi13Index++){
                                    
                                    // SECONARDY ANGLE //
                                    psi13=psi13Index*dpsi;
                                    psi24=psi24Index*dpsi;

                                    // DEFINE AZIMUTHAL ANGLES //
                                    phir1=phi13+psi13/2.0;
                                    phir2=phi24+psi24/2.0;
                                    phir3=phi13-psi13/2.0;
                                    phir4=phi24-psi24/2.0;
                                    
                                    r1x=r1*cos(phir1);
                                    r1y=r1*sin(phir1);
                                    r2x=r2*cos(phir2);
                                    r2y=r2*sin(phir2);
                                    r3x=r3*cos(phir3);
                                    r3y=r3*sin(phir3);
                                    r4x=r4*cos(phir4);
                                    r4y=r4*sin(phir4);
                                    
                                    // SET ACUTAL POINTS FOR DIPOLE CORRELATOR
                                    x[0][0]=(R1x+0.5*r1x);  x[0][1]=(R1y+0.5*r1y);
                                    x[1][0]=(R1x-0.5*r1x);  x[1][1]=(R1y-0.5*r1y);
                                    x[2][0]=(R2x+0.5*r2x);  x[2][1]=(R2y+0.5*r2y);
                                    x[3][0]=(R2x-0.5*r2x);  x[3][1]=(R2y-0.5*r2y);
                                    x[4][0]=(R3x+0.5*r3x);  x[4][1]=(R3y+0.5*r3y);
                                    x[5][0]=(R3x-0.5*r3x);  x[5][1]=(R3y-0.5*r3y);
                                    x[6][0]=(R4x+0.5*r4x);  x[6][1]=(R4y+0.5*r4y);
                                    x[7][0]=(R4x-0.5*r4x);  x[7][1]=(R4y-0.5*r4y);
                                    
                                    double Simpson1Coeff=(psi13Index==0 || psi13Index==psiIndexMax ? 1.0 : 2.0+2.0*(psi13Index%2))*dpsi/(6.0*PI);
                                    double Simpson2Coeff=(psi24Index==0 || psi24Index==psiIndexMax ? 1.0 : 2.0+2.0*(psi24Index%2))*dpsi/(6.0*PI);

                                    
                                    // SET MATRIX EXPONENTIAL RESULT //
                                    Integrand+=Simpson1Coeff*Simpson2Coeff*MatrixEvaluation(x,g4muSQR,m)*cos(double(n1)*psi13+double(n2)*psi24);
                                    //Integrand+=Simpson1Coeff*Simpson2Coeff*MatrixEvaluation(x[0][0],x[0][1],x[1][0],x[1][1],x[2][0],x[2][1],x[3][0],x[3][1],g4muSQR)*MatrixEvaluation(x[4][0],x[4][1],x[5][0],x[5][1],x[6][0],x[6][1],x[7][0],x[7][1],g4muSQR)*cos(double(n1)*psi13+double(n2)*psi24);

                                    
                                }// PSI1 INDEX
                            }// PSI2 INDEX
                            
                            // DETERMINE SIGN OF EACH DISTRIBUTION //
                            sign1=int(0.5*(1.0-PDF::PInt(n1,r1,pMax,Bk)/sqrt(PDF::PInt(n1,r1,pMax,Bk)*PDF::PInt(n1,r1,pMax,Bk))));
                            sign2=int(0.5*(1.0-PDF::PInt(n2,r2,pMax,Bk)/sqrt(PDF::PInt(n2,r2,pMax,Bk)*PDF::PInt(n2,r2,pMax,Bk))));
                            sign3=int(0.5*(1.0-PDF::PInt(n1,r3,pMax,Bk)/sqrt(PDF::PInt(n1,r3,pMax,Bk)*PDF::PInt(n1,r3,pMax,Bk))));
                            sign4=int(0.5*(1.0-PDF::PInt(n2,r4,pMax,Bk)/sqrt(PDF::PInt(n2,r4,pMax,Bk)*PDF::PInt(n2,r4,pMax,Bk))));
                            // DETERMINE BIN //
                            IntegrandSign=2.0*(2.0*(2.0*sign1+sign2)+sign3)+sign4;
                            
                            // INCREMENT COUNTER FOR GIVEN BIN //
                            i[IntegrandSign]++;
                            
                            // MULTIPLY BY DISTR NORMS //
                            Integrand*=Norm[ResultIndex(sign1,pMaxIndex,n1)]*Norm[ResultIndex(sign2,pMaxIndex,n2)]*Norm[ResultIndex(sign3,pMaxIndex,n1)]*Norm[ResultIndex(sign4,pMaxIndex,n2)];
                            // DETERMINE MEAN AND VARIANCE //
                            Mean[IntegrandSign]+=(Integrand-Mean[IntegrandSign])/double(i[IntegrandSign]);
                            IntegrandSqr[IntegrandSign]+=Integrand*Integrand;
                            
                            for(int j=0;j<NumSums;j++){
                                iTot+=i[j];
                            }
                            
                            //std::cerr << iTot << std::endl;
                            
                        }while(iTot<iMax);
                        
                        
                        // COMMANDLINE OUTPUT //
                        std::cerr << "#i= ";
                        
                        for(int j=0;j<NumSums;j++){
                            std::cerr << i[j] << " ";
                        }
                        std::cerr << " PTS COMPUTED " << std::endl;
                        
                        // UNCERTAINTY OF THE MEAN //
                        double varSqrMean[NumSums];
                        // POSITIVE POSITIVE VALUES
                        for(int config=0;config<NumSums;config++){
                            if(i[0]!=0){
                                
                                varSqrMean[config]=(IntegrandSqr[config]-double(i[config])*(Mean[config]*Mean[config]))/ (double(i[config])*(double(i[config])-1.0));
                                
                            }
                            else{
                                varSqrMean[config]=0.0;
                            }
                            
                        }
                        
                        double TotalMean=0.0;
                        // SUM OVER BINS //
                        for(int config=0;config<NumSums;config++){
                            if(i[config]>=100){
                                TotalMean+=Mean[config];
                                
                            }
                        }
                        double TotalUncertainty=0.0;
                        double TempUncertainty=0.0;
                        for(int config=0;config<NumSums;config++){
                            if(i[config]>=100){
                                TempUncertainty+=varSqrMean[config]*varSqrMean[config];
                            }
                        }
                        TotalUncertainty=std::pow(TempUncertainty,0.25);
                        // OUTPUT DATA AND UNCERTAINTY //
                        OutputStream << TotalMean << " " << TotalUncertainty << " ";
                    } // n!=1 LOOP
                    
                }// n1 LOOP
            }// n2 LOOP
            // NEW LINE //
            OutputStream << std::endl;
            
        }
        
        // CLOSE OUT-STREAM //
        OutputStream.close();
        
        // COMMANDLINE OUTPUT //
        std::cout << "#FINISHED COMPUTING VALUES" << std::endl;
        
        // CLEAN-UP //
        delete[] Mean;
        delete[] IntegrandSqr;
        
        delete[] Norm;
        
        for(int j=0;j<nWilsonLines;j++){
            delete[] x[j];
        }
        // COMMANDLINE OUTPUT //
        std::cout << "#DONE SAVING VALUES ID=" << MY_MPI_RNG_SEED << std::endl;
        
        
    }
}
