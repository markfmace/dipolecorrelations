namespace Integration{
    
    double TrapezoidIntegrationInf(double r0,double RelErr,double funct(double,int,double,int,double),int IntegralFlag,double p,int n,double Bpar){
                
        // ASSUMING ZERO AT INFINITY //
        double Result = 0.5*(funct(r0,IntegralFlag,p,n,Bpar));

        // IF WEIRD THINGS HAPPENING AT r0=0 //
        if(std::isnan(Result)==true && r0==0){
            Result = 0.0;
        }
        
        double dResult;
        double MaxIter=28.0;
        
        for(double N=2;N<=pow(2,MaxIter);N*=2){
            
            double dx = 1.0/N;
            
            dResult = 0.0;
            for(int i=1;i<N;i+=2){
                double t = dx*i;
                dResult += 1.0/(t*t)*funct(r0+(1.0-t)/t,IntegralFlag,p,n,Bpar);
            }
            
            if(fabs(2.0*dResult/(dResult+Result)-1.0)<RelErr/10.0){
                Result += dResult;
                
                return Result/N;
            }
            Result += dResult;
        }
        
        std::cerr << "ERROR -- #IntegralFlag=" << IntegralFlag << " p=" << p << " n=" << n << " B=" << Bpar << " result=" << std::setprecision(16) << Result/std::pow(2,MaxIter) << std::endl;
        
        return Result/std::pow(2,MaxIter);
    }


}
