#include <gsl/gsl_math.h>
#include <gsl/gsl_linalg.h>

// NOTE: THIS IS ROW MAJOR ORDERING

// a_00 a_01
// a_10 a_11
// =
// a_0 a_1
// a_2 a_3

class Inversion {
    
private:
    
    // LINEAR DIMENSION OF THE MATRIX //
    int MatrixSize;
    
private:
    
    // MATRIX //
    std::complex<double> *NonsymmMatrix;
    
    // INVERSE MATRIX
    std::complex<double> *InverseMatrix;
    
    
public:
    
    // ACCESS INVERSE //
    void GetInverse(std::complex<double> *InvertedMatrix){
        
        for(int i=0;i<MatrixSize;i++){
            for(int j=0;j<MatrixSize;j++){
                
                InvertedMatrix[i*MatrixSize+j]=InverseMatrix[i*MatrixSize+j];
            }
        }
        
    }
    
public:
    
    // INVERSION USING GSL LIBRARY //
    void Invert(){
        
        int s;
        
        // COPY MATRIX //
        std::complex<double> *CopyMatrix=new std::complex<double>[MatrixSize*MatrixSize];
        std::memcpy(CopyMatrix,this->NonsymmMatrix,MatrixSize*MatrixSize*sizeof(std::complex<double>));
        
        // SET GSL MATRIX //
        gsl_matrix_complex_view GSLMatrix = gsl_matrix_complex_view_array (reinterpret_cast<double*>(CopyMatrix),MatrixSize,MatrixSize);
        
        // GSL ALLOCATION //
        gsl_matrix_complex *InvMat = gsl_matrix_complex_alloc (MatrixSize, MatrixSize);
        
        gsl_permutation * p = gsl_permutation_alloc (MatrixSize);
        
        // INVERT //
        gsl_linalg_complex_LU_decomp (&GSLMatrix.matrix, p, &s);
        
        gsl_linalg_complex_LU_invert (&GSLMatrix.matrix, p, InvMat);
        
        
        // SET INVERSE //
        for (int i=0;i<MatrixSize;i++){
            
            // SET EIGENVECTOR //
            for(int j=0;j<MatrixSize;j++){
                
                gsl_complex val=gsl_matrix_complex_get(InvMat,i,j);
                
                this->InverseMatrix[i*MatrixSize+j]=std::complex<double>(GSL_REAL(val),GSL_IMAG(val));
                
            }
            
        }
        
        // CLEANUP //
        gsl_permutation_free (p);
        
        gsl_matrix_complex_free (InvMat);
        
        delete[] CopyMatrix;
    }
  /*
public:
    
    // CHECK THAT INVERSION IS CORRECT //
    void Check(){
        
        int CHECKVALUE=1;
        
        for(int s=0;s<MatrixSize;s++){
            
            for(int i=0;i<MatrixSize;i++){
                
                std::complex<double> qVal=0.0;
                
                for(int j=0;j<MatrixSize;j++){
                    if(i==j){
                        qVal=std::complex<double>(1.0);
                    }
                    else{
                        qVal=std::complex<double>(0.0);
                        
                    }
                }
                
                if(std::abs(qVal-EigenValues[s]*EigenVectors[i+s*MatrixSize])>std::pow(10.0,-10)){
                    CHECKVALUE=0;
                    
                    std::cerr << s <<  " " << std::abs(qVal-EigenValues[s]*EigenVectors[i+s*MatrixSize]) << std::endl;
                }
                
            }
            
        }
        
        if(CHECKVALUE==1){
            std::cerr << "#INVERSION SUCCESSFUL" << std::endl;
        }
        else{
            std::cerr << "#INVERSION FAILED" << std::endl;
            exit(0);
        }
        
    }
  
public:
    
    void Output(std::string fname){
        
        std::ofstream OutStream;
        OutStream.open(fname.c_str());
        
        for(int s=0;s<MatrixSize;s++){
            
            OutStream << "#EVal= " << real(EigenValues[s]) << " " << imag(EigenValues[s]) << std::endl;
            for(int i=0;i<MatrixSize;i++){
                OutStream << real(EigenVectors[i+s*MatrixSize]) << " " << imag(EigenVectors[i+s*MatrixSize]) << " ";
            }
            OutStream << std::endl;
            
        }
        
        OutStream.close();
        
    }
  */    
    
public:
    
    // CONSTRUCTOR //
    Inversion(std::complex<double> *InputMatrix,int InputSize){
        
        // SETUP //
        this->MatrixSize=InputSize;
        
        InverseMatrix=new std::complex<double>[MatrixSize*MatrixSize];
        
        // SET MATRIX //
        this->NonsymmMatrix=new std::complex<double>[MatrixSize*MatrixSize];
        std::memcpy(this->NonsymmMatrix,InputMatrix,MatrixSize*MatrixSize*sizeof(std::complex<double>));
        
    }
    
    // DESTRUCTOR //
    ~Inversion(){
        
        delete[] NonsymmMatrix;
        delete[] InverseMatrix;
        
    }
    
};
