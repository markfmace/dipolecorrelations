#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>

// NOTE: THIS IS ROW MAJOR ORDERING

// a_00 a_01
// a_10 a_11
// =
// a_0 a_1
// a_2 a_3

class Eigensystem {
    
private:
    
    // LINEAR DIMENSION OF THE MATRIX //
    int MatrixSize;
    
private:
    
    // MATRIX //
    double *RealNonsymmMatrix;
    
    // EIGENVECTOR AND EIGENVALUES //
    std::complex<double> *EigenValues;
    std::complex<double> *EigenVectors;
    
public:
    
    // ACCESS EIGENMODES //
    void GetEigenMode(int s,std::complex<double> &eVal,std::complex<double> *eVec){
        
        eVal=EigenValues[s];
        
        for(int i=0;i<MatrixSize;i++){
            eVec[i]=EigenVectors[i+s*MatrixSize];
        }
        
    }
    
    // ACCESS EIGENMODES //
    void SetEigenMode(int s,std::complex<double> eVal,std::complex<double> *eVec){
        
        EigenValues[s]=eVal;
        
        for(int i=0;i<MatrixSize;i++){
            EigenVectors[i+s*MatrixSize]=eVec[i];
        }
        
    }
    
public:
    
    // DIAGONALIZATION USING GSL LIBRARY //
    void Diagonalize(){
        
        // COPY MATRIX //
        double *CopyMatrix=new double[MatrixSize*MatrixSize];
        std::memcpy(CopyMatrix,this->RealNonsymmMatrix,MatrixSize*MatrixSize*sizeof(double));
        
        // SET GSL MATRIX //
        gsl_matrix_view GSLMatrix = gsl_matrix_view_array (reinterpret_cast<double*>(CopyMatrix),MatrixSize,MatrixSize);
        
        // GSL ALLOCATION //
        gsl_vector_complex *eVals = gsl_vector_complex_alloc (MatrixSize);
        gsl_matrix_complex *eVecs = gsl_matrix_complex_alloc (MatrixSize, MatrixSize);
        
        // DIAGONALIZE //
        gsl_eigen_nonsymmv_workspace * GSLMatrixWorkspace = gsl_eigen_nonsymmv_alloc (MatrixSize);
        gsl_eigen_nonsymmv (&GSLMatrix.matrix, eVals, eVecs, GSLMatrixWorkspace);
        gsl_eigen_nonsymmv_free (GSLMatrixWorkspace);
        
        // SORT EIGENVALUES //
        //gsl_eigen_nonsymmv_sort (eVals, eVecs, GSL_EIGEN_SORT_VAL_DESC);

        // SET EIGENSYSTEM //
        for (int s=0;s<MatrixSize;s++){
            
            gsl_complex eval=gsl_vector_complex_get(eVals, s);
            // SET EIGENVALUE //
            this->EigenValues[s]=std::complex<double>(GSL_REAL(eval),GSL_IMAG(eval));
        
            // SET EIGENVECTOR //
            for(int j=0;j<MatrixSize;j++){
                
                gsl_complex val=gsl_matrix_complex_get(eVecs,j,s);

                this->EigenVectors[j+s*MatrixSize]=std::complex<double>(GSL_REAL(val),GSL_IMAG(val));

            }
            
        }
        
        // CLEANUP //
        gsl_vector_complex_free (eVals);
        gsl_matrix_complex_free (eVecs);
        
        delete[] CopyMatrix;
    }
    
public:
    
    // CHECK THAT EIGENFUNCTIONS ARE CORRECT //
    void Check(){
        
        int CHECKVALUE=1;
        
        for(int s=0;s<MatrixSize;s++){
            
            for(int i=0;i<MatrixSize;i++){
                
                std::complex<double> qVal=0.0;
                
                for(int j=0;j<MatrixSize;j++){
                    qVal+=RealNonsymmMatrix[i*MatrixSize+j]*EigenVectors[j+s*MatrixSize];
                }
                
                if(std::abs(qVal-EigenValues[s]*EigenVectors[i+s*MatrixSize])>std::pow(10.0,-10)){
                    CHECKVALUE=0;
                    
                    std::cerr << s <<  " " << std::abs(qVal-EigenValues[s]*EigenVectors[i+s*MatrixSize]) << std::endl;
                }
                
            }
            
        }
        
        if(CHECKVALUE==1){
            std::cerr << "#DIAGONALIZATION SUCCESSFUL" << std::endl;
        }
        else{
            std::cerr << "#DIAGONALIZATION FAILED" << std::endl;
            exit(0);
        }
        
    }
    
public:
    
    void Output(std::string fname){
        
        std::ofstream OutStream;
        OutStream.open(fname.c_str());
        
        for(int s=0;s<MatrixSize;s++){
            
            OutStream << "#EVal= " << real(EigenValues[s]) << " " << imag(EigenValues[s]) << std::endl;
            for(int i=0;i<MatrixSize;i++){
                OutStream << real(EigenVectors[i+s*MatrixSize]) << " " << imag(EigenVectors[i+s*MatrixSize]) << " ";
            }
            OutStream << std::endl;
            
        }
        
        OutStream.close();
        
    }
    
    
public:
    
    // CONSTRUCTOR //
    Eigensystem(double *InputMatrix,int InputSize){
        
        // SETUP //
        this->MatrixSize=InputSize;
        
        EigenVectors=new std::complex<double>[MatrixSize*MatrixSize];
        EigenValues=new std::complex<double>[MatrixSize];
        
        // SET MATRIX //
        this->RealNonsymmMatrix=new double[MatrixSize*MatrixSize];
        std::memcpy(this->RealNonsymmMatrix,InputMatrix,MatrixSize*MatrixSize*sizeof(double));
        
    }
    
    // DESTRUCTOR //
    ~Eigensystem(){
        
        delete[] EigenVectors;
        delete[] EigenValues;
        
        delete[] RealNonsymmMatrix;
    }
    
};
