// DIS AAMQS FORM
//Lloc(r) = (g^4μ^2*r^2)/4/Log[1/(r*Lqcd) + Exp[1]]
//Λqcd ≡ (m/2)exp(γE−1/2)
double ec=1;
double Lloc(double ux,double uy,double vx,double vy,double Lambda){
    
    if(ux==vx && uy==vy){
        return 0.0;
    }
    else{
        return -std::pow(EuclideanNorm(ux,uy,vx,vy),2)/(16.0*PI)*std::log(1.0/(EuclideanNorm(ux,uy,vx,vy)*Lambda)+2.71828182845904509*ec);
    }
    
}

// 0-0 0-1
// 1-0 1-1
//
// 2-0 2-1
// 3-0 3-1
//
// 4-0 4-1
// 5-0 5-1
//
// 6-0 6-1
// 7-0 7-1
//
// 8-0 8-1
// 9-0 9-1
//
// 10-0 10-1
// 11-0 11-1

double MatrixEvaluation(double **x,double g4muSQR,double Lambda,int nD){
    double tot=0;
    
    if(nD==2){
        tot=-Lloc(x[0][0],x[0][1],x[2][0],x[2][1],Lambda)-Lloc(x[1][0],x[1][1],x[3][0],x[3][1],Lambda)+Lloc(x[0][0],x[0][1],x[1][0],x[1][1],Lambda)+Lloc(x[0][0],x[0][1],x[3][0],x[3][1],Lambda)+Lloc(x[1][0],x[1][1],x[2][0],x[2][1],Lambda)+Lloc(x[2][0],x[2][1],x[3][0],x[3][1],Lambda);

    }
    else if(nD==4){
        tot=Lloc(x[0][0],x[0][1],x[1][0],x[1][1],Lambda)-Lloc(x[0][0],x[0][1],x[2][0],x[2][1],Lambda)+Lloc(x[1][0],x[1][1],x[2][0],x[2][1],Lambda)+Lloc(x[0][0],x[0][1],x[3][0],x[3][1],Lambda)-Lloc(x[1][0],x[1][1],x[3][0],x[3][1],Lambda)+Lloc(x[2][0],x[2][1],x[3][0],x[3][1],Lambda)-Lloc(x[0][0],x[0][1],x[4][0],x[4][1],Lambda)+Lloc(x[1][0],x[1][1],x[4][0],x[4][1],Lambda)-Lloc(x[2][0],x[2][1],x[4][0],x[4][1],Lambda)+Lloc(x[3][0],x[3][1],x[4][0],x[4][1],Lambda)+Lloc(x[0][0],x[0][1],x[5][0],x[5][1],Lambda)-Lloc(x[1][0],x[1][1],x[5][0],x[5][1],Lambda)+Lloc(x[2][0],x[2][1],x[5][0],x[5][1],Lambda)-Lloc(x[3][0],x[3][1],x[5][0],x[5][1],Lambda)+Lloc(x[4][0],x[4][1],x[5][0],x[5][1],Lambda)-Lloc(x[0][0],x[0][1],x[6][0],x[6][1],Lambda)+Lloc(x[1][0],x[1][1],x[6][0],x[6][1],Lambda)-Lloc(x[2][0],x[2][1],x[6][0],x[6][1],Lambda)+Lloc(x[3][0],x[3][1],x[6][0],x[6][1],Lambda)-Lloc(x[4][0],x[4][1],x[6][0],x[6][1],Lambda)+Lloc(x[5][0],x[5][1],x[6][0],x[6][1],Lambda)+Lloc(x[0][0],x[0][1],x[7][0],x[7][1],Lambda)-Lloc(x[1][0],x[1][1],x[7][0],x[7][1],Lambda)+Lloc(x[2][0],x[2][1],x[7][0],x[7][1],Lambda)-Lloc(x[3][0],x[3][1],x[7][0],x[7][1],Lambda)+Lloc(x[4][0],x[4][1],x[7][0],x[7][1],Lambda)-Lloc(x[5][0],x[5][1],x[7][0],x[7][1],Lambda)+Lloc(x[6][0],x[6][1],x[7][0],x[7][1],Lambda);
    }
    else if(nD==6){
    
        tot=Lloc(x[0][0],x[0][1],x[1][0],x[1][1],Lambda)-Lloc(x[0][0],x[0][1],x[2][0],x[2][1],Lambda)+Lloc(x[1][0],x[1][1],x[2][0],x[2][1],Lambda)+Lloc(x[0][0],x[0][1],x[3][0],x[3][1],Lambda)-Lloc(x[1][0],x[1][1],x[3][0],x[3][1],Lambda)+Lloc(x[2][0],x[2][1],x[3][0],x[3][1],Lambda)-Lloc(x[0][0],x[0][1],x[4][0],x[4][1],Lambda)+Lloc(x[1][0],x[1][1],x[4][0],x[4][1],Lambda)-Lloc(x[2][0],x[2][1],x[4][0],x[4][1],Lambda)+Lloc(x[3][0],x[3][1],x[4][0],x[4][1],Lambda)+Lloc(x[0][0],x[0][1],x[5][0],x[5][1],Lambda)-Lloc(x[1][0],x[1][1],x[5][0],x[5][1],Lambda)+Lloc(x[2][0],x[2][1],x[5][0],x[5][1],Lambda)-Lloc(x[3][0],x[3][1],x[5][0],x[5][1],Lambda)+Lloc(x[4][0],x[4][1],x[5][0],x[5][1],Lambda)-Lloc(x[0][0],x[0][1],x[6][0],x[6][1],Lambda)+Lloc(x[1][0],x[1][1],x[6][0],x[6][1],Lambda)-Lloc(x[2][0],x[2][1],x[6][0],x[6][1],Lambda)+Lloc(x[3][0],x[3][1],x[6][0],x[6][1],Lambda)-Lloc(x[4][0],x[4][1],x[6][0],x[6][1],Lambda)+Lloc(x[5][0],x[5][1],x[6][0],x[6][1],Lambda)+Lloc(x[0][0],x[0][1],x[7][0],x[7][1],Lambda)-Lloc(x[1][0],x[1][1],x[7][0],x[7][1],Lambda)+Lloc(x[2][0],x[2][1],x[7][0],x[7][1],Lambda)-Lloc(x[3][0],x[3][1],x[7][0],x[7][1],Lambda)+Lloc(x[4][0],x[4][1],x[7][0],x[7][1],Lambda)-Lloc(x[5][0],x[5][1],x[7][0],x[7][1],Lambda)+Lloc(x[6][0],x[6][1],x[7][0],x[7][1],Lambda)-Lloc(x[0][0],x[0][1],x[8][0],x[8][1],Lambda)+Lloc(x[1][0],x[1][1],x[8][0],x[8][1],Lambda)-Lloc(x[2][0],x[2][1],x[8][0],x[8][1],Lambda)+Lloc(x[3][0],x[3][1],x[8][0],x[8][1],Lambda)-Lloc(x[4][0],x[4][1],x[8][0],x[8][1],Lambda)+Lloc(x[5][0],x[5][1],x[8][0],x[8][1],Lambda)-Lloc(x[6][0],x[6][1],x[8][0],x[8][1],Lambda)+Lloc(x[7][0],x[7][1],x[8][0],x[8][1],Lambda)+Lloc(x[0][0],x[0][1],x[9][0],x[9][1],Lambda)-Lloc(x[1][0],x[1][1],x[9][0],x[9][1],Lambda)+Lloc(x[2][0],x[2][1],x[9][0],x[9][1],Lambda)-Lloc(x[3][0],x[3][1],x[9][0],x[9][1],Lambda)+Lloc(x[4][0],x[4][1],x[9][0],x[9][1],Lambda)-Lloc(x[5][0],x[5][1],x[9][0],x[9][1],Lambda)+Lloc(x[6][0],x[6][1],x[9][0],x[9][1],Lambda)-Lloc(x[7][0],x[7][1],x[9][0],x[9][1],Lambda)+Lloc(x[8][0],x[8][1],x[9][0],x[9][1],Lambda)-Lloc(x[0][0],x[0][1],x[10][0],x[10][1],Lambda)+Lloc(x[1][0],x[1][1],x[10][0],x[10][1],Lambda)-Lloc(x[2][0],x[2][1],x[10][0],x[10][1],Lambda)+Lloc(x[3][0],x[3][1],x[10][0],x[10][1],Lambda)-Lloc(x[4][0],x[4][1],x[10][0],x[10][1],Lambda)+Lloc(x[5][0],x[5][1],x[10][0],x[10][1],Lambda)-Lloc(x[6][0],x[6][1],x[10][0],x[10][1],Lambda)+Lloc(x[7][0],x[7][1],x[10][0],x[10][1],Lambda)-Lloc(x[8][0],x[8][1],x[10][0],x[10][1],Lambda)+Lloc(x[9][0],x[9][1],x[10][0],x[10][1],Lambda)+Lloc(x[0][0],x[0][1],x[11][0],x[11][1],Lambda)-Lloc(x[1][0],x[1][1],x[11][0],x[11][1],Lambda)+Lloc(x[2][0],x[2][1],x[11][0],x[11][1],Lambda)-Lloc(x[3][0],x[3][1],x[11][0],x[11][1],Lambda)+Lloc(x[4][0],x[4][1],x[11][0],x[11][1],Lambda)-Lloc(x[5][0],x[5][1],x[11][0],x[11][1],Lambda)+Lloc(x[6][0],x[6][1],x[11][0],x[11][1],Lambda)-Lloc(x[7][0],x[7][1],x[11][0],x[11][1],Lambda)+Lloc(x[8][0],x[8][1],x[11][0],x[11][1],Lambda)-Lloc(x[9][0],x[9][1],x[11][0],x[11][1],Lambda)+Lloc(x[10][0],x[10][1],x[11][0],x[11][1],Lambda);

    }
    else if(nD==8){
        tot=Lloc(x[0][0],x[0][1],x[1][0],x[1][1],Lambda)-Lloc(x[0][0],x[0][1],x[2][0],x[2][1],Lambda)+Lloc(x[1][0],x[1][1],x[2][0],x[2][1],Lambda)+Lloc(x[0][0],x[0][1],x[3][0],x[3][1],Lambda)-Lloc(x[1][0],x[1][1],x[3][0],x[3][1],Lambda)+Lloc(x[2][0],x[2][1],x[3][0],x[3][1],Lambda)-Lloc(x[0][0],x[0][1],x[4][0],x[4][1],Lambda)+Lloc(x[1][0],x[1][1],x[4][0],x[4][1],Lambda)-Lloc(x[2][0],x[2][1],x[4][0],x[4][1],Lambda)+Lloc(x[3][0],x[3][1],x[4][0],x[4][1],Lambda)+Lloc(x[0][0],x[0][1],x[5][0],x[5][1],Lambda)-Lloc(x[1][0],x[1][1],x[5][0],x[5][1],Lambda)+Lloc(x[2][0],x[2][1],x[5][0],x[5][1],Lambda)-Lloc(x[3][0],x[3][1],x[5][0],x[5][1],Lambda)+Lloc(x[4][0],x[4][1],x[5][0],x[5][1],Lambda)-Lloc(x[0][0],x[0][1],x[6][0],x[6][1],Lambda)+Lloc(x[1][0],x[1][1],x[6][0],x[6][1],Lambda)-Lloc(x[2][0],x[2][1],x[6][0],x[6][1],Lambda)+Lloc(x[3][0],x[3][1],x[6][0],x[6][1],Lambda)-Lloc(x[4][0],x[4][1],x[6][0],x[6][1],Lambda)+Lloc(x[5][0],x[5][1],x[6][0],x[6][1],Lambda)+Lloc(x[0][0],x[0][1],x[7][0],x[7][1],Lambda)-Lloc(x[1][0],x[1][1],x[7][0],x[7][1],Lambda)+Lloc(x[2][0],x[2][1],x[7][0],x[7][1],Lambda)-Lloc(x[3][0],x[3][1],x[7][0],x[7][1],Lambda)+Lloc(x[4][0],x[4][1],x[7][0],x[7][1],Lambda)-Lloc(x[5][0],x[5][1],x[7][0],x[7][1],Lambda)+Lloc(x[6][0],x[6][1],x[7][0],x[7][1],Lambda)-Lloc(x[0][0],x[0][1],x[8][0],x[8][1],Lambda)+Lloc(x[1][0],x[1][1],x[8][0],x[8][1],Lambda)-Lloc(x[2][0],x[2][1],x[8][0],x[8][1],Lambda)+Lloc(x[3][0],x[3][1],x[8][0],x[8][1],Lambda)-Lloc(x[4][0],x[4][1],x[8][0],x[8][1],Lambda)+Lloc(x[5][0],x[5][1],x[8][0],x[8][1],Lambda)-Lloc(x[6][0],x[6][1],x[8][0],x[8][1],Lambda)+Lloc(x[7][0],x[7][1],x[8][0],x[8][1],Lambda)+Lloc(x[0][0],x[0][1],x[9][0],x[9][1],Lambda)-Lloc(x[1][0],x[1][1],x[9][0],x[9][1],Lambda)+Lloc(x[2][0],x[2][1],x[9][0],x[9][1],Lambda)-Lloc(x[3][0],x[3][1],x[9][0],x[9][1],Lambda)+Lloc(x[4][0],x[4][1],x[9][0],x[9][1],Lambda)-Lloc(x[5][0],x[5][1],x[9][0],x[9][1],Lambda)+Lloc(x[6][0],x[6][1],x[9][0],x[9][1],Lambda)-Lloc(x[7][0],x[7][1],x[9][0],x[9][1],Lambda)+Lloc(x[8][0],x[8][1],x[9][0],x[9][1],Lambda)-Lloc(x[0][0],x[0][1],x[10][0],x[10][1],Lambda)+Lloc(x[1][0],x[1][1],x[10][0],x[10][1],Lambda)-Lloc(x[2][0],x[2][1],x[10][0],x[10][1],Lambda)+Lloc(x[3][0],x[3][1],x[10][0],x[10][1],Lambda)-Lloc(x[4][0],x[4][1],x[10][0],x[10][1],Lambda)+Lloc(x[5][0],x[5][1],x[10][0],x[10][1],Lambda)-Lloc(x[6][0],x[6][1],x[10][0],x[10][1],Lambda)+Lloc(x[7][0],x[7][1],x[10][0],x[10][1],Lambda)-Lloc(x[8][0],x[8][1],x[10][0],x[10][1],Lambda)+Lloc(x[9][0],x[9][1],x[10][0],x[10][1],Lambda)+Lloc(x[0][0],x[0][1],x[11][0],x[11][1],Lambda)-Lloc(x[1][0],x[1][1],x[11][0],x[11][1],Lambda)+Lloc(x[2][0],x[2][1],x[11][0],x[11][1],Lambda)-Lloc(x[3][0],x[3][1],x[11][0],x[11][1],Lambda)+Lloc(x[4][0],x[4][1],x[11][0],x[11][1],Lambda)-Lloc(x[5][0],x[5][1],x[11][0],x[11][1],Lambda)+Lloc(x[6][0],x[6][1],x[11][0],x[11][1],Lambda)-Lloc(x[7][0],x[7][1],x[11][0],x[11][1],Lambda)+Lloc(x[8][0],x[8][1],x[11][0],x[11][1],Lambda)-Lloc(x[9][0],x[9][1],x[11][0],x[11][1],Lambda)+Lloc(x[10][0],x[10][1],x[11][0],x[11][1],Lambda)-Lloc(x[0][0],x[0][1],x[12][0],x[12][1],Lambda)+Lloc(x[1][0],x[1][1],x[12][0],x[12][1],Lambda)-Lloc(x[2][0],x[2][1],x[12][0],x[12][1],Lambda)+Lloc(x[3][0],x[3][1],x[12][0],x[12][1],Lambda)-Lloc(x[4][0],x[4][1],x[12][0],x[12][1],Lambda)+Lloc(x[5][0],x[5][1],x[12][0],x[12][1],Lambda)-Lloc(x[6][0],x[6][1],x[12][0],x[12][1],Lambda)+Lloc(x[7][0],x[7][1],x[12][0],x[12][1],Lambda)-Lloc(x[8][0],x[8][1],x[12][0],x[12][1],Lambda)+Lloc(x[9][0],x[9][1],x[12][0],x[12][1],Lambda)-Lloc(x[10][0],x[10][1],x[12][0],x[12][1],Lambda)+Lloc(x[11][0],x[11][1],x[12][0],x[12][1],Lambda)+Lloc(x[0][0],x[0][1],x[13][0],x[13][1],Lambda)-Lloc(x[1][0],x[1][1],x[13][0],x[13][1],Lambda)+Lloc(x[2][0],x[2][1],x[13][0],x[13][1],Lambda)-Lloc(x[3][0],x[3][1],x[13][0],x[13][1],Lambda)+Lloc(x[4][0],x[4][1],x[13][0],x[13][1],Lambda)-Lloc(x[5][0],x[5][1],x[13][0],x[13][1],Lambda)+Lloc(x[6][0],x[6][1],x[13][0],x[13][1],Lambda)-Lloc(x[7][0],x[7][1],x[13][0],x[13][1],Lambda)+Lloc(x[8][0],x[8][1],x[13][0],x[13][1],Lambda)-Lloc(x[9][0],x[9][1],x[13][0],x[13][1],Lambda)+Lloc(x[10][0],x[10][1],x[13][0],x[13][1],Lambda)-Lloc(x[11][0],x[11][1],x[13][0],x[13][1],Lambda)+Lloc(x[12][0],x[12][1],x[13][0],x[13][1],Lambda)-Lloc(x[0][0],x[0][1],x[14][0],x[14][1],Lambda)+Lloc(x[1][0],x[1][1],x[14][0],x[14][1],Lambda)-Lloc(x[2][0],x[2][1],x[14][0],x[14][1],Lambda)+Lloc(x[3][0],x[3][1],x[14][0],x[14][1],Lambda)-Lloc(x[4][0],x[4][1],x[14][0],x[14][1],Lambda)+Lloc(x[5][0],x[5][1],x[14][0],x[14][1],Lambda)-Lloc(x[6][0],x[6][1],x[14][0],x[14][1],Lambda)+Lloc(x[7][0],x[7][1],x[14][0],x[14][1],Lambda)-Lloc(x[8][0],x[8][1],x[14][0],x[14][1],Lambda)+Lloc(x[9][0],x[9][1],x[14][0],x[14][1],Lambda)-Lloc(x[10][0],x[10][1],x[14][0],x[14][1],Lambda)+Lloc(x[11][0],x[11][1],x[14][0],x[14][1],Lambda)-Lloc(x[12][0],x[12][1],x[14][0],x[14][1],Lambda)+Lloc(x[13][0],x[13][1],x[14][0],x[14][1],Lambda)+Lloc(x[0][0],x[0][1],x[15][0],x[15][1],Lambda)-Lloc(x[1][0],x[1][1],x[15][0],x[15][1],Lambda)+Lloc(x[2][0],x[2][1],x[15][0],x[15][1],Lambda)-Lloc(x[3][0],x[3][1],x[15][0],x[15][1],Lambda)+Lloc(x[4][0],x[4][1],x[15][0],x[15][1],Lambda)-Lloc(x[5][0],x[5][1],x[15][0],x[15][1],Lambda)+Lloc(x[6][0],x[6][1],x[15][0],x[15][1],Lambda)-Lloc(x[7][0],x[7][1],x[15][0],x[15][1],Lambda)+Lloc(x[8][0],x[8][1],x[15][0],x[15][1],Lambda)-Lloc(x[9][0],x[9][1],x[15][0],x[15][1],Lambda)+Lloc(x[10][0],x[10][1],x[15][0],x[15][1],Lambda)-Lloc(x[11][0],x[11][1],x[15][0],x[15][1],Lambda)+Lloc(x[12][0],x[12][1],x[15][0],x[15][1],Lambda)-Lloc(x[13][0],x[13][1],x[15][0],x[15][1],Lambda)+Lloc(x[14][0],x[14][1],x[15][0],x[15][1],Lambda);
    
    }
    else{
        std::cerr << "# WRONG NUMBER OF DIPOLES" << std::endl;
        exit(0);
    }
    
    return exp(g4muSQR*tot);
}
