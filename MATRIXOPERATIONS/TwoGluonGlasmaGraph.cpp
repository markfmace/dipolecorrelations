// DEFINE NUMERICAL GLUON PROPOGATOR FUNCTION L(u,v) //
// BESSEL FUNCTION FORM
double Lfunction(double ux,double uy,double vx,double vy,double Lambda){
    
    if(ux==vx && uy==vy){
        return 0.0;
    }
    else{
        return EuclideanNorm(ux,uy,vx,vy)*gsl_sf_bessel_K1(Lambda*EuclideanNorm(ux,uy,vx,vy))/(4.0*Lambda*PI) - 1.0/(4.0*std::pow(Lambda,2)*PI);
    }
    
}
// DIS AAMQS FORM
//L(r) = (g^4μ^2*r^2)/4/Log[1/(r*Lqcd) + Exp[1]]
//Λqcd ≡ (m/2)exp(γE−1/2)
double ec=1;
double Lfunction2(double ux,double uy,double vx,double vy,double Lambda){
    
    if(ux==vx && uy==vy){
        return 0.0;
    }
    else{
        return -std::pow(EuclideanNorm(ux,uy,vx,vy),2)/(16.0*PI)*std::log(1.0/(EuclideanNorm(ux,uy,vx,vy)*Lambda)+2.71828182845904509*ec);
    }
    
}

double SingleDipole(double ux,double uy,double vx,double vy,double g4muSQR,double Lambda){
    return exp(Cf*g4muSQR*Lfunction2(ux,uy,vx,vy,Lambda));
}

#include "../MatrixExp/matrix_exponential.cpp"

// CALCULATES MEval(x1,y1,x2,y2,x3,y3,x4,y4)=<D(x1,y1)D(x2,y2)D(x3,y3)D(x4,y4)>
double MatrixEvaluation(double x1, double y1,double x2, double y2,double x3, double y3,double x4, double y4,double g4muSQR,double Lambda){
    
    
       // RESET FINAL RESULT //
    double FinalResult=0.0;
    
    FinalResult=SingleDipole(x1,y1,x2,y2,g4muSQR,Lambda)*SingleDipole(x3,y3,x4,y4,g4muSQR,Lambda)+1.0/(Nc*Nc-1.0)*(SingleDipole(x1,y1,x3,y3,g4muSQR,Lambda)*SingleDipole(x2,y2,x4,y4,g4muSQR,Lambda)+SingleDipole(x1,y1,x4,y4,g4muSQR,Lambda)*SingleDipole(x2,y2,x3,y3,g4muSQR,Lambda));
    
        // RETURN FINAL RESULT //
    return FinalResult;
    
}
