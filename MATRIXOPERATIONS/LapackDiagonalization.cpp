// CONVERSION FROM C SOURCE CODE
// NOTE FOR NERSC NEEDS TO BE cheev_ INSTEAD OF cheev
extern "C" {
    extern void dgeev(const char* jobvl, const char* jobvr, int* n, double* a, int* lda, double* wr, double* wi, double* vl, int* ldvl, double* vr, int* ldvr, double* work, int* lwork, int* info );
}

////////////////////////////////////////
//                                     //
// NOTE: LAPACK AND THIS CODE DEFINES MATRICES AS    //
//                                     //
//          A_0 A_3 A_6                //
//          A_1 A_4 A_7                //
//          A_2 A_5 A_8                //
//                                     //
//              OR                     //
//                                     //
//          A_00 A_01 A_02             //
//          A_10 A_11 A_12             //
//          A_20 A_21 A_22             //
//                                     //
//    WHEREAS GSL DEFINES IT AS        //
//                                     //
//          A_0 A_1 A_1                //
//          A_3 A_4 A_5                //
//          A_6 A_7 A_8                //
//                                     //
//              OR                     //
//                                     //
//          A_00 A_01 A_02             //
//          A_10 A_11 A_12             //
//          A_20 A_21 A_22             //
//                                     //
//                                     //
/////////////////////////////////////////



class Eigensystem {
    
private:
    
    // LINEAR DIMENSION OF THE MATRIX //
    int MatrixSize;
    
private:
    
    // MATRIX //
    double *Matrix;
    
    double *MatrixUnTransposed;
    
    
    // EIGENVECTOR AND EIGENVALUES //
    double *EigenValues;
    double *EigenVectors;
    
public:
    
    // ACCESS EIGENMODES //
    void GetEigenMode(int s,double &eVal,double *eVec){
        
        eVal=EigenValues[s];
        
        for(int i=0;i<MatrixSize;i++){
            eVec[i]=EigenVectors[i+s*MatrixSize];
        }
        
    }
    
    // ACCESS EIGENMODES //
    void SetEigenMode(int s,double eVal,double *eVec){
        
        EigenValues[s]=eVal;
        
        for(int i=0;i<MatrixSize;i++){
            EigenVectors[i+s*MatrixSize]=eVec[i];
        }
        
    }
    
public:
    
    // DIAGONALIZATION USING LAPACK LIBRARY //
    void Diagonalize(){
        
        // COPY MATRIX //
        double *CopyMatrix=new double[MatrixSize*MatrixSize];
        std::memcpy(CopyMatrix,this->Matrix,MatrixSize*MatrixSize*sizeof(double));
        
        double *eValsReal=new double[MatrixSize];
        double *eValsImag=new double[MatrixSize];

        double *eVecsRight=new double[MatrixSize*MatrixSize];
        double *eVecsLeft=new double[MatrixSize*MatrixSize];


        // LOCAL BUFFERS //
        int info;
        double wkopt;
        // lwork dimension should be at least max(4*n) //
        int lwork;

        // QUERY AND ALLOCATE WORKSPACE //
        lwork = -1;
        dgeev("N","V",&MatrixSize,CopyMatrix,&MatrixSize,eValsReal,eValsImag,eVecsLeft,&MatrixSize,eVecsRight,&MatrixSize,&wkopt,&lwork,&info);
        lwork = int(wkopt);

        // DEFINE WORKSPACE
        double* work=new double[lwork*sizeof(double)];
        // SOLVE EIGENPROBLEM //
        dgeev("N","V",&MatrixSize,CopyMatrix,&MatrixSize,eValsReal,eValsImag,eVecsLeft,&MatrixSize,eVecsRight,&MatrixSize,work,&lwork,&info);

        // CHECK CONVERGENCE //
        if( info > 0 ) {
            std::cerr << "The algorithm failed to compute eigenvalues." << std::endl;
            exit( 1 );
        }

        // SET EIGENSYSTEM //
        // IN REVERSE ORDER AS LAPACK GIVES LOWEST TO HIGHEST //
        /*for(int s=0;s<MatrixSize;s++){

            
            // SET EIGENVALUE //
            this->EigenValues[s] = eVals[s];
            
            std::cout << this->EigenValues[s] << std::endl;
            
            // SET EIGENVECTOR //
            for(int j=0;j<MatrixSize;j++){
                
                this->EigenVectors[j+s*MatrixSize]=CopyMatrix[j+s*MatrixSize];
                
                std::cout << this->EigenVectors[j+s*MatrixSize] << " ";
                
            }
            
            std::cout << std::endl;
            
            
        }*/
        
        // IN REVERSE ORDER AS LAPACK GIVES LOWEST TO HIGHEST //
        for(int s=0;s<MatrixSize;s++){
            
            
            // SET EIGENVALUE //
            this->EigenValues[((MatrixSize-1)-s)] = eValsReal[s];
                        
            // SET EIGENVECTOR //
            for(int j=0;j<MatrixSize;j++){
                
                this->EigenVectors[j+((MatrixSize-1)-s)*MatrixSize]=eVecsRight[j+s*MatrixSize];
                
            }
            
            //std::cout << std::endl;
            
            
        }
        
        
        // FREE WORKSPACE
        delete[] work;
        
        delete[] CopyMatrix;
        
        // FREE TEMP EIGENSYSTEM
        delete[] eValsReal;
        delete[] eValsImag;
        delete[] eVecsRight;
        delete[] eVecsLeft;

        
        
    }
    
public:
    
    // CHECK THAT EIGENFUNCTIONS ARE CORRECT //
    void Check(){
        
        int CHECKVALUE=1;
        
        for(int s=0;s<MatrixSize;s++){
            
            for(int i=0;i<MatrixSize;i++){
                
                double qVal=0.0;
                
                for(int j=0;j<MatrixSize;j++){
                    qVal+=Matrix[i+MatrixSize*j]*EigenVectors[j+s*MatrixSize];
                    
                }
                // WARNING -- CHANGED PRECISION -- LOWER THAN WE'D LIKE //
                if(std::abs(qVal-EigenValues[s]*EigenVectors[i+s*MatrixSize])>std::pow(10.0,-8)){
                    CHECKVALUE=0;
                }
                
            }
            
        }
        
        if(CHECKVALUE==1){
            //if(MPIBasic::ID==0){
                std::cerr << "#DIAGONALIZATION SUCCESSFUL" << std::endl;
            //}
        }
        else{
            //if(MPIBasic::ID==0){
                std::cerr << "#DIAGONALIZATION FAILED" << std::endl;
            //}
            exit(0);
        }
        
    }
    
public:
    
    void Output(std::string fname){
        
        std::ofstream OutStream;
        OutStream.open(fname.c_str());
        
        for(int s=0;s<MatrixSize;s++){
            
            OutStream << "#EVal= " << EigenValues[s] << std::endl;
            for(int i=0;i<MatrixSize;i++){
                OutStream << EigenVectors[i+s*MatrixSize] << " ";
            }
            OutStream << std::endl;
            
        }
        
        OutStream.close();
        
    }
    
    
public:
    
    // CONSTRUCTOR //
    Eigensystem(double *InputMatrix,int InputSize){
        
        // SETUP //
        this->MatrixSize=InputSize;
        
        EigenVectors=new double[MatrixSize*MatrixSize];
        EigenValues=new double[MatrixSize];
        
        // SET TEMP MATRIX TO BE TRANSPOSED //
        this->MatrixUnTransposed=new double[MatrixSize*MatrixSize];
        
        // COPY
        std::memcpy(this->MatrixUnTransposed,InputMatrix,MatrixSize*MatrixSize*sizeof(double));
        
        // SET MATRIX //
        this->Matrix=new double[MatrixSize*MatrixSize];
        std::memcpy(this->Matrix,InputMatrix,MatrixSize*MatrixSize*sizeof(double));

        
        // TRANSFORM TO ROW DOMINANT FORMAT LIKE GSL //
        for(int i=0;i<InputSize;i++){
            for(int j=0;j<InputSize;j++){
                
                this->Matrix[i+InputSize*j]=this->MatrixUnTransposed[j+InputSize*i];
                
            }
        }
        
        delete[] this->MatrixUnTransposed;
        
        
    }
    
    // DESTRUCTOR //
    ~Eigensystem(){
        
        delete[] EigenVectors;
        delete[] EigenValues;
        
        delete Matrix;
    }
    
};
