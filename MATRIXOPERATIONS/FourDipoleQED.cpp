// DIS AAMQS FORM
//Lloc(r) = (g^4μ^2*r^2)/4/Log[1/(r*Lqcd) + Exp[1]]
//Λqcd ≡ (m/2)exp(γE−1/2)
double ec=1;
double Lloc(double ux,double uy,double vx,double vy,double Lambda){
    
    if(ux==vx && uy==vy){
        return 0.0;
    }
    else{
        return -std::pow(EuclideanNorm(ux,uy,vx,vy),2)/(16.0*PI)*std::log(1.0/(EuclideanNorm(ux,uy,vx,vy)*Lambda)+2.71828182845904509*ec);
    }
    
}

// 00 01
// 10 11
//
// 20 21
// 30 31
//
// 40 41
// 50 51
//
// 60 61
// 70 71
double MatrixEvaluation(double **x,double g4muSQR,double Lambda){
    
//    double t0 = Lloc(x[0][0],x[0][1],x[1][0],x[1][1],Lambda) + Lloc(x[2][0],x[2][1],x[3][0],x[3][1],Lambda) + Lloc(x[4][0],x[4][1],x[5][0],x[5][1],Lambda) + Lloc(x[6][0],x[6][1],x[7][0],x[7][1],Lambda);
//    
//    double t1 = Lloc(x[0][0],x[0][1],x[3][0],x[3][1],Lambda) + Lloc(x[0][0],x[0][1],x[5][0],x[5][1],Lambda) + Lloc(x[0][0],x[0][1],x[7][0],x[7][1],Lambda) + Lloc(x[1][0],x[1][1],x[2][0],x[2][1],Lambda) + Lloc(x[1][0],x[1][1],x[4][0],x[4][1],Lambda) + Lloc(x[1][0],x[1][1],x[6][0],x[6][1],Lambda) + Lloc(x[2][0],x[2][1],x[5][0],x[5][1],Lambda) + Lloc(x[2][0],x[2][1],x[7][0],x[7][1],Lambda) + Lloc(x[3][0],x[3][1],x[4][0],x[4][1],Lambda) + Lloc(x[3][0],x[3][1],x[6][0],x[6][1],Lambda) + Lloc(x[4][0],x[4][1],x[7][0],x[7][1],Lambda) + Lloc(x[5][0],x[5][1],x[6][0],x[6][1],Lambda);
//    
//    double t2 = Lloc(x[0][0],x[0][1],x[2][0],x[2][1],Lambda) + Lloc(x[0][0],x[0][1],x[4][0],x[4][1],Lambda) + Lloc(x[0][0],x[0][1],x[6][0],x[6][1],Lambda) + Lloc(x[1][0],x[1][1],x[3][0],x[3][1],Lambda) + Lloc(x[1][0],x[1][1],x[5][0],x[5][1],Lambda) + Lloc(x[1][0],x[1][1],x[7][0],x[7][1],Lambda) + Lloc(x[2][0],x[2][1],x[4][0],x[4][1],Lambda) + Lloc(x[2][0],x[2][1],x[6][0],x[6][1],Lambda) + Lloc(x[3][0],x[3][1],x[5][0],x[5][1],Lambda) + Lloc(x[3][0],x[3][1],x[7][0],x[7][1],Lambda) + Lloc(x[4][0],x[4][1],x[6][0],x[6][1],Lambda) + Lloc(x[5][0],x[5][1],x[7][0],x[7][1],Lambda);
    
//    double tot=t0+t1-t2;

   double tot=Lloc(x[0][0],x[0][1],x[1][0],x[1][1],Lambda)-Lloc(x[0][0],x[0][1],x[2][0],x[2][1],Lambda)+Lloc(x[1][0],x[1][1],x[2][0],x[2][1],Lambda)+Lloc(x[0][0],x[0][1],x[3][0],x[3][1],Lambda)-Lloc(x[1][0],x[1][1],x[3][0],x[3][1],Lambda)+Lloc(x[2][0],x[2][1],x[3][0],x[3][1],Lambda)-Lloc(x[0][0],x[0][1],x[4][0],x[4][1],Lambda)+Lloc(x[1][0],x[1][1],x[4][0],x[4][1],Lambda)-Lloc(x[2][0],x[2][1],x[4][0],x[4][1],Lambda)+Lloc(x[3][0],x[3][1],x[4][0],x[4][1],Lambda)+Lloc(x[0][0],x[0][1],x[5][0],x[5][1],Lambda)-Lloc(x[1][0],x[1][1],x[5][0],x[5][1],Lambda)+Lloc(x[2][0],x[2][1],x[5][0],x[5][1],Lambda)-Lloc(x[3][0],x[3][1],x[5][0],x[5][1],Lambda)+Lloc(x[4][0],x[4][1],x[5][0],x[5][1],Lambda)-Lloc(x[0][0],x[0][1],x[6][0],x[6][1],Lambda)+Lloc(x[1][0],x[1][1],x[6][0],x[6][1],Lambda)-Lloc(x[2][0],x[2][1],x[6][0],x[6][1],Lambda)+Lloc(x[3][0],x[3][1],x[6][0],x[6][1],Lambda)-Lloc(x[4][0],x[4][1],x[6][0],x[6][1],Lambda)+Lloc(x[5][0],x[5][1],x[6][0],x[6][1],Lambda)+Lloc(x[0][0],x[0][1],x[7][0],x[7][1],Lambda)-Lloc(x[1][0],x[1][1],x[7][0],x[7][1],Lambda)+Lloc(x[2][0],x[2][1],x[7][0],x[7][1],Lambda)-Lloc(x[3][0],x[3][1],x[7][0],x[7][1],Lambda)+Lloc(x[4][0],x[4][1],x[7][0],x[7][1],Lambda)-Lloc(x[5][0],x[5][1],x[7][0],x[7][1],Lambda)+Lloc(x[6][0],x[6][1],x[7][0],x[7][1],Lambda);
    
    return exp(g4muSQR*tot);
}
