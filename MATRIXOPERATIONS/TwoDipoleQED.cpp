// DIS AAMQS FORM
//L(r) = (g^4μ^2*r^2)/4/Log[1/(r*Lqcd) + Exp[1]]
//Λqcd ≡ (m/2)exp(γE−1/2)
double ec=1;
double Lloc(double ux,double uy,double vx,double vy,double Lambda){
    
    if(ux==vx && uy==vy){
        return 0.0;
    }
    else{
        return -std::pow(EuclideanNorm(ux,uy,vx,vy),2)/(16.0*PI)*std::log(1.0/(EuclideanNorm(ux,uy,vx,vy)*Lambda)+2.71828182845904509*ec);
    }
    
}

double MatrixEvaluation(double x1, double y1,double x2, double y2,double x3, double y3,double x4, double y4,double g4muSQR,double Lambda){
    
    double tot=-Lloc(x1,y1,x3,y3,Lambda)-Lloc(x2,y2,x4,y4,Lambda)+Lloc(x1,y1,x2,y2,Lambda)+Lloc(x1,y1,x4,y4,Lambda)+Lloc(x2,y2,x3,y3,Lambda)+Lloc(x3,y3,x4,y4,Lambda);
        
    return exp(g4muSQR*tot);

}
