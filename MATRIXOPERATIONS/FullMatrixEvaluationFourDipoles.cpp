// CALCULATES MEval(x1,y1,x2,y2,x3,y3,x4,y4,x5,y5,x6,y6,x7,y7,x8,y8)=<D(x1,y1,x2,y2)D(x3,y3,x4,y4)D(x5,y5,x6,y6)D(x7,y7,x8,y8)>

// DEFINE NUMERICAL GLUON PROPOGATOR FUNCTION L(u,v) //
double Lfunction(double ux,double uy,double vx,double vy,double Lambda){
    
    if(ux==vx && uy==vy){
        return 0.0;
    }
    else{
        return EuclideanNorm(ux,uy,vx,vy)*gsl_sf_bessel_K1(Lambda*EuclideanNorm(ux,uy,vx,vy))/(4.0*Lambda*PI) - 1.0/(4.0*std::pow(Lambda,2)*PI);
    }
    
}
// DIS AAMQS FORM
//L(r) = (g^4μ^2*r^2)/4/Log[1/(r*Lqcd) + Exp[1]]
//Λqcd ≡ (m/2)exp(γE−1/2)
double ec=1;
double Lfunction2(double ux,double uy,double vx,double vy,double Lambda){
    
    if(ux==vx && uy==vy){
        return 0.0;
    }
    else{
        return -std::pow(EuclideanNorm(ux,uy,vx,vy),2)/(16.0*PI)*std::log(1.0/(EuclideanNorm(ux,uy,vx,vy)*Lambda)+2.71828182845904509*ec);
    }
    
}

// MATLAB-LIKE MATRIX EXPONENTIAL //
#include "../MatrixExp/matrix_exponential.cpp"
//#include "../MatrixExp/r8lib.hpp"

// EVALUATE MATRIX //
double MatrixEvaluation(double **x,double g4muSQR,double Lambda){
    
    //    std::cerr << "## IN Matrix " << std::endl;
    //
    //    for(int j=0;j<8;j++){
    //        for(int k=0;k<2;k++){
    //
    //            std::cerr << x[j][k] << " ";
    //
    //        }
    //    }
    //
    //    std::cerr << std::endl;
    
    // GLUON PROPOAGATORS //
    double L[nWilsonLines][nWilsonLines]={
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0}
    };
    
    // DEFINE N GLUON PROPAGATOR MATRIX //
    double *M=new double[MatrixSize*MatrixSize];
    
    // DEFINE MATRIX BUFFERS //
    double *MatrixExp;
    
    // VECTORS FOR MATRIX PRODUCTS
    double NcVector[24]={1.0,1.0/Nc,1.0/Nc,1.0/Nc,1.0/Nc,1.0/Nc,1.0/Nc,1.0/(Nc*Nc),1.0/(Nc*Nc),1.0/(Nc*Nc),1.0/(Nc*Nc),1.0/(Nc*Nc),1.0/(Nc*Nc),1.0/(Nc*Nc),1.0/(Nc*Nc),1.0/(Nc*Nc),1.0/(Nc*Nc),1.0/(Nc*Nc),1.0/(Nc*Nc*Nc),1.0/(Nc*Nc*Nc),1.0/(Nc*Nc*Nc),1.0/(Nc*Nc*Nc),1.0/(Nc*Nc*Nc),1.0/(Nc*Nc*Nc)};
    //double InitialConditionVector[24]={1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
    
    
    // DEFINE GLUON PROPOGATOR TERMS//
    for(int j=0;j<nWilsonLines;j++){
        for(int i=0;i<=j;i++){
            
            L[i][j]=g4muSQR*Lfunction2(x[i][0],x[i][1],x[j][0],x[j][1],Lambda);
        }
        
    }
    // OUTPUT COORDINATES //
    //std::cout << "Cx1={" << x[0][0] << "," << x[0][1] << "};" << std::endl;
    //std::cout << "Cx2={" << x[1][0] << "," << x[1][1] << "};" << std::endl;
    //std::cout << "Cx3={" << x[2][0] << "," << x[2][1] << "};" << std::endl;
    //std::cout << "Cx4={" << x[3][0] << "," << x[3][1] << "};" << std::endl;
    //std::cout << "Cx5={" << x[4][0] << "," << x[4][1] << "};" << std::endl;
    //std::cout << "Cx6={" << x[5][0] << "," << x[5][1] << "};" << std::endl;
    //std::cout << "Cx7={" << x[6][0] << "," << x[6][1] << "};" << std::endl;
    //std::cout << "Cx8={" << x[7][0] << "," << x[7][1] << "};" << std::endl;
    
    // RESET MATRIX //
    for(int i=0;i<MatrixSize*MatrixSize;i++){
        M[i]=0.0;
        //MatrixExp[i]=0.0;
    }
    
    // DEFINE N GLUON PROPAGATOR MATRIX //
    M[0]=L[0][1]*(-1./(2.*Nc) + Nc/2.) + L[2][3]*(-1./(2.*Nc) + Nc/2.) + L[4][5]*(-1./(2.*Nc) + Nc/2.) + L[6][7]*(-1./(2.*Nc) + Nc/2.) + L[0][2]/(2.*Nc) - L[0][3]/(2.*Nc) + L[0][4]/(2.*Nc) - L[0][5]/(2.*Nc) + L[0][6]/(2.*Nc) - L[0][7]/(2.*Nc) - L[1][2]/(2.*Nc) + L[1][3]/(2.*Nc) - L[1][4]/(2.*Nc) + L[1][5]/(2.*Nc) - L[1][6]/(2.*Nc) + L[1][7]/(2.*Nc) + L[2][4]/(2.*Nc) - L[2][5]/(2.*Nc) + L[2][6]/(2.*Nc) - L[2][7]/(2.*Nc) - L[3][4]/(2.*Nc) + L[3][5]/(2.*Nc) - L[3][6]/(2.*Nc) + L[3][7]/(2.*Nc) + L[4][6]/(2.*Nc) - L[4][7]/(2.*Nc) - L[5][6]/(2.*Nc) + L[5][7]/(2.*Nc);
    M[1]=L[4][5]/2. - L[4][6]/2. - L[5][7]/2. + L[6][7]/2.;
    M[2]=L[2][3]/2. - L[2][4]/2. - L[3][5]/2. + L[4][5]/2.;
    M[3]=L[2][3]/2. - L[2][6]/2. - L[3][7]/2. + L[6][7]/2.;
    M[4]=L[0][1]/2. - L[0][2]/2. - L[1][3]/2. + L[2][3]/2.;
    M[5]=L[0][1]/2. - L[0][4]/2. - L[1][5]/2. + L[4][5]/2.;
    M[6]=L[0][1]/2. - L[0][6]/2. - L[1][7]/2. + L[6][7]/2.;
    M[24]=-L[4][6]/2. + L[4][7]/2. + L[5][6]/2. - L[5][7]/2.;
    M[25]=L[0][1]*(-1./(2.*Nc) + Nc/2.) + L[2][3]*(-1./(2.*Nc) + Nc/2.) + L[4][7]*(-1./(2.*Nc) + Nc/2.) + L[5][6]*(-1./(2.*Nc) + Nc/2.) + L[0][2]/(2.*Nc) - L[0][3]/(2.*Nc) + L[0][4]/(2.*Nc) - L[0][5]/(2.*Nc) + L[0][6]/(2.*Nc) - L[0][7]/(2.*Nc) - L[1][2]/(2.*Nc) + L[1][3]/(2.*Nc) - L[1][4]/(2.*Nc) + L[1][5]/(2.*Nc) - L[1][6]/(2.*Nc) + L[1][7]/(2.*Nc) + L[2][4]/(2.*Nc) - L[2][5]/(2.*Nc) + L[2][6]/(2.*Nc) - L[2][7]/(2.*Nc) - L[3][4]/(2.*Nc) + L[3][5]/(2.*Nc) - L[3][6]/(2.*Nc) + L[3][7]/(2.*Nc) - L[4][5]/(2.*Nc) + L[4][6]/(2.*Nc) + L[5][7]/(2.*Nc) - L[6][7]/(2.*Nc);
    M[31]=L[0][1]/2. - L[0][2]/2. - L[1][3]/2. + L[2][3]/2.;
    M[34]=L[2][3]/2. - L[2][6]/2. - L[3][5]/2. + L[5][6]/2.;
    M[35]=L[2][3]/2. - L[2][4]/2. - L[3][7]/2. + L[4][7]/2.;
    M[39]=L[0][1]/2. - L[0][6]/2. - L[1][5]/2. + L[5][6]/2.;
    M[41]=L[0][1]/2. - L[0][4]/2. - L[1][7]/2. + L[4][7]/2.;
    M[48]=-L[2][4]/2. + L[2][5]/2. + L[3][4]/2. - L[3][5]/2.;
    M[50]=L[0][1]*(-1./(2.*Nc) + Nc/2.) + L[2][5]*(-1./(2.*Nc) + Nc/2.) + L[3][4]*(-1./(2.*Nc) + Nc/2.) + L[6][7]*(-1./(2.*Nc) + Nc/2.) + L[0][2]/(2.*Nc) - L[0][3]/(2.*Nc) + L[0][4]/(2.*Nc) - L[0][5]/(2.*Nc) + L[0][6]/(2.*Nc) - L[0][7]/(2.*Nc) - L[1][2]/(2.*Nc) + L[1][3]/(2.*Nc) - L[1][4]/(2.*Nc) + L[1][5]/(2.*Nc) - L[1][6]/(2.*Nc) + L[1][7]/(2.*Nc) - L[2][3]/(2.*Nc) + L[2][4]/(2.*Nc) + L[2][6]/(2.*Nc) - L[2][7]/(2.*Nc) + L[3][5]/(2.*Nc) - L[3][6]/(2.*Nc) + L[3][7]/(2.*Nc) - L[4][5]/(2.*Nc) + L[4][6]/(2.*Nc) - L[4][7]/(2.*Nc) - L[5][6]/(2.*Nc) + L[5][7]/(2.*Nc);
    M[57]=L[0][1]/2. - L[0][6]/2. - L[1][7]/2. + L[6][7]/2.;
    M[58]=L[3][4]/2. - L[3][7]/2. - L[4][6]/2. + L[6][7]/2.;
    M[59]=L[2][5]/2. - L[2][6]/2. - L[5][7]/2. + L[6][7]/2.;
    M[60]=L[0][1]/2. - L[0][4]/2. - L[1][3]/2. + L[3][4]/2.;
    M[62]=L[0][1]/2. - L[0][2]/2. - L[1][5]/2. + L[2][5]/2.;
    M[72]=-L[2][6]/2. + L[2][7]/2. + L[3][6]/2. - L[3][7]/2.;
    M[75]=L[0][1]*(-1./(2.*Nc) + Nc/2.) + L[2][7]*(-1./(2.*Nc) + Nc/2.) + L[3][6]*(-1./(2.*Nc) + Nc/2.) + L[4][5]*(-1./(2.*Nc) + Nc/2.) + L[0][2]/(2.*Nc) - L[0][3]/(2.*Nc) + L[0][4]/(2.*Nc) - L[0][5]/(2.*Nc) + L[0][6]/(2.*Nc) - L[0][7]/(2.*Nc) - L[1][2]/(2.*Nc) + L[1][3]/(2.*Nc) - L[1][4]/(2.*Nc) + L[1][5]/(2.*Nc) - L[1][6]/(2.*Nc) + L[1][7]/(2.*Nc) - L[2][3]/(2.*Nc) + L[2][4]/(2.*Nc) - L[2][5]/(2.*Nc) + L[2][6]/(2.*Nc) - L[3][4]/(2.*Nc) + L[3][5]/(2.*Nc) + L[3][7]/(2.*Nc) + L[4][6]/(2.*Nc) - L[4][7]/(2.*Nc) - L[5][6]/(2.*Nc) + L[5][7]/(2.*Nc) - L[6][7]/(2.*Nc);
    M[80]=L[0][1]/2. - L[0][4]/2. - L[1][5]/2. + L[4][5]/2.;
    M[82]=-L[2][4]/2. + L[2][7]/2. + L[4][5]/2. - L[5][7]/2.;
    M[83]=-L[3][5]/2. + L[3][6]/2. + L[4][5]/2. - L[4][6]/2.;
    M[85]=L[0][1]/2. - L[0][6]/2. - L[1][3]/2. + L[3][6]/2.;
    M[88]=L[0][1]/2. - L[0][2]/2. - L[1][7]/2. + L[2][7]/2.;
    M[96]=-L[0][2]/2. + L[0][3]/2. + L[1][2]/2. - L[1][3]/2.;
    M[100]=L[0][3]*(-1./(2.*Nc) + Nc/2.) + L[1][2]*(-1./(2.*Nc) + Nc/2.) + L[4][5]*(-1./(2.*Nc) + Nc/2.) + L[6][7]*(-1./(2.*Nc) + Nc/2.) - L[0][1]/(2.*Nc) + L[0][2]/(2.*Nc) + L[0][4]/(2.*Nc) - L[0][5]/(2.*Nc) + L[0][6]/(2.*Nc) - L[0][7]/(2.*Nc) + L[1][3]/(2.*Nc) - L[1][4]/(2.*Nc) + L[1][5]/(2.*Nc) - L[1][6]/(2.*Nc) + L[1][7]/(2.*Nc) - L[2][3]/(2.*Nc) + L[2][4]/(2.*Nc) - L[2][5]/(2.*Nc) + L[2][6]/(2.*Nc) - L[2][7]/(2.*Nc) - L[3][4]/(2.*Nc) + L[3][5]/(2.*Nc) - L[3][6]/(2.*Nc) + L[3][7]/(2.*Nc) + L[4][6]/(2.*Nc) - L[4][7]/(2.*Nc) - L[5][6]/(2.*Nc) + L[5][7]/(2.*Nc);
    M[103]=L[4][5]/2. - L[4][6]/2. - L[5][7]/2. + L[6][7]/2.;
    M[108]=L[1][2]/2. - L[1][5]/2. - L[2][4]/2. + L[4][5]/2.;
    M[109]=L[1][2]/2. - L[1][7]/2. - L[2][6]/2. + L[6][7]/2.;
    M[110]=L[0][3]/2. - L[0][4]/2. - L[3][5]/2. + L[4][5]/2.;
    M[112]=L[0][3]/2. - L[0][6]/2. - L[3][7]/2. + L[6][7]/2.;
    M[120]=-L[0][4]/2. + L[0][5]/2. + L[1][4]/2. - L[1][5]/2.;
    M[125]=L[0][5]*(-1./(2.*Nc) + Nc/2.) + L[1][4]*(-1./(2.*Nc) + Nc/2.) + L[2][3]*(-1./(2.*Nc) + Nc/2.) + L[6][7]*(-1./(2.*Nc) + Nc/2.) - L[0][1]/(2.*Nc) + L[0][2]/(2.*Nc) - L[0][3]/(2.*Nc) + L[0][4]/(2.*Nc) + L[0][6]/(2.*Nc) - L[0][7]/(2.*Nc) - L[1][2]/(2.*Nc) + L[1][3]/(2.*Nc) + L[1][5]/(2.*Nc) - L[1][6]/(2.*Nc) + L[1][7]/(2.*Nc) + L[2][4]/(2.*Nc) - L[2][5]/(2.*Nc) + L[2][6]/(2.*Nc) - L[2][7]/(2.*Nc) - L[3][4]/(2.*Nc) + L[3][5]/(2.*Nc) - L[3][6]/(2.*Nc) + L[3][7]/(2.*Nc) - L[4][5]/(2.*Nc) + L[4][6]/(2.*Nc) - L[4][7]/(2.*Nc) - L[5][6]/(2.*Nc) + L[5][7]/(2.*Nc);
    M[128]=L[2][3]/2. - L[2][6]/2. - L[3][7]/2. + L[6][7]/2.;
    M[132]=-L[0][2]/2. + L[0][5]/2. + L[2][3]/2. - L[3][5]/2.;
    M[134]=-L[1][3]/2. + L[1][4]/2. + L[2][3]/2. - L[2][4]/2.;
    M[135]=L[1][4]/2. - L[1][7]/2. - L[4][6]/2. + L[6][7]/2.;
    M[137]=L[0][5]/2. - L[0][6]/2. - L[5][7]/2. + L[6][7]/2.;
    M[144]=-L[0][6]/2. + L[0][7]/2. + L[1][6]/2. - L[1][7]/2.;
    M[150]=L[0][7]*(-1./(2.*Nc) + Nc/2.) + L[1][6]*(-1./(2.*Nc) + Nc/2.) + L[2][3]*(-1./(2.*Nc) + Nc/2.) + L[4][5]*(-1./(2.*Nc) + Nc/2.) - L[0][1]/(2.*Nc) + L[0][2]/(2.*Nc) - L[0][3]/(2.*Nc) + L[0][4]/(2.*Nc) - L[0][5]/(2.*Nc) + L[0][6]/(2.*Nc) - L[1][2]/(2.*Nc) + L[1][3]/(2.*Nc) - L[1][4]/(2.*Nc) + L[1][5]/(2.*Nc) + L[1][7]/(2.*Nc) + L[2][4]/(2.*Nc) - L[2][5]/(2.*Nc) + L[2][6]/(2.*Nc) - L[2][7]/(2.*Nc) - L[3][4]/(2.*Nc) + L[3][5]/(2.*Nc) - L[3][6]/(2.*Nc) + L[3][7]/(2.*Nc) + L[4][6]/(2.*Nc) - L[4][7]/(2.*Nc) - L[5][6]/(2.*Nc) + L[5][7]/(2.*Nc) - L[6][7]/(2.*Nc);
    M[153]=L[2][3]/2. - L[2][4]/2. - L[3][5]/2. + L[4][5]/2.;
    M[157]=-L[0][2]/2. + L[0][7]/2. + L[2][3]/2. - L[3][7]/2.;
    M[159]=-L[0][4]/2. + L[0][7]/2. + L[4][5]/2. - L[5][7]/2.;
    M[160]=-L[1][3]/2. + L[1][6]/2. + L[2][3]/2. - L[2][6]/2.;
    M[161]=-L[1][5]/2. + L[1][6]/2. + L[4][5]/2. - L[4][6]/2.;
    M[169]=-L[0][2]/2. + L[0][3]/2. + L[1][2]/2. - L[1][3]/2.;
    M[172]=-L[4][6]/2. + L[4][7]/2. + L[5][6]/2. - L[5][7]/2.;
    M[175]=L[0][3]*(-1./(2.*Nc) + Nc/2.) + L[1][2]*(-1./(2.*Nc) + Nc/2.) + L[4][7]*(-1./(2.*Nc) + Nc/2.) + L[5][6]*(-1./(2.*Nc) + Nc/2.) - L[0][1]/(2.*Nc) + L[0][2]/(2.*Nc) + L[0][4]/(2.*Nc) - L[0][5]/(2.*Nc) + L[0][6]/(2.*Nc) - L[0][7]/(2.*Nc) + L[1][3]/(2.*Nc) - L[1][4]/(2.*Nc) + L[1][5]/(2.*Nc) - L[1][6]/(2.*Nc) + L[1][7]/(2.*Nc) - L[2][3]/(2.*Nc) + L[2][4]/(2.*Nc) - L[2][5]/(2.*Nc) + L[2][6]/(2.*Nc) - L[2][7]/(2.*Nc) - L[3][4]/(2.*Nc) + L[3][5]/(2.*Nc) - L[3][6]/(2.*Nc) + L[3][7]/(2.*Nc) - L[4][5]/(2.*Nc) + L[4][6]/(2.*Nc) + L[5][7]/(2.*Nc) - L[6][7]/(2.*Nc);
    M[186]=L[1][2]/2. - L[1][5]/2. - L[2][6]/2. + L[5][6]/2.;
    M[187]=L[1][2]/2. - L[1][7]/2. - L[2][4]/2. + L[4][7]/2.;
    M[188]=L[0][3]/2. - L[0][6]/2. - L[3][5]/2. + L[5][6]/2.;
    M[190]=L[0][3]/2. - L[0][4]/2. - L[3][7]/2. + L[4][7]/2.;
    M[195]=-L[0][4]/2. + L[0][5]/2. + L[1][4]/2. - L[1][5]/2.;
    M[197]=-L[2][6]/2. + L[2][7]/2. + L[3][6]/2. - L[3][7]/2.;
    
    M[200]=L[0][5]*(-1./(2.*Nc) + Nc/2.) + L[1][4]*(-1./(2.*Nc) + Nc/2.) + L[2][7]*(-1./(2.*Nc) + Nc/2.) + L[3][6]*(-1./(2.*Nc) + Nc/2.) - L[0][1]/(2.*Nc) + L[0][2]/(2.*Nc) - L[0][3]/(2.*Nc) + L[0][4]/(2.*Nc) + L[0][6]/(2.*Nc) - L[0][7]/(2.*Nc) - L[1][2]/(2.*Nc) + L[1][3]/(2.*Nc) + L[1][5]/(2.*Nc) - L[1][6]/(2.*Nc) + L[1][7]/(2.*Nc) - L[2][3]/(2.*Nc) + L[2][4]/(2.*Nc) - L[2][5]/(2.*Nc) + L[2][6]/(2.*Nc) - L[3][4]/(2.*Nc) + L[3][5]/(2.*Nc) + L[3][7]/(2.*Nc) - L[4][5]/(2.*Nc) + L[4][6]/(2.*Nc) - L[4][7]/(2.*Nc) - L[5][6]/(2.*Nc) + L[5][7]/(2.*Nc) - L[6][7]/(2.*Nc);
    M[211]=L[0][5]/2. - L[0][6]/2. - L[3][5]/2. + L[3][6]/2.;
    M[212]=L[1][4]/2. - L[1][7]/2. - L[2][4]/2. + L[2][7]/2.;
    M[213]=-L[1][3]/2. + L[1][4]/2. + L[3][6]/2. - L[4][6]/2.;
    M[215]=-L[0][2]/2. + L[0][5]/2. + L[2][7]/2. - L[5][7]/2.;
    M[218]=-L[0][6]/2. + L[0][7]/2. + L[1][6]/2. - L[1][7]/2.;
    M[222]=-L[2][4]/2. + L[2][5]/2. + L[3][4]/2. - L[3][5]/2.;
    M[225]=L[0][7]*(-1./(2.*Nc) + Nc/2.) + L[1][6]*(-1./(2.*Nc) + Nc/2.) + L[2][5]*(-1./(2.*Nc) + Nc/2.) + L[3][4]*(-1./(2.*Nc) + Nc/2.) - L[0][1]/(2.*Nc) + L[0][2]/(2.*Nc) - L[0][3]/(2.*Nc) + L[0][4]/(2.*Nc) - L[0][5]/(2.*Nc) + L[0][6]/(2.*Nc) - L[1][2]/(2.*Nc) + L[1][3]/(2.*Nc) - L[1][4]/(2.*Nc) + L[1][5]/(2.*Nc) + L[1][7]/(2.*Nc) - L[2][3]/(2.*Nc) + L[2][4]/(2.*Nc) + L[2][6]/(2.*Nc) - L[2][7]/(2.*Nc) + L[3][5]/(2.*Nc) - L[3][6]/(2.*Nc) + L[3][7]/(2.*Nc) - L[4][5]/(2.*Nc) + L[4][6]/(2.*Nc) - L[4][7]/(2.*Nc) - L[5][6]/(2.*Nc) + L[5][7]/(2.*Nc) - L[6][7]/(2.*Nc);
    M[234]=-L[0][4]/2. + L[0][7]/2. + L[3][4]/2. - L[3][7]/2.;
    M[237]=-L[0][2]/2. + L[0][7]/2. + L[2][5]/2. - L[5][7]/2.;
    M[238]=-L[1][5]/2. + L[1][6]/2. + L[2][5]/2. - L[2][6]/2.;
    M[239]=-L[1][3]/2. + L[1][6]/2. + L[3][4]/2. - L[4][6]/2.;
    M[241]=L[2][5]/2. - L[2][6]/2. - L[3][5]/2. + L[3][6]/2.;
    M[242]=L[3][6]/2. - L[3][7]/2. - L[4][6]/2. + L[4][7]/2.;
    M[243]=-L[2][4]/2. + L[2][5]/2. + L[4][7]/2. - L[5][7]/2.;
    M[250]=L[0][1]*(-1./(2.*Nc) + Nc/2.) + L[2][5]*(-1./(2.*Nc) + Nc/2.) + L[3][6]*(-1./(2.*Nc) + Nc/2.) + L[4][7]*(-1./(2.*Nc) + Nc/2.) + L[0][2]/(2.*Nc) - L[0][3]/(2.*Nc) + L[0][4]/(2.*Nc) - L[0][5]/(2.*Nc) + L[0][6]/(2.*Nc) - L[0][7]/(2.*Nc) - L[1][2]/(2.*Nc) + L[1][3]/(2.*Nc) - L[1][4]/(2.*Nc) + L[1][5]/(2.*Nc) - L[1][6]/(2.*Nc) + L[1][7]/(2.*Nc) - L[2][3]/(2.*Nc) + L[2][4]/(2.*Nc) + L[2][6]/(2.*Nc) - L[2][7]/(2.*Nc) - L[3][4]/(2.*Nc) + L[3][5]/(2.*Nc) + L[3][7]/(2.*Nc) - L[4][5]/(2.*Nc) + L[4][6]/(2.*Nc) - L[5][6]/(2.*Nc) + L[5][7]/(2.*Nc) - L[6][7]/(2.*Nc);
    M[258]=L[0][1]/2. - L[0][6]/2. - L[1][3]/2. + L[3][6]/2.;
    M[260]=L[0][1]/2. - L[0][2]/2. - L[1][5]/2. + L[2][5]/2.;
    M[263]=L[0][1]/2. - L[0][4]/2. - L[1][7]/2. + L[4][7]/2.;
    M[265]=-L[2][4]/2. + L[2][7]/2. + L[3][4]/2. - L[3][7]/2.;
    M[266]=-L[2][6]/2. + L[2][7]/2. + L[5][6]/2. - L[5][7]/2.;
    M[267]=L[3][4]/2. - L[3][5]/2. - L[4][6]/2. + L[5][6]/2.;
    M[275]=L[0][1]*(-1./(2.*Nc) + Nc/2.) + L[2][7]*(-1./(2.*Nc) + Nc/2.) + L[3][4]*(-1./(2.*Nc) + Nc/2.) + L[5][6]*(-1./(2.*Nc) + Nc/2.) + L[0][2]/(2.*Nc) - L[0][3]/(2.*Nc) + L[0][4]/(2.*Nc) - L[0][5]/(2.*Nc) + L[0][6]/(2.*Nc) - L[0][7]/(2.*Nc) - L[1][2]/(2.*Nc) + L[1][3]/(2.*Nc) - L[1][4]/(2.*Nc) + L[1][5]/(2.*Nc) - L[1][6]/(2.*Nc) + L[1][7]/(2.*Nc) - L[2][3]/(2.*Nc) + L[2][4]/(2.*Nc) - L[2][5]/(2.*Nc) + L[2][6]/(2.*Nc) + L[3][5]/(2.*Nc) - L[3][6]/(2.*Nc) + L[3][7]/(2.*Nc) - L[4][5]/(2.*Nc) + L[4][6]/(2.*Nc) - L[4][7]/(2.*Nc) + L[5][7]/(2.*Nc) - L[6][7]/(2.*Nc);
    M[283]=L[0][1]/2. - L[0][4]/2. - L[1][3]/2. + L[3][4]/2.;
    M[285]=L[0][1]/2. - L[0][6]/2. - L[1][5]/2. + L[5][6]/2.;
    M[286]=L[0][1]/2. - L[0][2]/2. - L[1][7]/2. + L[2][7]/2.;
    M[290]=L[0][3]/2. - L[0][4]/2. - L[1][3]/2. + L[1][4]/2.;
    M[292]=L[1][4]/2. - L[1][5]/2. - L[2][4]/2. + L[2][5]/2.;
    M[293]=-L[0][2]/2. + L[0][3]/2. + L[2][5]/2. - L[3][5]/2.;
    M[300]=L[0][3]*(-1./(2.*Nc) + Nc/2.) + L[1][4]*(-1./(2.*Nc) + Nc/2.) + L[2][5]*(-1./(2.*Nc) + Nc/2.) + L[6][7]*(-1./(2.*Nc) + Nc/2.) - L[0][1]/(2.*Nc) + L[0][2]/(2.*Nc) + L[0][4]/(2.*Nc) - L[0][5]/(2.*Nc) + L[0][6]/(2.*Nc) - L[0][7]/(2.*Nc) - L[1][2]/(2.*Nc) + L[1][3]/(2.*Nc) + L[1][5]/(2.*Nc) - L[1][6]/(2.*Nc) + L[1][7]/(2.*Nc) - L[2][3]/(2.*Nc) + L[2][4]/(2.*Nc) + L[2][6]/(2.*Nc) - L[2][7]/(2.*Nc) - L[3][4]/(2.*Nc) + L[3][5]/(2.*Nc) - L[3][6]/(2.*Nc) + L[3][7]/(2.*Nc) - L[4][5]/(2.*Nc) + L[4][6]/(2.*Nc) - L[4][7]/(2.*Nc) - L[5][6]/(2.*Nc) + L[5][7]/(2.*Nc);
    M[306]=L[1][4]/2. - L[1][7]/2. - L[4][6]/2. + L[6][7]/2.;
    M[307]=L[2][5]/2. - L[2][6]/2. - L[5][7]/2. + L[6][7]/2.;
    M[311]=L[0][3]/2. - L[0][6]/2. - L[3][7]/2. + L[6][7]/2.;
    M[315]=L[0][3]/2. - L[0][6]/2. - L[1][3]/2. + L[1][6]/2.;
    M[316]=L[1][6]/2. - L[1][7]/2. - L[2][6]/2. + L[2][7]/2.;
    M[318]=-L[0][2]/2. + L[0][3]/2. + L[2][7]/2. - L[3][7]/2.;
    M[325]=L[0][3]*(-1./(2.*Nc) + Nc/2.) + L[1][6]*(-1./(2.*Nc) + Nc/2.) + L[2][7]*(-1./(2.*Nc) + Nc/2.) + L[4][5]*(-1./(2.*Nc) + Nc/2.) - L[0][1]/(2.*Nc) + L[0][2]/(2.*Nc) + L[0][4]/(2.*Nc) - L[0][5]/(2.*Nc) + L[0][6]/(2.*Nc) - L[0][7]/(2.*Nc) - L[1][2]/(2.*Nc) + L[1][3]/(2.*Nc) - L[1][4]/(2.*Nc) + L[1][5]/(2.*Nc) + L[1][7]/(2.*Nc) - L[2][3]/(2.*Nc) + L[2][4]/(2.*Nc) - L[2][5]/(2.*Nc) + L[2][6]/(2.*Nc) - L[3][4]/(2.*Nc) + L[3][5]/(2.*Nc) - L[3][6]/(2.*Nc) + L[3][7]/(2.*Nc) + L[4][6]/(2.*Nc) - L[4][7]/(2.*Nc) - L[5][6]/(2.*Nc) + L[5][7]/(2.*Nc) - L[6][7]/(2.*Nc);
    M[330]=-L[2][4]/2. + L[2][7]/2. + L[4][5]/2. - L[5][7]/2.;
    M[331]=-L[1][5]/2. + L[1][6]/2. + L[4][5]/2. - L[4][6]/2.;
    M[333]=L[0][3]/2. - L[0][4]/2. - L[3][5]/2. + L[4][5]/2.;
    M[338]=-L[0][2]/2. + L[0][5]/2. + L[1][2]/2. - L[1][5]/2.;
    M[340]=-L[0][4]/2. + L[0][5]/2. + L[3][4]/2. - L[3][5]/2.;
    M[341]=L[1][2]/2. - L[1][3]/2. - L[2][4]/2. + L[3][4]/2.;
    M[350]=L[0][5]*(-1./(2.*Nc) + Nc/2.) + L[1][2]*(-1./(2.*Nc) + Nc/2.) + L[3][4]*(-1./(2.*Nc) + Nc/2.) + L[6][7]*(-1./(2.*Nc) + Nc/2.) - L[0][1]/(2.*Nc) + L[0][2]/(2.*Nc) - L[0][3]/(2.*Nc) + L[0][4]/(2.*Nc) + L[0][6]/(2.*Nc) - L[0][7]/(2.*Nc) + L[1][3]/(2.*Nc) - L[1][4]/(2.*Nc) + L[1][5]/(2.*Nc) - L[1][6]/(2.*Nc) + L[1][7]/(2.*Nc) - L[2][3]/(2.*Nc) + L[2][4]/(2.*Nc) - L[2][5]/(2.*Nc) + L[2][6]/(2.*Nc) - L[2][7]/(2.*Nc) + L[3][5]/(2.*Nc) - L[3][6]/(2.*Nc) + L[3][7]/(2.*Nc) - L[4][5]/(2.*Nc) + L[4][6]/(2.*Nc) - L[4][7]/(2.*Nc) - L[5][6]/(2.*Nc) + L[5][7]/(2.*Nc);
    M[356]=L[3][4]/2. - L[3][7]/2. - L[4][6]/2. + L[6][7]/2.;
    M[357]=L[1][2]/2. - L[1][7]/2. - L[2][6]/2. + L[6][7]/2.;
    M[358]=L[0][5]/2. - L[0][6]/2. - L[5][7]/2. + L[6][7]/2.;
    M[361]=L[0][5]/2. - L[0][6]/2. - L[1][5]/2. + L[1][6]/2.;
    M[365]=L[1][6]/2. - L[1][7]/2. - L[4][6]/2. + L[4][7]/2.;
    M[366]=-L[0][4]/2. + L[0][5]/2. + L[4][7]/2. - L[5][7]/2.;
    M[375]=L[0][5]*(-1./(2.*Nc) + Nc/2.) + L[1][6]*(-1./(2.*Nc) + Nc/2.) + L[2][3]*(-1./(2.*Nc) + Nc/2.) + L[4][7]*(-1./(2.*Nc) + Nc/2.) - L[0][1]/(2.*Nc) + L[0][2]/(2.*Nc) - L[0][3]/(2.*Nc) + L[0][4]/(2.*Nc) + L[0][6]/(2.*Nc) - L[0][7]/(2.*Nc) - L[1][2]/(2.*Nc) + L[1][3]/(2.*Nc) - L[1][4]/(2.*Nc) + L[1][5]/(2.*Nc) + L[1][7]/(2.*Nc) + L[2][4]/(2.*Nc) - L[2][5]/(2.*Nc) + L[2][6]/(2.*Nc) - L[2][7]/(2.*Nc) - L[3][4]/(2.*Nc) + L[3][5]/(2.*Nc) - L[3][6]/(2.*Nc) + L[3][7]/(2.*Nc) - L[4][5]/(2.*Nc) + L[4][6]/(2.*Nc) - L[5][6]/(2.*Nc) + L[5][7]/(2.*Nc) - L[6][7]/(2.*Nc);
    M[378]=-L[0][2]/2. + L[0][5]/2. + L[2][3]/2. - L[3][5]/2.;
    M[380]=-L[1][3]/2. + L[1][6]/2. + L[2][3]/2. - L[2][6]/2.;
    M[381]=L[2][3]/2. - L[2][4]/2. - L[3][7]/2. + L[4][7]/2.;
    M[387]=-L[0][2]/2. + L[0][7]/2. + L[1][2]/2. - L[1][7]/2.;
    M[388]=-L[0][6]/2. + L[0][7]/2. + L[3][6]/2. - L[3][7]/2.;
    M[390]=L[1][2]/2. - L[1][3]/2. - L[2][6]/2. + L[3][6]/2.;
    M[400]=L[0][7]*(-1./(2.*Nc) + Nc/2.) + L[1][2]*(-1./(2.*Nc) + Nc/2.) + L[3][6]*(-1./(2.*Nc) + Nc/2.) + L[4][5]*(-1./(2.*Nc) + Nc/2.) - L[0][1]/(2.*Nc) + L[0][2]/(2.*Nc) - L[0][3]/(2.*Nc) + L[0][4]/(2.*Nc) - L[0][5]/(2.*Nc) + L[0][6]/(2.*Nc) + L[1][3]/(2.*Nc) - L[1][4]/(2.*Nc) + L[1][5]/(2.*Nc) - L[1][6]/(2.*Nc) + L[1][7]/(2.*Nc) - L[2][3]/(2.*Nc) + L[2][4]/(2.*Nc) - L[2][5]/(2.*Nc) + L[2][6]/(2.*Nc) - L[2][7]/(2.*Nc) - L[3][4]/(2.*Nc) + L[3][5]/(2.*Nc) + L[3][7]/(2.*Nc) + L[4][6]/(2.*Nc) - L[4][7]/(2.*Nc) - L[5][6]/(2.*Nc) + L[5][7]/(2.*Nc) - L[6][7]/(2.*Nc);
    M[404]=-L[0][4]/2. + L[0][7]/2. + L[4][5]/2. - L[5][7]/2.;
    M[406]=-L[3][5]/2. + L[3][6]/2. + L[4][5]/2. - L[4][6]/2.;
    M[407]=L[1][2]/2. - L[1][5]/2. - L[2][4]/2. + L[4][5]/2.;
    M[409]=-L[0][4]/2. + L[0][7]/2. + L[1][4]/2. - L[1][7]/2.;
    M[413]=-L[0][6]/2. + L[0][7]/2. + L[5][6]/2. - L[5][7]/2.;
    M[414]=L[1][4]/2. - L[1][5]/2. - L[4][6]/2. + L[5][6]/2.;
    M[425]=L[0][7]*(-1./(2.*Nc) + Nc/2.) + L[1][4]*(-1./(2.*Nc) + Nc/2.) + L[2][3]*(-1./(2.*Nc) + Nc/2.) + L[5][6]*(-1./(2.*Nc) + Nc/2.) - L[0][1]/(2.*Nc) + L[0][2]/(2.*Nc) - L[0][3]/(2.*Nc) + L[0][4]/(2.*Nc) - L[0][5]/(2.*Nc) + L[0][6]/(2.*Nc) - L[1][2]/(2.*Nc) + L[1][3]/(2.*Nc) + L[1][5]/(2.*Nc) - L[1][6]/(2.*Nc) + L[1][7]/(2.*Nc) + L[2][4]/(2.*Nc) - L[2][5]/(2.*Nc) + L[2][6]/(2.*Nc) - L[2][7]/(2.*Nc) - L[3][4]/(2.*Nc) + L[3][5]/(2.*Nc) - L[3][6]/(2.*Nc) + L[3][7]/(2.*Nc) - L[4][5]/(2.*Nc) + L[4][6]/(2.*Nc) - L[4][7]/(2.*Nc) + L[5][7]/(2.*Nc) - L[6][7]/(2.*Nc);
    M[427]=-L[0][2]/2. + L[0][7]/2. + L[2][3]/2. - L[3][7]/2.;
    M[430]=-L[1][3]/2. + L[1][4]/2. + L[2][3]/2. - L[2][4]/2.;
    M[431]=L[2][3]/2. - L[2][6]/2. - L[3][5]/2. + L[5][6]/2.;
    M[439]=-L[1][5]/2. + L[1][6]/2. + L[2][5]/2. - L[2][6]/2.;
    M[441]=L[0][3]/2. - L[0][4]/2. - L[3][7]/2. + L[4][7]/2.;
    M[442]=L[0][3]/2. - L[0][6]/2. - L[1][3]/2. + L[1][6]/2.;
    M[444]=L[1][6]/2. - L[1][7]/2. - L[4][6]/2. + L[4][7]/2.;
    M[445]=-L[2][4]/2. + L[2][5]/2. + L[4][7]/2. - L[5][7]/2.;
    M[447]=-L[0][2]/2. + L[0][3]/2. + L[2][5]/2. - L[3][5]/2.;
    M[450]=L[0][3]*(-1./(2.*Nc) + Nc/2.) + L[1][6]*(-1./(2.*Nc) + Nc/2.) + L[2][5]*(-1./(2.*Nc) + Nc/2.) + L[4][7]*(-1./(2.*Nc) + Nc/2.) - L[0][1]/(2.*Nc) + L[0][2]/(2.*Nc) + L[0][4]/(2.*Nc) - L[0][5]/(2.*Nc) + L[0][6]/(2.*Nc) - L[0][7]/(2.*Nc) - L[1][2]/(2.*Nc) + L[1][3]/(2.*Nc) - L[1][4]/(2.*Nc) + L[1][5]/(2.*Nc) + L[1][7]/(2.*Nc) - L[2][3]/(2.*Nc) + L[2][4]/(2.*Nc) + L[2][6]/(2.*Nc) - L[2][7]/(2.*Nc) - L[3][4]/(2.*Nc) + L[3][5]/(2.*Nc) - L[3][6]/(2.*Nc) + L[3][7]/(2.*Nc) - L[4][5]/(2.*Nc) + L[4][6]/(2.*Nc) - L[5][6]/(2.*Nc) + L[5][7]/(2.*Nc) - L[6][7]/(2.*Nc);
    M[463]=L[1][4]/2. - L[1][7]/2. - L[2][4]/2. + L[2][7]/2.;
    M[464]=L[0][3]/2. - L[0][6]/2. - L[3][5]/2. + L[5][6]/2.;
    M[467]=L[0][3]/2. - L[0][4]/2. - L[1][3]/2. + L[1][4]/2.;
    M[468]=-L[2][6]/2. + L[2][7]/2. + L[5][6]/2. - L[5][7]/2.;
    M[469]=L[1][4]/2. - L[1][5]/2. - L[4][6]/2. + L[5][6]/2.;
    M[473]=-L[0][2]/2. + L[0][3]/2. + L[2][7]/2. - L[3][7]/2.;
    M[475]=L[0][3]*(-1./(2.*Nc) + Nc/2.) + L[1][4]*(-1./(2.*Nc) + Nc/2.) + L[2][7]*(-1./(2.*Nc) + Nc/2.) + L[5][6]*(-1./(2.*Nc) + Nc/2.) - L[0][1]/(2.*Nc) + L[0][2]/(2.*Nc) + L[0][4]/(2.*Nc) - L[0][5]/(2.*Nc) + L[0][6]/(2.*Nc) - L[0][7]/(2.*Nc) - L[1][2]/(2.*Nc) + L[1][3]/(2.*Nc) + L[1][5]/(2.*Nc) - L[1][6]/(2.*Nc) + L[1][7]/(2.*Nc) - L[2][3]/(2.*Nc) + L[2][4]/(2.*Nc) - L[2][5]/(2.*Nc) + L[2][6]/(2.*Nc) - L[3][4]/(2.*Nc) + L[3][5]/(2.*Nc) - L[3][6]/(2.*Nc) + L[3][7]/(2.*Nc) - L[4][5]/(2.*Nc) + L[4][6]/(2.*Nc) - L[4][7]/(2.*Nc) + L[5][7]/(2.*Nc) - L[6][7]/(2.*Nc);
    M[487]=L[0][5]/2. - L[0][6]/2. - L[3][5]/2. + L[3][6]/2.;
    M[488]=L[1][2]/2. - L[1][7]/2. - L[2][4]/2. + L[4][7]/2.;
    M[490]=-L[0][2]/2. + L[0][5]/2. + L[1][2]/2. - L[1][5]/2.;
    M[494]=L[3][6]/2. - L[3][7]/2. - L[4][6]/2. + L[4][7]/2.;
    M[495]=L[1][2]/2. - L[1][3]/2. - L[2][6]/2. + L[3][6]/2.;
    M[496]=-L[0][4]/2. + L[0][5]/2. + L[4][7]/2. - L[5][7]/2.;
    M[500]=L[0][5]*(-1./(2.*Nc) + Nc/2.) + L[1][2]*(-1./(2.*Nc) + Nc/2.) + L[3][6]*(-1./(2.*Nc) + Nc/2.) + L[4][7]*(-1./(2.*Nc) + Nc/2.) - L[0][1]/(2.*Nc) + L[0][2]/(2.*Nc) - L[0][3]/(2.*Nc) + L[0][4]/(2.*Nc) + L[0][6]/(2.*Nc) - L[0][7]/(2.*Nc) + L[1][3]/(2.*Nc) - L[1][4]/(2.*Nc) + L[1][5]/(2.*Nc) - L[1][6]/(2.*Nc) + L[1][7]/(2.*Nc) - L[2][3]/(2.*Nc) + L[2][4]/(2.*Nc) - L[2][5]/(2.*Nc) + L[2][6]/(2.*Nc) - L[2][7]/(2.*Nc) - L[3][4]/(2.*Nc) + L[3][5]/(2.*Nc) + L[3][7]/(2.*Nc) - L[4][5]/(2.*Nc) + L[4][6]/(2.*Nc) - L[5][6]/(2.*Nc) + L[5][7]/(2.*Nc) - L[6][7]/(2.*Nc);
    M[512]=-L[1][3]/2. + L[1][6]/2. + L[3][4]/2. - L[4][6]/2.;
    M[513]=-L[0][2]/2. + L[0][5]/2. + L[2][7]/2. - L[5][7]/2.;
    M[515]=L[0][5]/2. - L[0][6]/2. - L[1][5]/2. + L[1][6]/2.;
    M[517]=-L[0][4]/2. + L[0][5]/2. + L[3][4]/2. - L[3][5]/2.;
    M[518]=L[1][6]/2. - L[1][7]/2. - L[2][6]/2. + L[2][7]/2.;
    M[519]=-L[2][4]/2. + L[2][7]/2. + L[3][4]/2. - L[3][7]/2.;
    M[525]=L[0][5]*(-1./(2.*Nc) + Nc/2.) + L[1][6]*(-1./(2.*Nc) + Nc/2.) + L[2][7]*(-1./(2.*Nc) + Nc/2.) + L[3][4]*(-1./(2.*Nc) + Nc/2.) - L[0][1]/(2.*Nc) + L[0][2]/(2.*Nc) - L[0][3]/(2.*Nc) + L[0][4]/(2.*Nc) + L[0][6]/(2.*Nc) - L[0][7]/(2.*Nc) - L[1][2]/(2.*Nc) + L[1][3]/(2.*Nc) - L[1][4]/(2.*Nc) + L[1][5]/(2.*Nc) + L[1][7]/(2.*Nc) - L[2][3]/(2.*Nc) + L[2][4]/(2.*Nc) - L[2][5]/(2.*Nc) + L[2][6]/(2.*Nc) + L[3][5]/(2.*Nc) - L[3][6]/(2.*Nc) + L[3][7]/(2.*Nc) - L[4][5]/(2.*Nc) + L[4][6]/(2.*Nc) - L[4][7]/(2.*Nc) - L[5][6]/(2.*Nc) + L[5][7]/(2.*Nc) - L[6][7]/(2.*Nc);
    M[535]=-L[0][4]/2. + L[0][7]/2. + L[3][4]/2. - L[3][7]/2.;
    M[537]=L[1][2]/2. - L[1][5]/2. - L[2][6]/2. + L[5][6]/2.;
    M[539]=-L[0][2]/2. + L[0][7]/2. + L[1][2]/2. - L[1][7]/2.;
    M[542]=-L[0][6]/2. + L[0][7]/2. + L[5][6]/2. - L[5][7]/2.;
    M[544]=L[3][4]/2. - L[3][5]/2. - L[4][6]/2. + L[5][6]/2.;
    M[545]=L[1][2]/2. - L[1][3]/2. - L[2][4]/2. + L[3][4]/2.;
    M[550]=L[0][7]*(-1./(2.*Nc) + Nc/2.) + L[1][2]*(-1./(2.*Nc) + Nc/2.) + L[3][4]*(-1./(2.*Nc) + Nc/2.) + L[5][6]*(-1./(2.*Nc) + Nc/2.) - L[0][1]/(2.*Nc) + L[0][2]/(2.*Nc) - L[0][3]/(2.*Nc) + L[0][4]/(2.*Nc) - L[0][5]/(2.*Nc) + L[0][6]/(2.*Nc) + L[1][3]/(2.*Nc) - L[1][4]/(2.*Nc) + L[1][5]/(2.*Nc) - L[1][6]/(2.*Nc) + L[1][7]/(2.*Nc) - L[2][3]/(2.*Nc) + L[2][4]/(2.*Nc) - L[2][5]/(2.*Nc) + L[2][6]/(2.*Nc) - L[2][7]/(2.*Nc) + L[3][5]/(2.*Nc) - L[3][6]/(2.*Nc) + L[3][7]/(2.*Nc) - L[4][5]/(2.*Nc) + L[4][6]/(2.*Nc) - L[4][7]/(2.*Nc) + L[5][7]/(2.*Nc) - L[6][7]/(2.*Nc);
    M[560]=-L[0][2]/2. + L[0][7]/2. + L[2][5]/2. - L[5][7]/2.;
    M[561]=-L[1][3]/2. + L[1][4]/2. + L[3][6]/2. - L[4][6]/2.;
    M[562]=-L[0][4]/2. + L[0][7]/2. + L[1][4]/2. - L[1][7]/2.;
    M[564]=-L[0][6]/2. + L[0][7]/2. + L[3][6]/2. - L[3][7]/2.;
    M[568]=L[1][4]/2. - L[1][5]/2. - L[2][4]/2. + L[2][5]/2.;
    M[569]=L[2][5]/2. - L[2][6]/2. - L[3][5]/2. + L[3][6]/2.;
    M[575]=L[0][7]*(-1./(2.*Nc) + Nc/2.) + L[1][4]*(-1./(2.*Nc) + Nc/2.) + L[2][5]*(-1./(2.*Nc) + Nc/2.) + L[3][6]*(-1./(2.*Nc) + Nc/2.) - L[0][1]/(2.*Nc) + L[0][2]/(2.*Nc) - L[0][3]/(2.*Nc) + L[0][4]/(2.*Nc) - L[0][5]/(2.*Nc) + L[0][6]/(2.*Nc) - L[1][2]/(2.*Nc) + L[1][3]/(2.*Nc) + L[1][5]/(2.*Nc) - L[1][6]/(2.*Nc) + L[1][7]/(2.*Nc) - L[2][3]/(2.*Nc) + L[2][4]/(2.*Nc) + L[2][6]/(2.*Nc) - L[2][7]/(2.*Nc) - L[3][4]/(2.*Nc) + L[3][5]/(2.*Nc) + L[3][7]/(2.*Nc) - L[4][5]/(2.*Nc) + L[4][6]/(2.*Nc) - L[4][7]/(2.*Nc) - L[5][6]/(2.*Nc) + L[5][7]/(2.*Nc) - L[6][7]/(2.*Nc);
    
    // GET MATRIX EXPONENTIAL //
    MatrixExp=r8mat_expm1(MatrixSize,M);
    //MatrixExp=r8mat_expm2(MatrixSize,M);
    
    // RESET FINAL RESULT //
    double FinalResult=0.0;
    /*
     // SLOW VERISON //
     // MULTIPLY WITH Nc WEIGHTS AND INITIAL CONDITION //
     for(int i=0;i<MatrixSize;i++){
     for(int j=0;j<MatrixSize;j++){
     
     FinalResult+=NcVector[i]*real(MatrixExp[MatrixIndex(i,j)])*InitialConditionVector[j];
     
     }
     }
     */
    // FASTER VERSION //
    /*
    // MULTIPLY WITH Nc WEIGHTS AND INITIAL CONDITION //
    for(int i=0;i<MatrixSize;i++){
        
        FinalResult+=NcVector[i]*MatrixExp[MatrixIndex(i,0)];
        
        
    }
    // CLEAN UP //
     */
    
    // MULTIPLY WITH Nc WEIGHTS AND OVER ALL CONFIGURATIONS //
    for(int i=0;i<MatrixSize;i++){
        for(int j=0;j<MatrixSize;j++){
        
            FinalResult+=NcVector[i]*MatrixExp[MatrixIndex(i,j)];
        
        }
    }
    
    // CLEAN UP //
    delete[] M;
    delete[] MatrixExp;
    
    // RETURN FINAL RESULT //
    return FinalResult;
    
    
}
