// DEFINE NUMERICAL GLUON PROPOGATOR FUNCTION L(u,v) //
// BESSEL FUNCTION FORM
double Lfunction(double ux,double uy,double vx,double vy,double Lambda){
    
    if(ux==vx && uy==vy){
        return 0.0;
    }
    else{
        return EuclideanNorm(ux,uy,vx,vy)*gsl_sf_bessel_K1(Lambda*EuclideanNorm(ux,uy,vx,vy))/(4.0*Lambda*PI) - 1.0/(4.0*std::pow(Lambda,2)*PI);
    }
    
}
// DIS AAMQS FORM
//L(r) = (g^4μ^2*r^2)/4/Log[1/(r*Lqcd) + Exp[1]]
//Λqcd ≡ (m/2)exp(γE−1/2)
double ec=1;
double Lfunction2(double ux,double uy,double vx,double vy,double Lambda){
    
    if(ux==vx && uy==vy){
        return 0.0;
    }
    else{
        return -std::pow(EuclideanNorm(ux,uy,vx,vy),2)/(16.0*PI)*std::log(1.0/(EuclideanNorm(ux,uy,vx,vy)*Lambda)+std::exp(1.0)*ec);
    }
    
}
// SINGLE PARTICLE KERNEL //
double SingleParticleKernel(double x,double y,double g4muSQR,double Lambda){
    return exp(Cf*g4muSQR*Lfunction2(x/2.0,0.0,-x/2.0,0.0,Lambda));
}

#include "../MatrixExp/matrix_exponential.cpp"

// CALCULATES MEval(x1,y1,x2,y2,x3,y3,x4,y4)=<D({x1,y1},{x2,y2})D({x3,y3},{x4,y4})>
double MatrixEvaluation(double x1,double y1,double x2,double y2,double x3,double y3,double x4,double y4,double g4muSQR,double Lambda){
    
    // GLUON PROPOAGATORS //
    double L[nWilsonLines][nWilsonLines]={
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0}
    };
    
    // DEFINE N GLUON PROPAGATOR MATRIX //
    double *M=new double[MatrixSize*MatrixSize];
    
    // DEFINE MATRIX BUFFERS //
    //std::complex<double> *ExponentialMatrix=new std::complex<double>[MatrixSize*MatrixSize];
    //std::complex<double> *MatrixExp=new std::complex<double>[MatrixSize*MatrixSize];
    //std::complex<double> *InvertedEigenvectorMatrix=new std::complex<double>[MatrixSize*MatrixSize];
    //std::complex<double> *EigenvectorMatrix=new std::complex<double>[MatrixSize*MatrixSize];
    
    // MATRIX EXPONENTIAL //
    double *MatrixExp;

    // VECTORS FOR MATRIX PRODUCTS
    double NcVector[MatrixSize]={1.0,1.0/Nc};
    //double InitialConditionVector[MatrixSize]={1.0,0.0};
    
    // POSITION VECTOR//
    double x[nWilsonLines][2]={{0,0},{0,0},{0,0},{0,0}};
    
    // RESET MATRICES //
    for(int i=0;i<MatrixSize*MatrixSize;i++){
        
        M[i]=0.0;
        //ExponentialMatrix[i]=0.0;
        //MatrixExp[i]=0.0;
        //InvertedEigenvectorMatrix[i]=0.0;
        //EigenvectorMatrix[i]=0.0;
    }
    // DEFINE COORDINATES //
    x[0][0]=x1;
    x[0][1]=y1;
    x[1][0]=x2;
    x[1][1]=y2;
    
    x[2][0]=x3;
    x[2][1]=y3;
    x[3][0]=x4;
    x[3][1]=y4;
    
     // OUTPUT COORDINATES //
//                        std::cout << "Cx1={" << x[0][0] << "," << x[0][1] << "};" << std::endl;
//                        std::cout << "Cx2={" << x[1][0] << "," << x[1][1] << "};" << std::endl;
//                        std::cout << "Cx3={" << x[2][0] << "," << x[2][1] << "};" << std::endl;
//                        std::cout << "Cx4={" << x[3][0] << "," << x[3][1] << "};" << std::endl;
    
    // DEFINE GLUON PROPOGATOR TERMS//
    for(int j=0;j<nWilsonLines;j++){
        for(int i=0;i<=j;i++){
            
            L[i][j]=g4muSQR*Lfunction2(x[i][0],x[i][1],x[j][0],x[j][1],Lambda);
        }
        
    }
    
    // DEFINE N GLUON PROPAGATOR MATRIX //
    M[0]=L[0][1]*(-1/(2.*Nc) + Nc/2.) + L[2][3]*(-1/(2.*Nc) + Nc/2.) + L[0][2]/(2.*Nc) - L[0][3]/(2.*Nc) - L[1][2]/(2.*Nc) + L[1][3]/(2.*Nc);
    M[1]=L[0][1]/2. - L[0][2]/2. - L[1][3]/2. + L[2][3]/2.;
    M[2]=-L[0][2]/2. + L[0][3]/2. + L[1][2]/2. - L[1][3]/2.;
    M[3]=L[0][3]*(-1/(2.*Nc) + Nc/2.) + L[1][2]*(-1/(2.*Nc) + Nc/2.) - L[0][1]/(2.*Nc) + L[0][2]/(2.*Nc) + L[1][3]/(2.*Nc) - L[2][3]/(2.*Nc);
    
    // OPTION FOR EIGENSYSTEM DETERMINATION OF MATRIX EXPONENTIAL //
    /*
    // SET UP EIGENSYSTEM //
    Eigensystem *MatrixEigensystem=new Eigensystem(M,MatrixSize);
    // DIAGONALIZE GLUON MATRIX //
    MatrixEigensystem->Diagonalize();
    // CHECK DIAGONALIZATION //
    //MatrixEigensystem->Check();
    // BUFFER FOR EIGENVALUES //
    double Eigenvalues[MatrixSize];
    
    std::complex<double> eVal; std::complex<double> *eVec=new std::complex<double>[MatrixSize];
    // ASSIGN EIGENVALUES TO VECTOR AND EIGENVECTOR TO MATRIX //
    for(int j=0;j<MatrixSize;j++){
        
        MatrixEigensystem->GetEigenMode(j,eVal,eVec);
        
        Eigenvalues[j]=std::real(eVal);
        
        for(int i=0;i<MatrixSize;i++){
            
            EigenvectorMatrix[MatrixIndex(i,j)]=eVec[i];
            
        }
    }
    
    delete eVec;
    // DEFINE INVERSION
    Inversion *MatrixInversion=new Inversion(EigenvectorMatrix,MatrixSize);
    // INVERT EIGENVECTOR MATRIX //
    MatrixInversion->Invert();
    MatrixInversion->GetInverse(InvertedEigenvectorMatrix);
    // DETERMINE DIAGONAL MATRIX OF EXP OF EIGENVALUES //
    for(int j=0;j<MatrixSize;j++){
        for(int i=0;i<MatrixSize;i++){
            
            if(i==j){
                
                ExponentialMatrix[MatrixIndex(i,j)]=std::exp(Eigenvalues[j]);
                
            }
            else{
                ExponentialMatrix[MatrixIndex(i,j)]=0.0;
            }
            
        }
    }
    // DEFINE FULL GLUON MATRIX EXPONENTIAL //
    for(int j=0;j<MatrixSize;j++){
        
        for(int i=0;i<MatrixSize;i++){
            for(int k=0;k<MatrixSize;k++){
                for(int l=0;l<MatrixSize;l++){
                    
                    MatrixExp[MatrixIndex(i,j)]+=EigenvectorMatrix[MatrixIndex(i,k)]*ExponentialMatrix[MatrixIndex(k,l)]*InvertedEigenvectorMatrix[MatrixIndex(l,j)];
                    
                }
            }
        }
    }
     */
    // END EIGENSYSTEM OPTION //
    
    // USE MATRIX EXPONETIAL BLACK BOX PACKAGE //
    // GET MATRIX EXPONENTIAL //
    MatrixExp=r8mat_expm1(MatrixSize,M);
    //MatrixExp=r8mat_expm2(MatrixSize,M);

    // END PACKAGE OPTION //
    
    // RESET FINAL RESULT //
    double FinalResult=0.0;
    // SLOW RESULT //
    /*
    // MULTIPLY WITH Nc WEIGHTS AND INITIAL CONDITION //
    for(int i=0;i<MatrixSize;i++){
        for(int j=0;j<MatrixSize;j++){
            
            FinalResult+=NcVector[i]*real(MatrixExp[MatrixIndex(i,j)])*InitialConditionVector[j];
            
        }
    }
    */
    // FAST RESULT //
    // MULTIPLY WITH Nc WEIGHTS -- PICK OUT INTIAL CONDITION //
    /*
    for(int i=0;i<MatrixSize;i++){
        
        FinalResult+=NcVector[i]*MatrixExp[MatrixIndex(i,0)];
            
    }
    */
    
     for(int i=0;i<MatrixSize;i++){
     
         FinalResult+=NcVector[i]*MatrixExp[MatrixIndex(i,1)];
     
     }
    
    /*
    // DIPOLE-DIPOLE +1/Nc*QUADRUPOLE TERM
    // MULTIPLIED WITH Nc WEIGHTS //
    for(int i=0;i<MatrixSize;i++){
        for(int j=0;j<MatrixSize;j++){

            FinalResult+=NcVector[i]*MatrixExp[MatrixIndex(i,j)];
        
        }
    }
    */
    // CLEAN UP //
    //delete MatrixEigensystem;
    //delete MatrixInversion;
    
    // CLEAN UP //
    delete[] M;
    //delete[] ExponentialMatrix;
    delete[] MatrixExp;
    //delete[] InvertedEigenvectorMatrix;
    //delete[] EigenvectorMatrix;
    
    // RETURN FINAL RESULT //
    return FinalResult;

}
