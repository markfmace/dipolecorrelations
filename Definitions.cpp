///////////////////
/// DEFINITIONS //
//////////////////
#include <ctime>
#include <cmath>
#include <cstring>
#include <cstdlib>
#include <complex>
#include <string>
#include <iostream>
#include <fstream>
#include <limits>
#include <vector>
// MPI //
#include <mpi.h>
// OPEN MP //
#include <omp.h>

// GSL //
#include <gsl/gsl_math.h>
#include <gsl/gsl_qrng.h>
#include <gsl/gsl_integration.h>

// BESSEL FUNCTION //
#include <gsl/gsl_sf_bessel.h>

// PI //
#define PI double(3.141592653589793238462643383279502884197169399375105820974944592)   // PI
#define SQRT2 double(1.414213562373095048801688724209698078569671875376948073176679738)   // sqrt(2)
#define EXP double(2.718281828459045235360287471352662497757247093699959574966967628)   // exp(1)

// DEFINITIONS
typedef std::complex<double> COMPLEX;
#define ComplexI COMPLEX(0.0,1.0) // i
static const int MAX_DIGITS_PRECISION=std::numeric_limits<double>::digits10;
static const int OUTPUT_PRECISION=MAX_DIGITS_PRECISION;

// DEFINE EUCLIDEAN NORM //
double EuclideanNorm(double ux,double uy,double vx,double vy){
    return std::sqrt((ux-vx)*(ux-vx)+(uy-vy)*(uy-vy));
}
