#ifndef GSL_DBL_EPSILON
#define GSL_DBL_EPSILON        2.2204460492503131e-16
#endif

#include "../MISC/SpecialFunctions.cpp"

namespace PDF {

    
    //PROBABILITY DISTRIBUTIONS //
    double P(int n,double r,double p,double Bpar){
        
        return r*exp(-r*r/(4.0*Bpar))*(Bessel(n,p*r))/(2.0*Bpar);
    }
    
    double PInt(int n,double r,double pMax,double Bpar){
        
        if(n==0){
            
            return pMax*exp(-r*r/(4.0*Bpar))*(Bessel(1,pMax*r))/(2.0*Bpar);
        }
        else if(n==1){
            std::cerr << "#n=1 CALLED -- ERROR " << std::endl;
            return 0;
            
        }
        else if(n==2){
            return exp(-r*r/(4.0*Bpar))/(2.0*Bpar)*(2.0-2.0*Bessel(0,pMax*r)-pMax*r*Bessel(1,pMax*r))/r;
            
        }
        else if(n==3){
            if(r==0.0){
                return 0.0;
            }
            else{
                return std::exp(-r*r/(4.0*Bpar))*(Bessel(1,pMax*r)*(3.0*PI*pMax*r*StruveH0(pMax*r)-16.0)+pMax*r*Bessel(0,pMax*r)*(8.0-3.0*PI*StruveH1(pMax*r)))/(4.0*Bpar*r);
            }
            
        }
        else if(n==4){
            if(r==0.0){
                return 0.0;
            }
            else{
                return std::exp(-r*r/(4.0*Bpar))*(4.0*r+(pMax*r*r-8.0/pMax)*Bessel(1,pMax*r)-8.0*r*Bessel(2,pMax*r))/(2.0*Bpar*r*r);
            }
        }
        else if(n==5){
            if(r==0.0){
                return 0.0;
            }else{
                return std::exp(-r*r/(4.0*Bpar))*(Bessel(1,pMax*r)*(16.0*std::pow(pMax*r,2)+5.0*PI*std::pow(pMax*r,3)*StruveH0(pMax*r)-256.0)+pMax*r*Bessel(0,pMax*r)*(128.0+8.0*std::pow(pMax*r,2)-5.0*PI*std::pow(pMax*r,2)*StruveH1(pMax*r)))/(4.0*Bpar*r*std::pow(pMax*r,2));
            }
            
        }
        else{
            
            std::cerr << "#WRONG HARMONIC!!! -- CATASTROPHIC FAILURE -- PInt -- " << n << std::endl;
            return 0.0;
        }
    }
    
    // TO RE-NORMALIZE DISTRIBUTION //
    double MaxDistance=10.0;
    
    // ABSOLUTE J //
    double PABS(int n,double r,double p,double Bpar){
        
        return r*exp(-r*r/(4.0*Bpar))*fabs(Bessel(n,p*r))/(2.0*Bpar);
        
    }
    
    // ABSOLUTE J //
    double PABSInt(int n,double r,double pMax,double Bpar){
        
        if(n==0){
            MaxDistance=std::pow(pMax,2)/2.0;
            return fabs(PInt(n,r,pMax,Bpar));
        }
        else if(n==2 || n==3 || n==4 || n==5){
            MaxDistance=std::pow(pMax,2)/2.0;
            return fabs(PInt(n,r,pMax,Bpar));
            
        }
        else{
            
            std::cerr << "#WRONG HARMONIC!!! -- CATASTROPHIC FAILURE -- PABSInt -- " << n<< std::endl;
            return 0.0;
        }
    }
    
    
    // PROBABILITY DISTRIBUTIONS //
    double P0Int(double r,void *params){
        
        double pMax = *(double *) params;
        if(pMax>0){
            if(Bessel(1,pMax*r)>0){
                return pMax*exp(-r*r/(4.0*B))/(2.0*B)*Bessel(1,pMax*r);
            }
            else{
                return 0.0;
            }
        }
        else{
            if(Bessel(1,pMax*r)<0){
                return pMax*exp(-r*r/(4.0*B))/(2.0*B)*Bessel(1,pMax*r);
            }
            else{
                return 0.0;
            }
        }
        
    }
    
    // PROBABILITY DISTRIBUTIONS //
    double P2Int(double r,void *params){
        
        double pMax = *(double *) params;
        if(pMax>0){
            if((2.0-2.0*Bessel(0,pMax*r)-pMax*r*Bessel(1,pMax*r))>0){
                return exp(-r*r/(4.0*B))/(2.0*B)*(2.0-2.0*Bessel(0,pMax*r)-pMax*r*Bessel(1,pMax*r))/r;
            }
            else{
                return 0.0;
            }
        }
        else{
            if((2.0-2.0*Bessel(0,pMax*r)-pMax*r*Bessel(1,pMax*r))<0){
                return exp(-r*r/(4.0*B))/(2.0*B)*(2.0-2.0*Bessel(0,pMax*r)-pMax*r*Bessel(1,pMax*r))/r;
            }
            else{
                return 0.0;
            }
        }
        
    }
    // ALL PROBABILITY DISTRIBUTIONS FOR DIFFERENTIAL SPECTRA //
    // IntegralFlag: 0 J_n>0, 1 J_n<0, 2 J_n, 3 abs(J_n) //
    double Pall(double r,int IntegralFlag,double p,int n,double Bpar){
        // DEFINE OVERALL DISTRIBUTION //
        double P=r*exp(-r*r/(4.0*Bpar))/(2.0*Bpar)*Bessel(n,p*r);
        
        // J_n >0
        if(IntegralFlag==0){
            if(P>0){
                return P;
            }
            else{
                return 0.0;
            }
        }
        // J_n < 0
        if(IntegralFlag==1){
            if(P<0){
                return P;
            }
            else{
                return 0.0;
            }
        }
        
        // J_n
        if(IntegralFlag==2){
            return P;
        }
        // abs(J_n)
        if(IntegralFlag==3){
            return fabs(P);
        }
        
        std::cerr << "ERROR: invalid flag set" << std::endl;
        return 0.0;
    }
    // ALL PROBABILITY DISTRIBUTIONS FOR INTEGRATED SPECTRA //
    // IntegralFlag: 0 J_n>0, 1 J_n<0, 2 J_n, 3 abs(J_n) //
    double PallInt(double r,int IntegralFlag,double pMax,int n,double Bpar){
        // DEFINE OVERALL DISTRIBUTION //
        double P;
        if(n==0){
            
            P=pMax*exp(-r*r/(4.0*Bpar))/(2.0*Bpar)*Bessel(1,pMax*r);
        }
        else if(n==1){
            
            std::cerr << "#WRONG HARMONIC!!! n=1 CALLED -- CATASTROPHIC FAILURE" << std::endl;
            return 0.0;
        }
        else if(n==2){
            P=exp(-r*r/(4.0*Bpar))/(2.0*Bpar)*(2.0-2.0*Bessel(0,pMax*r)-pMax*r*Bessel(1,pMax*r))/r;
            
        }
        else if(n==3){
            if(r==0.0){
                P=0.0;
            }
            else{
                P=std::exp(-r*r/(4.0*Bpar))*(Bessel(1,pMax*r)*(3.0*PI*pMax*r*StruveH0(pMax*r)-16.0)+pMax*r*Bessel(0,pMax*r)*(8.0-3.0*PI*StruveH1(pMax*r)))/(4.0*Bpar*r);
            }
        }
        else if(n==4){
            if(r==0.0){
                P=0.0;
            }
            else{
                P=std::exp(-r*r/(4.0*Bpar))*(4.0*r+(pMax*r*r-8.0/pMax)*Bessel(1,pMax*r)-8.0*r*Bessel(2,pMax*r))/(2.0*Bpar*r*r);
            }
        }
        else if(n==5){
            if(r==0.0){
                P=0.0;
            }else{
                P=std::exp(-r*r/(4.0*Bpar))*(Bessel(1,pMax*r)*(16.0*std::pow(pMax*r,2)+5.0*PI*std::pow(pMax*r,3)*StruveH0(pMax*r)-256.0)+pMax*r*Bessel(0,pMax*r)*(128.0+8.0*std::pow(pMax*r,2)-5.0*PI*std::pow(pMax*r,2)*StruveH1(pMax*r)))/(4.0*Bpar*r*std::pow(pMax*r,2));
            }
            
        }
        else{
            
            std::cerr << "#WRONG HARMONIC!!! -- CATASTROPHIC FAILURE -- PallInt -- " << n << std::endl;
            return 0.0;
        }
        
        // J_n >0
        if(IntegralFlag==0){
            if(P>0){
                return P;
            }
            else{
                return 0.0;
            }
        }
        // J_n < 0
        if(IntegralFlag==1){
            if(P<0){
                return P;
            }
            else{
                return 0.0;
            }
        }
        
        // J_n
        if(IntegralFlag==2){
            return P;
        }
        // abs(J_n)
        if(IntegralFlag==3){
            return fabs(P);
        }
        
        std::cerr << "ERROR: invalid flag set" << std::endl;
        return 0.0;
    }
    
    // NORMALIZATION OF DISTRIBUTIONS FOR DIFFERENTIAL SPECTRA //
    double GetNormalization(int IntegralFlag,double p,int n,double Bpar){
        
        // BEGIN BY HAND OPTION //
        double RelErr=std::pow(10.0,-10);
        
        // J_n>0
        if(IntegralFlag==0){
            //J_n
            IntegralFlag=2;
            double Norm=Integration::TrapezoidIntegrationInf(0.0,RelErr,Pall,IntegralFlag,p,n,Bpar);
            //abs(J_n)
            IntegralFlag=3;
            double NormAbs=Integration::TrapezoidIntegrationInf(0.0,RelErr,Pall,IntegralFlag,p,n,Bpar);
            
            return 0.5*(Norm+NormAbs);
        }
        // J_n<0
        else if(IntegralFlag==1){
            //J_n
            IntegralFlag=2;
            double Norm=Integration::TrapezoidIntegrationInf(0.0,RelErr,Pall,IntegralFlag,p,n,Bpar);
            //abs(J_n)
            IntegralFlag=3;
            double NormAbs=Integration::TrapezoidIntegrationInf(0.0,RelErr,Pall,IntegralFlag,p,n,Bpar);
            
            return 0.5*(Norm-NormAbs);
        }
        // J_n
        else if(IntegralFlag==2){
            
            double Norm=Integration::TrapezoidIntegrationInf(0.0,RelErr,Pall,IntegralFlag,p,n,Bpar);
            
            return Norm;
        }
        // abs(J_n)
        else if(IntegralFlag==3){
            //abs(J_n)
            double NormAbs=Integration::TrapezoidIntegrationInf(0.0,RelErr,Pall,IntegralFlag,p,n,Bpar);
            
            return NormAbs;
        }
        else{
            std::cerr << "#WRONG INTEGRALFLAG -- CRITICAL ERROR " << std::endl;
            
            exit(0);
        }
        // END BY HAND OPTION //
        
    }
    // NORMALIZATION OF DISTRIBUTIONS FOR INTEGRATED SPECTRA //
    double GetNormalizationIntegrated(int IntegralFlag,double pMax,int n,double Bpar){
        
        
        // BEGIN BY HAND OPTION //
        double RelErr=std::pow(10.0,-10);
        
        // J_n>0
        if(IntegralFlag==0){
            //J_n
            IntegralFlag=2;
            double Norm=Integration::TrapezoidIntegrationInf(0.0,RelErr,PallInt,IntegralFlag,pMax,n,Bpar);
            //abs(J_n)
            IntegralFlag=3;
            double NormAbs=Integration::TrapezoidIntegrationInf(0.0,RelErr,PallInt,IntegralFlag,pMax,n,Bpar);
            
            return 0.5*(Norm+NormAbs);
        }
        // J_n<0
        else if(IntegralFlag==1){
            //J_n
            IntegralFlag=2;
            double Norm=Integration::TrapezoidIntegrationInf(0.0,RelErr,PallInt,IntegralFlag,pMax,n,Bpar);
            //abs(J_n)
            IntegralFlag=3;
            double NormAbs=Integration::TrapezoidIntegrationInf(0.0,RelErr,PallInt,IntegralFlag,pMax,n,Bpar);
            
            return 0.5*(Norm-NormAbs);
        }
        // J_n
        else if(IntegralFlag==2){
            
            double Norm=Integration::TrapezoidIntegrationInf(0.0,RelErr,PallInt,IntegralFlag,pMax,n,Bpar);
            
            return Norm;
        }
        // abs(J_n)
        else if(IntegralFlag==3){
            //abs(J_n)
            double NormAbs=Integration::TrapezoidIntegrationInf(0.0,RelErr,PallInt,IntegralFlag,pMax,n,Bpar);
            
            return NormAbs;
        }
        else{
            std::cerr << "#WRONG INTEGRALFLAG -- CRITICAL ERROR " << std::endl;
            exit(0);
        }
        
    }
    // DIFFERENTIAL //
    double SamplePDistrubtion(double pT,double n,double Bpar){
        
        double RejectVar,r;
        
        do{
            r=RandomNumberGenerator::RaleighRNG(sqrt(2*Bpar));
            
            RejectVar=RandomNumberGenerator::rng();
            
        }while(RejectVar>PDF::PABS(n,r,pT,Bpar)/Rayleigh(r,sqrt(2*Bpar)));
        
        return r;
    }
    
    double SamplePDistrubtionInt(double pMax,double n,double Bpar){
        
        double RejectVar,r;
        
        do{
            r=RandomNumberGenerator::RaleighRNG(sqrt(2*Bpar));
            
            RejectVar=MaxDistance*RandomNumberGenerator::rng();
            
        }while(RejectVar>PDF::PABSInt(n,r,pMax,Bpar)/Rayleigh(r,sqrt(2*Bpar)));
        
        return r;
    }
    
    
}

